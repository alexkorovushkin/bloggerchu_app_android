webpackJsonp([10],{

/***/ 1056:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export UserSettings */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileOptionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_geo_service__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_profile__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__welcome_welcome__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_clipboard__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_crop__ = __webpack_require__(192);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import {ChoiceCityPage} from "../choice-city/choice-city";








var UserSettings = /** @class */ (function () {
    function UserSettings(data) {
        this.id = data.id;
        this.coin = data.coin;
        this.name = data.name;
        this.type = data.type;
        this.city = data.city;
        this.full_address = data.full_address;
        this.lat = data.lat;
        this.lng = data.lng;
        this.profession_id = data.profession_id;
        this.workDay = data.workDay;
        this.workTimeStart = data.workTimeStart;
        this.workTimeEnd = data.workTimeEnd;
        this.lunchTimeEnd = data.lunchTimeEnd;
        this.lunchTimeStart = data.lunchTimeStart;
        this.lunchEnable = data.lunchEnable;
        this.about = data.about;
        this.profession = data.profession;
        this.sex = data.sex;
        this.out = data.out;
        this.userLink = data.userLink;
    }
    return UserSettings;
}());

var ProfileOptionsPage = /** @class */ (function () {
    function ProfileOptionsPage(navCtrl, navParams, auth, config, modalCtrl, geoService, profile, actionSheetCtrl, camera, platform, toastCtrl, clipboard, transfer, loadingCtrl, appCtrl, crop) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.config = config;
        this.modalCtrl = modalCtrl;
        this.geoService = geoService;
        this.profile = profile;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.clipboard = clipboard;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.appCtrl = appCtrl;
        this.crop = crop;
        this.waitLoading = true;
        this.professions = [];
        this.serviceList = [];
        this.addCallbackFunction = function (_params) {
            return new Promise(function (resolve, reject) {
                //console.log(_params);
                _this.serviceList.push(_params);
                resolve();
            });
        };
        this.newProfessionID = this.auth.currentUser.profession_id;
        //console.log(this.serviceList);
        //console.log(this.serviceList)
        this.newSettings = new UserSettings(this.auth.currentUser);
    }
    ProfileOptionsPage.prototype.ionViewWillEnter = function () {
        //console.log(this.newSettings.name)
    };
    ProfileOptionsPage.prototype.ionViewWillLeave = function () {
        //this.navCtrl.setRoot(HomePage)
    };
    ProfileOptionsPage.prototype.getIndexByType = function (array, type) {
        return array.findIndex(function (e) { return e.types.indexOf(type) >= 0; });
    };
    ProfileOptionsPage.prototype.getProf = function () {
        var _this = this;
        this.profile.getProfessions().subscribe(function (res) {
            _this.professions = res;
        });
    };
    ProfileOptionsPage.prototype.getMasterService = function () {
        var _this = this;
        this.profile.getMasterService().subscribe(function (res) {
            if (res)
                _this.serviceList = res;
            //console.log('загружен список услуг', this.serviceList )
        });
    };
    ProfileOptionsPage.prototype.onChangeProfession = function () {
        var _this = this;
        //console.log('изменение профессии')
        if (this.newProfessionID != this.auth.currentUser.profession_id) {
            this.profile.setProfession(this.newProfessionID).subscribe(function (res) {
                _this.auth.currentUser.profession_id = _this.newProfessionID;
                //console.log(res)
            });
        }
    };
    ProfileOptionsPage.prototype.choiceCity = function () {
        var _this = this;
        var cssClass;
        this.config.getActiveTheme().subscribe(function (res) { return cssClass = res; });
        var modalChoiceCity = this.modalCtrl.create('ChoiceCityPage', { type: this.auth.currentUser.type }, { cssClass: cssClass });
        modalChoiceCity.onDidDismiss(function (data) {
            if (data) {
                //console.log(data);
                //this.auth.currentUser.city=data.structured_formatting.main_text;
                //let city=data.structured_formatting;
                _this.geoService.placeDetail(data.place_id).subscribe(function (res) {
                    //let res = resp.data;
                    //console.log(res)
                    var address = {
                        full_address: data.description,
                        city: res.result.address_components[_this.getIndexByType(res.result.address_components, 'locality')].long_name,
                        lat: res.result.geometry.location.lat,
                        lng: res.result.geometry.location.lng
                    };
                    _this.auth.currentUser.city = address.city;
                    _this.auth.currentUser.full_address = address.full_address;
                    _this.auth.setUserFullAddress(address).subscribe(function (res) {
                        //console.log(res)
                    });
                    _this.auth.setUserCity(address.city).subscribe(function (res) {
                        //console.log(res)
                    });
                });
            }
        });
        modalChoiceCity.present();
    };
    ProfileOptionsPage.prototype.exitAcc = function () {
        var _this = this;
        this.auth.logout().subscribe(function (allowed) {
            if (allowed) {
                _this.appCtrl.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_5__welcome_welcome__["a" /* WelcomePage */]);
            }
            else {
            }
        }, function (error) {
        });
    };
    ProfileOptionsPage.prototype.getProfessionIndexById = function (id) {
        return this.professions.findIndex(function (e) { return e.id === id; });
    };
    ProfileOptionsPage.prototype.saveSetting = function () {
        var _this = this;
        this.auth.setUserSettings(this.newSettings).subscribe(function (res) {
            if (res.status) {
                //console.log(res)
                //console.log(this.professions[this.getProfessionIndexById(this.newSettings.profession_id.toString())].name)
                _this.auth.currentUser.name = _this.newSettings.name;
                _this.auth.currentUser.profession_id = _this.newSettings.profession_id;
                _this.auth.currentUser.profession = _this.newSettings.profession_id ? _this.professions[_this.getProfessionIndexById(_this.newSettings.profession_id)].name : '';
                _this.auth.currentUser.workDay = _this.newSettings.workDay;
                _this.auth.currentUser.workTimeStart = _this.newSettings.workTimeStart;
                _this.auth.currentUser.workTimeEnd = _this.newSettings.workTimeEnd;
                _this.auth.currentUser.lunchTimeEnd = _this.newSettings.lunchTimeEnd;
                _this.auth.currentUser.lunchTimeStart = _this.newSettings.lunchTimeStart;
                _this.auth.currentUser.lunchEnable = _this.newSettings.lunchEnable;
                _this.auth.currentUser.about = _this.newSettings.about;
                _this.auth.currentUser.sex = _this.newSettings.sex;
                _this.auth.currentUser.out = _this.newSettings.out;
                _this.auth.currentUser.userLink = _this.newSettings.userLink;
                _this.navCtrl.popToRoot();
            }
        });
    };
    ProfileOptionsPage.prototype.goToMyKassa = function () {
        this.navCtrl.push('MykassaPage');
    };
    ProfileOptionsPage.prototype.choiceProfession = function () {
        var _this = this;
        var cssClass;
        this.config.getActiveTheme().subscribe(function (res) { return cssClass = res; });
        var modalChoiceProfession = this.modalCtrl.create('ProfessionSelectPage', { professions: this.professions }, { cssClass: cssClass });
        modalChoiceProfession.onDidDismiss(function (data) {
            if (data) {
                //console.log(data)
                _this.newSettings.profession = data.name;
                _this.newSettings.profession_id = data.id;
            }
        });
        modalChoiceProfession.present();
    };
    ProfileOptionsPage.prototype.toggleTheme = function () {
        console.log(this.themeSelected);
        if (this.themeSelected) {
            this.config.setActiveTheme('dark-theme');
        }
        else {
            this.config.setActiveTheme('light-theme');
        }
    };
    ProfileOptionsPage.prototype.choiceTypeAvatarSource = function (type) {
        var _this = this;
        var buttons;
        if (this.platform.is('ios')) {
            buttons = [{
                    text: 'Выбрать из библиотеки',
                    //icon: 'photos',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.PHOTOLIBRARY, type); }
                }, {
                    text: 'Сделать фотографию',
                    // icon: 'camera',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.CAMERA, type); }
                }, {
                    text: 'Отмена',
                    role: 'cancel',
                }];
        }
        else {
            buttons = [{
                    text: 'Выбрать из библиотеки',
                    icon: 'photos',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.PHOTOLIBRARY, type); }
                }, {
                    text: 'Сделать фотографию',
                    icon: 'camera',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.CAMERA, type); }
                }, {
                    text: 'Отмена',
                    role: 'cancel',
                    icon: 'close',
                }];
        }
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Источник изображения',
            buttons: buttons
        });
        actionSheet.present();
    };
    ProfileOptionsPage.prototype.getImageAvatar = function (selectedSourceType, type) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: selectedSourceType,
            mediaType: 0,
            correctOrientation: true,
            allowEdit: false,
            encodingType: 0
        };
        var optionCrop = {
            quality: 100,
            widthRatio: 0,
            heightRatio: 0,
            targetWidth: 0,
            targetHeight: 0
        };
        if (type == 'avatar') {
            options.targetWidth = 300;
            options.targetHeight = 300;
            optionCrop.heightRatio = 1;
            optionCrop.widthRatio = 1;
            if (this.platform.is('ios')) {
                options.targetWidth = 300;
                options.targetHeight = 300;
                optionCrop.heightRatio = 1;
                optionCrop.widthRatio = 1.3;
            }
        }
        else if (type == 'background') {
            /*      options.targetWidth = 1024;
                  options.targetHeight = 600;*/
        }
        var loader = this.loadingCtrl.create({
            content: "Подождите...",
            dismissOnPageChange: true
            /*      spinner: 'hide',
                  content: `
                  <div class="custom-spinner-container">
                    <div class="custom-spinner-box"></div>
                  </div>`,*/
        });
        loader.present();
        this.camera.getPicture(options).then(function (imageData) {
            _this.imageURI = imageData;
            _this.crop.crop(imageData, optionCrop).then(function (newImage) {
                _this.imageURI = newImage;
                loader.present();
                _this.uploadFile(type);
            }, function (err) {
                //console.log(err);
                loader.dismissAll();
            });
        }, function (err) {
            //console.log(err);
            _this.presentToast(err);
        });
    };
    ProfileOptionsPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            //console.log('Dismissed toast');
        });
        toast.present();
    };
    ProfileOptionsPage.prototype.uploadFile = function (type) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Отправка изображения..."
            /*      spinner: 'hide',
                  content: `
                  <div class="custom-spinner-container">
                    <div class="custom-spinner-box"></div>
                  </div>`,*/
        });
        loader.present();
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'file',
            fileName: 'ionicfile.jpg',
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {
                'Authorization': 'Bearer ' + __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */].jwt2
            }
        };
        fileTransfer.upload(this.imageURI, this.config.apiURL + 'fn/upload_file.php?type=' + type, options)
            .then(function (data) {
            //console.log(data);
            var tmp_data = JSON.parse(data.response);
            //console.log(tmp_data);
            loader.dismissAll();
            if (tmp_data.isSuccess) {
                if (type == 'avatar')
                    _this.auth.currentUser.avatar = tmp_data.files[0].file;
                if (type == 'background')
                    _this.auth.currentUser.background = tmp_data.files[0].file;
                //this.presentToast("Изображение профиля успешно обновлено");
            }
            else
                _this.presentToast("Не удалось загрузить Изображение. Повторите позже");
        }, function (err) {
            //console.log(err);
            loader.dismissAll();
            _this.presentToast("Не удалось обновить Изображение. Повторите позже");
        });
        fileTransfer.onProgress(function (progressEvent) {
            /*      if (progressEvent.lengthComputable) {
                    this.loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
                  } else {
                    this.loadingStatus.increment();
                  }*/
            ////console.log(progressEvent)
        });
    };
    ProfileOptionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile-options',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/profile-options/profile-options.html"*/'<ion-header>\n\n  <ion-navbar >\n    <ion-title>Профиль</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <div class="avatar" text-center>\n    <img (click)="choiceTypeAvatarSource(\'avatar\')"  src="{{ auth.currentUser.avatar }}" alt="">\n  </div>\n  <form  id="ngFormSettings" #settingsForm="ngForm" class="nameUser">\n    <ion-item>\n      <ion-label floating>Имя:</ion-label>\n      <ion-input required minlength="2" (ionChange)="saveSetting()" maxlength="60" name="name" type="text" [(ngModel)]="newSettings.name"></ion-input>\n    </ion-item>\n  </form>\n  <hr>\n  <ion-item>\n    <ion-label floating>О себе:</ion-label>\n    <ion-textarea (ionChange)="saveSetting()" elasticTextArea  type="text" [(ngModel)]="newSettings.about"></ion-textarea>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Идентификатор:</ion-label>\n    <ion-input readonly minlength="1" (ionChange)="saveSetting()" maxlength="60" type="text" [(ngModel)]="newSettings.userLink"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label floating>Город: <small>(необязательно)</small></ion-label>\n    <ion-input tappable type="text" [readonly]="true" (ionFocus)="choiceCity()" value="{{auth.currentUser.type==2?auth.currentUser.full_address:auth.currentUser.city}}"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Пол: <small>(необязательно)</small></ion-label>\n    <ion-select [(ngModel)]="newSettings.sex" (ionChange)="saveSetting()" cancelText="Отмена" okText="Выбрать">\n      <ion-option value="0">Не выбран</ion-option>\n      <ion-option value="1">Женский</ion-option>\n      <ion-option value="2">Мужской</ion-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Баланс:</ion-label>\n    <ion-input readonly type="text" [(ngModel)]="newSettings.coin"></ion-input>\n  </ion-item>\n\n  <ion-buttons>\n    <button class="buttonKassa" ion-button (click)="goToMyKassa()">\n      Настроить расценки\n    </button>\n    <button class="buttonKassaE" ion-button (click)="exitAcc()">\n      Выйти\n    </button>\n  </ion-buttons>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/profile-options/profile-options.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_6__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_geo_service__["a" /* GeoServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_profile__["a" /* ProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_clipboard__["a" /* Clipboard */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_crop__["a" /* Crop */]])
    ], ProfileOptionsPage);
    return ProfileOptionsPage;
}());

//# sourceMappingURL=profile-options.js.map

/***/ }),

/***/ 882:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileOptionsPageModule", function() { return ProfileOptionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_options__ = __webpack_require__(1056);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(448);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProfileOptionsPageModule = /** @class */ (function () {
    function ProfileOptionsPageModule() {
    }
    ProfileOptionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_options__["a" /* ProfileOptionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_options__["a" /* ProfileOptionsPage */]),
            ],
        })
    ], ProfileOptionsPageModule);
    return ProfileOptionsPageModule;
}());

//# sourceMappingURL=profile-options.module.js.map

/***/ })

});
//# sourceMappingURL=10.js.map