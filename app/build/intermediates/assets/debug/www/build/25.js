webpackJsonp([25],{

/***/ 1052:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateGroupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_contacts__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_profile__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_friend_friend__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_img_upload_service__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__chat_chat__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_crop__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_file_transfer__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















/**
 * Generated class for the CreateGroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreateGroupPage = /** @class */ (function () {
    function CreateGroupPage(navCtrl, auth, navParams, profileService, toastCtrl, modalCtrl, chatService, config, contacts, transfer, loadingCtrl, browser, friendService, sqlite, crop, actionSheetCtrl, imgUploadService, camera, platform) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.navParams = navParams;
        this.profileService = profileService;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.chatService = chatService;
        this.config = config;
        this.contacts = contacts;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.browser = browser;
        this.friendService = friendService;
        this.sqlite = sqlite;
        this.crop = crop;
        this.actionSheetCtrl = actionSheetCtrl;
        this.imgUploadService = imgUploadService;
        this.camera = camera;
        this.platform = platform;
        this.searchText = '';
        this.waitLoading = true;
        this.invateList = [];
        this.nameGroup = '';
        this.roomInfo = { name: '', avatar: "assets/img/gp.png" };
        this.getLocalContacts();
        this.getPeople();
    }
    CreateGroupPage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    CreateGroupPage.prototype.getPeople = function (limitFrom, limitTo, options) {
        var _this = this;
        if (limitFrom === void 0) { limitFrom = 0; }
        if (limitTo === void 0) { limitTo = 10; }
        if (options === void 0) { options = null; }
        return new Promise(function (resolve) {
            _this.profileService.getPeople(limitFrom, limitTo, options).subscribe(function (res) {
                if (!_this.peopleList)
                    _this.peopleList = [];
                //console.log(res)
                for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                    var people = res_1[_i];
                    _this.peopleList.push(people);
                }
                _this.waitLoading = false;
                resolve();
                console.log(_this.peopleList);
            });
        });
    };
    CreateGroupPage.prototype.onSearch = function (ev) {
        var _this = this;
        this.peopleList = [];
        this.searchText = ev.target.value;
        if (this.searchText && this.searchText.trim() != '') {
            if (this.myPeopleList && this.myPeopleList.length > 0) {
                this.myPeopleList = this.myPeopleList.filter(function (item) {
                    console.log(item.name.toLowerCase().indexOf(_this.searchText.toLowerCase()));
                    return (item.name.toLowerCase().indexOf(_this.searchText.toLowerCase()) > -1 || item.invatePhone.indexOf(_this.searchText.toLowerCase()) > -1);
                });
            }
            this.getPeople(0, 10, { search: this.searchText.trim() });
        }
        else {
            this.waitLoading = true;
            this.peopleList = [];
            this.getLocalContacts();
        }
    };
    CreateGroupPage.prototype.checkInGroup = function (user) {
        return this.invateList.find(function (e) { return e === user; });
    };
    CreateGroupPage.prototype.addMan = function (user) {
        var index = this.findIndexByID(user.id);
        if (index === -1) {
            this.invateList.push(user);
        }
        else if (index >= 0) {
            this.invateList.splice(index, 1);
        }
    };
    CreateGroupPage.prototype.deleteFromGroup = function (id) {
        var index = this.findIndexByID(id);
        if (index >= 0) {
            this.invateList.splice(index, 1);
        }
    };
    CreateGroupPage.prototype.findIndexByID = function (id) {
        return this.invateList.findIndex(function (e) { return e.id === id; });
    };
    /*
    addMan(user){
      if (this.invateList){
  
      }
      this.invateList.push(user);
    }*/
    CreateGroupPage.prototype.createGroup = function () {
        var _this = this;
        console.log(this.nameGroup);
        console.log(this.invateList);
        if (this.nameGroup != '') {
            this.chatService.createGroup(this.nameGroup, this.invateList).subscribe(function (res) {
                if (res.status) {
                    _this.navCtrl.popToRoot();
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__chat_chat__["a" /* Chat */], { 'roomid': res.roomid, 'roomname': _this.nameGroup });
                }
            });
        }
    };
    CreateGroupPage.prototype.getLocalContacts = function () {
        var _this = this;
        this.waitLoading = true;
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        })
            .then(function (db) {
            //id INTEGER PRIMARY KEY,
            db.executeSql('CREATE TABLE IF NOT EXISTS mycontacts(id INT, invatePhone TEXT, name TEXT, avatar TEXT, profession TEXT, type INT )')
                .then(function () { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('SELECT * FROM mycontacts ORDER BY id DESC, name ASC')
                .then(function (res) {
                console.log('sqlite_contacts', res);
                _this.myPeopleList = [];
                if (res.rows.length == 0) {
                    _this.contacts.find(['*']).then(function (contacts_res) {
                        _this.myContacts = contacts_res;
                        console.log('contacts_res', _this.myContacts);
                        _this.friendService.saveMyContacts(_this.myContacts).subscribe(function (save_res) {
                            console.log('save_res', save_res);
                            if (save_res.status) {
                                _this.friendService.getMyContacts().subscribe(function (get_contact_res) {
                                    _this.myPeopleList = get_contact_res;
                                    _this.waitLoading = false;
                                    for (var _i = 0, _a = _this.myPeopleList; _i < _a.length; _i++) {
                                        var contact = _a[_i];
                                        db.executeSql('INSERT INTO mycontacts VALUES(?,?,?,?,?,?)', [contact.id, contact.invatePhone, contact.name, contact.avatar, contact.profession, contact.type])
                                            .then(function (insert_res) {
                                            console.log('insert_res', insert_res);
                                        });
                                    }
                                });
                            }
                        });
                    });
                }
                else {
                    _this.waitLoading = false;
                    for (var i = 0; i < res.rows.length; i++) {
                        console.log(res.rows.item(i));
                        _this.myPeopleList.push(res.rows.item(i));
                    }
                }
            })
                .catch(function (e) {
                console.log(e);
                _this.waitLoading = false;
            });
        })
            .catch(function (e) {
            console.log(e);
            _this.waitLoading = false;
        });
    };
    CreateGroupPage.prototype.imageGroupSelect = function (type) {
        var _this = this;
        var buttons;
        if (this.platform.is('ios')) {
            buttons = [{
                    text: 'Выбрать из библиотеки',
                    //icon: 'photos',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.PHOTOLIBRARY, type); }
                }, {
                    text: 'Сделать фотографию',
                    // icon: 'camera',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.CAMERA, type); }
                }, {
                    text: 'Отмена',
                    role: 'cancel',
                }];
        }
        else {
            buttons = [{
                    text: 'Выбрать из библиотеки',
                    icon: 'photos',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.PHOTOLIBRARY, type); }
                }, {
                    text: 'Сделать фотографию',
                    icon: 'camera',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.CAMERA, type); }
                }, {
                    text: 'Отмена',
                    role: 'cancel',
                    icon: 'close',
                }];
        }
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Источник изображения',
            buttons: buttons
        });
        actionSheet.present();
    };
    CreateGroupPage.prototype.getImageAvatar = function (selectedSourceType, type) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: selectedSourceType,
            mediaType: 0,
            correctOrientation: true,
            allowEdit: false,
            encodingType: 0
        };
        var optionCrop = {
            quality: 100,
            widthRatio: 0,
            heightRatio: 0,
            targetWidth: 0,
            targetHeight: 0
        };
        if (type == 'avatar') {
            options.targetWidth = 300;
            options.targetHeight = 300;
            optionCrop.heightRatio = 1;
            optionCrop.widthRatio = 1;
            if (this.platform.is('ios')) {
                options.targetWidth = 300;
                options.targetHeight = 300;
                optionCrop.heightRatio = 1;
                optionCrop.widthRatio = 1.3;
            }
        }
        else if (type == 'background') {
            /*      options.targetWidth = 1024;
                  options.targetHeight = 600;*/
        }
        var loader = this.loadingCtrl.create({
            content: "Подождите...",
            dismissOnPageChange: true
            /*      spinner: 'hide',
                  content: `
                  <div class="custom-spinner-container">
                    <div class="custom-spinner-box"></div>
                  </div>`,*/
        });
        loader.present();
        this.camera.getPicture(options).then(function (imageData) {
            _this.imageURI = imageData;
            _this.crop.crop(imageData, optionCrop).then(function (newImage) {
                _this.imageURI = newImage;
                loader.present();
                _this.uploadFile(type);
            }, function (err) {
                //console.log(err);
                loader.dismissAll();
            });
        }, function (err) {
            //console.log(err);
            _this.presentToast(err);
        });
    };
    CreateGroupPage.prototype.uploadFile = function (type) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Отправка изображения..."
            /*      spinner: 'hide',
                  content: `
                  <div class="custom-spinner-container">
                    <div class="custom-spinner-box"></div>
                  </div>`,*/
        });
        loader.present();
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'file',
            fileName: 'ionicfile.jpg',
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {
                'Authorization': 'Bearer ' + __WEBPACK_IMPORTED_MODULE_12__providers_auth_service__["a" /* AuthService */].jwt2
            }
        };
        fileTransfer.upload(this.imageURI, this.config.apiURL + 'fn/upload_file.php?type=' + type, options)
            .then(function (data) {
            //console.log(data);
            var tmp_data = JSON.parse(data.response);
            //console.log(tmp_data);
            loader.dismissAll();
            if (tmp_data.isSuccess) {
                if (type == 'avatar')
                    _this.auth.currentUser.avatar = tmp_data.files[0].file;
                if (type == 'background')
                    _this.auth.currentUser.background = tmp_data.files[0].file;
                //this.presentToast("Изображение профиля успешно обновлено");
            }
            else
                _this.presentToast("Не удалось загрузить Изображение. Повторите позже");
        }, function (err) {
            //console.log(err);
            loader.dismissAll();
            _this.presentToast("Не удалось обновить Изображение. Повторите позже");
        });
        fileTransfer.onProgress(function (progressEvent) {
            /*      if (progressEvent.lengthComputable) {
                    this.loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
                  } else {
                    this.loadingStatus.increment();
                  }*/
            ////console.log(progressEvent)
        });
    };
    CreateGroupPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            //console.log('Dismissed toast');
        });
        toast.present();
    };
    CreateGroupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-create-group',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/create-group/create-group.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Создать группу</ion-title>\n\n    <ion-buttons end>\n      <button ion-button icon-only [disabled]="invateList?.length==0 || nameGroup==\'\'" (click)="createGroup()"  >\n        Далее <ion-icon name="arrow-forward"></ion-icon>\n      </button>\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n<ion-content>\n  <div ion-fixed class="wait-loading" *ngIf="waitLoading">\n    <ion-spinner name="crescent"></ion-spinner>\n  </div>\n\n  <ion-list class="headerUser">\n    <ion-item class="headerUserInfo">\n      <ion-avatar item-start>\n        <div class="avatar" tappable (click)="imageGroupSelect(\'grouptime\')"  text-center>\n          <img src="assets/img/gp.png" alt="">\n        </div>\n      </ion-avatar>\n      <ion-input style="margin-bottom:0;"  [(ngModel)]="nameGroup" placeholder="Тема группы"  class="nameUserHeader"></ion-input>\n      <p >Введите тему группу и при желании установите картинку группу</p>\n    </ion-item>\n  </ion-list>\n\n  <div class="listGroupUsers" *ngIf="invateList?.length!=0">\n    <span *ngFor="let row of invateList" class="manPlus">\n      <div class="itemManGroup">\n        <ion-avatar item-start class="imageAvatarGroup">\n          <img src="{{row.avatar}}">\n        </ion-avatar>\n          <span class="nameGroup">{{row.name}}</span>\n          <ion-icon (click)="deleteFromGroup(row.id)" class="delGroup" name="close-circle"></ion-icon>\n      </div>\n    </span>\n  </div>\n\n  <ion-searchbar\n    animated="true"\n    placeholder="Поиск"\n    autocomplete="on"\n    autocorrect="on"\n    (ionInput)="onSearch($event)">\n  </ion-searchbar>\n<!--\n\n  <ion-list no-padding *ngIf="myPeopleList && myPeopleList.length>0">\n    <ion-list-header class="headerWho">\n      Мои контакты\n    </ion-list-header>\n    <ng-container *ngFor="let row of myPeopleList;" >\n      <ion-card *ngIf="row.id>0 && row.id!=auth.currentUser.id">\n        <ion-item >\n          <ion-avatar item-start tappable (click)="goToProfile(row.id)">\n            <img src="{{row.avatar}}">\n          </ion-avatar>\n          <h2 tappable (click)="addMan(row)">{{row.name}}</h2>\n          <p tappable (click)="addMan(row)" class="user-status" *ngIf="row.status==1">онлайн</p>\n          <p tappable (click)="addMan(row)" class="user-status" *ngIf="row.status==4">был(а) {{row.lastLogin | relativeTime}}</p>\n          <button [color]="config.themeColor.primary" (click)="goToChat(row.id)" ion-button icon-only item-end clear>\n            <ion-icon *ngIf="!checkInGroup(row)" name="add-circle"></ion-icon>\n            <ion-icon *ngIf="checkInGroup(row)" name="checkmark-circle" class="colorGreen"></ion-icon>\n          </button>\n        </ion-item>\n      </ion-card>\n\n      <ion-card *ngIf="row.id==0">\n        <ion-item >\n          <h2 >{{row.name}}</h2>\n          <button [color]="config.themeColor.primary"  ion-button (click)="invateSend(row.invatePhone)" item-end mode="ios">\n            Пригласить\n          </button>\n        </ion-item>\n      </ion-card>\n    </ng-container>\n  </ion-list>\n-->\n\n  <div style="clear: both;"></div>\n  <ion-list no-padding *ngIf="peopleList && peopleList.length>0">\n    <ion-list-header  class="headerWho">\n      Глобальный поиск\n    </ion-list-header>\n    <ion-card *ngFor="let row of peopleList ;" >\n      <ion-item *ngIf="row.id!=auth.currentUser.id">\n        <ion-avatar item-start tappable (click)="goToProfile(row.id)">\n          <img src="{{row.avatar}}">\n        </ion-avatar>\n        <h2 tappable (click)="addMan(row)">{{row.name}}</h2>\n        <p tappable (click)="addMan(row)" class="user-status" *ngIf="row.status==1">онлайн</p>\n        <p tappable (click)="addMan(row)" class="user-status" *ngIf="row.status==4">был(а) {{row.lastLogin | relativeTime}}</p>\n        <button [color]="config.themeColor.primary" (click)="addMan(row)" ion-button icon-only item-end clear>\n          <ion-icon *ngIf="!checkInGroup(row)" name="add-circle"></ion-icon>\n          <ion-icon *ngIf="checkInGroup(row)" name="checkmark-circle" class="colorGreen"></ion-icon>\n        </button>\n      </ion-item>\n    </ion-card>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/create-group/create-group.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_12__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__providers_profile__["a" /* ProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_6__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_contacts__["c" /* Contacts */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_8__providers_friend_friend__["a" /* FriendProvider */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_crop__["a" /* Crop */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_img_upload_service__["a" /* ImgUploadServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */]])
    ], CreateGroupPage);
    return CreateGroupPage;
}());

//# sourceMappingURL=create-group.js.map

/***/ }),

/***/ 878:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateGroupPageModule", function() { return CreateGroupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_group__ = __webpack_require__(1052);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_relative_time_module__ = __webpack_require__(435);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CreateGroupPageModule = /** @class */ (function () {
    function CreateGroupPageModule() {
    }
    CreateGroupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__create_group__["a" /* CreateGroupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__create_group__["a" /* CreateGroupPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_relative_time_module__["a" /* RelativeTimeModule */],
            ],
        })
    ], CreateGroupPageModule);
    return CreateGroupPageModule;
}());

//# sourceMappingURL=create-group.module.js.map

/***/ })

});
//# sourceMappingURL=25.js.map