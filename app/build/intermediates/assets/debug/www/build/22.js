webpackJsonp([22],{

/***/ 1034:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupViewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__chat_chat__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_img_upload_service__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_vibration__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_photo_viewer__ = __webpack_require__(188);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the GroupViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GroupViewPage = /** @class */ (function () {
    function GroupViewPage(navCtrl, navParams, auth, chatService, photoViewer, config, menuCtrl, alertCtrl, loadingCtrl, imgUploadService, actionSheetCtrl, platform, camera, vibration) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.chatService = chatService;
        this.photoViewer = photoViewer;
        this.config = config;
        this.menuCtrl = menuCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.imgUploadService = imgUploadService;
        this.actionSheetCtrl = actionSheetCtrl;
        this.platform = platform;
        this.camera = camera;
        this.vibration = vibration;
        this.waitLoading = true;
        this.waitLoadingLine = true;
        this.roomInfo = { name: '', avatar: "assets/img/gp.png", adminid: '', id: '' };
        this.id = this.navParams.get('id');
        if (this.navParams.get('id')) {
            this.getInfo(this.navParams.get('id'));
        }
    }
    GroupViewPage.prototype.getInfo = function (id) {
        var _this = this;
        this.chatService.getUserListInRoom(id).subscribe(function (res) {
            console.log(res);
            _this.waitLoading = false;
            if (res.status) {
                _this.roomInfo = res.room;
                _this.userList = res.list;
            }
        });
    };
    GroupViewPage.prototype.ionViewDidEnter = function () {
        this.getInfo(this.navParams.get('id'));
    };
    GroupViewPage.prototype.goToChat = function (id) {
        var _this = this;
        this.chatService.msgAddRoom(id).subscribe(function (res) {
            if (res.status === true) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__chat_chat__["a" /* Chat */], { roomid: res.roomid });
            }
        });
    };
    GroupViewPage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    GroupViewPage.prototype.addtogroup = function () {
        this.navCtrl.push('AddToGroupPage', { id: this.id });
    };
    GroupViewPage.prototype.viewAvatar = function (avatar, name) {
        this.photoViewer.show(avatar, name);
    };
    GroupViewPage.prototype.editNameGroup = function (id, name) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Изменение названия',
            message: "Введите новое название для группы " + name,
            inputs: [
                {
                    name: 'newnamegroup',
                    placeholder: 'Название группы'
                },
            ],
            buttons: [
                {
                    text: 'Отмена',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Изменить',
                    handler: function (data) {
                        _this.chatService.newNameGroup(id, data.newnamegroup).subscribe(function (res) {
                            if (res.status === true) {
                                _this.roomInfo.name = data.newnamegroup;
                            }
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    GroupViewPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Подождите...',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    GroupViewPage.prototype.showError = function (text) {
        this.loading.dismiss();
        var alert = this.alertCtrl.create({
            title: 'Ошибка',
            subTitle: text,
            buttons: ['Ок']
        });
        alert.present();
    };
    GroupViewPage.prototype.getImageAvatar = function (source, type) {
        var _this = this;
        this.imgUploadService.getImageAvatar(source, type, this.id).then(function (res) {
            if (res) {
                _this.roomInfo.avatar = res;
            }
        });
    };
    GroupViewPage.prototype.choiceTypeAvatarSource = function (type) {
        var _this = this;
        var buttons;
        if (this.platform.is('ios')) {
            buttons = [{
                    text: 'Выбрать из библиотеки',
                    //icon: 'photos',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.PHOTOLIBRARY, type); }
                }, {
                    text: 'Сделать фотографию',
                    // icon: 'camera',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.CAMERA, type); }
                }, {
                    text: 'Отмена',
                    role: 'cancel',
                }];
        }
        else {
            buttons = [{
                    text: 'Выбрать из библиотеки',
                    icon: 'photos',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.PHOTOLIBRARY, type); }
                }, {
                    text: 'Сделать фотографию',
                    icon: 'camera',
                    role: 'destructive',
                    handler: function () { _this.getImageAvatar(_this.camera.PictureSourceType.CAMERA, type); }
                }, {
                    text: 'Отмена',
                    role: 'cancel',
                    icon: 'close',
                }];
        }
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Источник изображения',
            buttons: buttons
        });
        actionSheet.present();
    };
    GroupViewPage.prototype.leaveFromRoom = function () {
        var _this = this;
        this.chatService.leaveFromRoom(this.id, this.auth.currentUser.id).subscribe(function (res) {
            if (res.status) {
                _this.navCtrl.popToRoot();
            }
        });
    };
    GroupViewPage.prototype.holdMan = function (userid) {
        var _this = this;
        if (this.roomInfo.adminid == this.auth.currentUser.id) {
            var actionSheet = this.actionSheetCtrl.create({
                buttons: [
                    {
                        text: 'Назначить администратором',
                        handler: function () {
                            console.log('admin' + userid);
                            _this.chatService.adminChange(_this.id, userid).subscribe(function (res) {
                                if (res.status) {
                                    _this.getInfo(_this.id);
                                }
                            });
                        }
                    },
                    {
                        text: 'Удалить из группы',
                        handler: function () {
                            console.log('admin' + userid);
                            _this.chatService.delFromRoom(_this.id, userid).subscribe(function (res) {
                                console.log(res);
                                if (res.status) {
                                    _this.getInfo(_this.id);
                                }
                            });
                        }
                    },
                    {
                        text: 'Отмена',
                        role: 'cancel',
                        handler: function () {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
    };
    GroupViewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-group-view',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/group-view/group-view.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Информация</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content no-padding>\n  <div ion-fixed class="wait-loading" *ngIf="waitLoading">\n    <ion-spinner name="crescent"></ion-spinner>\n  </div>\n\n  <ion-list class="headerUser">\n    <ion-item class="headerUserInfo groupHeader">\n      <ion-avatar item-start>\n        <div class="avatar" tappable  text-center (click)="viewAvatar(roomInfo.avatar,roomInfo.name)">\n          <img src="{{ roomInfo.avatar }}" alt="">\n        </div>\n      </ion-avatar>\n      <h3 style="margin-bottom:0;" class="nameUserHeader" (click)="editNameGroup(roomInfo.id,roomInfo.name)">{{ roomInfo.name }} </h3>\n    </ion-item>\n    <button (click)="choiceTypeAvatarSource(\'groupavatar\')"  class="buttonUpPhoto"><span> Изменить фото группы</span></button>\n  </ion-list>\n\n\n\n  <p class="labelOnTop" padding *ngIf="userList && (userList.length==1 || userList.length==101)">{{userList.length}} участник</p>\n  <p class="labelOnTop" padding *ngIf="userList && userList.length>=5 && userList.length!=101">{{userList.length}} участников</p>\n  <p class="labelOnTop" padding *ngIf="userList && ((userList.length>1 && userList.length<5) || (userList.length>101 && userList.length<105))">{{userList.length}} участника</p>\n\n  <button (click)="addtogroup()"  class="buttonUp"><ion-icon style="zoom:1.5;" name="person-add"></ion-icon><span> Добавить участников</span></button>\n\n  <ng-container *ngFor="let row of userList;"  no-padding>\n    <ion-card *ngIf="row.id>0" class="cardUsersGroup">\n      <ion-item (press)="holdMan(row.id)">\n        <ion-avatar item-start tappable (click)="goToProfile(row.id)" >\n          <img src="{{row.avatar}}">\n        </ion-avatar>\n        <h2 tappable  (click)="goToProfile(row.id)"  >{{row.id == auth.currentUser.id?\'Вы\':row.name}} <small *ngIf="row.id==roomInfo.adminid" class="admin">( админ )</small></h2>\n        <p tappable (click)="goToProfile(row.id)"  class="user-status" *ngIf="row.status==1">онлайн</p>\n        <p tappable (click)="goToProfile(row.id)"   class="user-status" *ngIf="row.status==4">был(а) {{row.lastLogin | relativeTime}}</p>\n        <button *ngIf="row.id !== auth.currentUser.id" [color]="config.themeColor.primary" (click)="goToChat(row.id)" ion-button icon-only item-end clear>\n          <ion-icon name="chatbubbles"></ion-icon>\n        </button>\n      </ion-item>\n    </ion-card>\n  </ng-container>\n\n   <button (click)="leaveFromRoom()" class="buttonDown"> Удалить и выйти</button>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/group-view/group-view.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_photo_viewer__["a" /* PhotoViewer */],
            __WEBPACK_IMPORTED_MODULE_4__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_img_upload_service__["a" /* ImgUploadServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_vibration__["a" /* Vibration */]])
    ], GroupViewPage);
    return GroupViewPage;
}());

//# sourceMappingURL=group-view.js.map

/***/ }),

/***/ 855:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupViewPageModule", function() { return GroupViewPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__group_view__ = __webpack_require__(1034);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_relative_time_module__ = __webpack_require__(435);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var GroupViewPageModule = /** @class */ (function () {
    function GroupViewPageModule() {
    }
    GroupViewPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__group_view__["a" /* GroupViewPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__group_view__["a" /* GroupViewPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_relative_time_module__["a" /* RelativeTimeModule */],
            ],
        })
    ], GroupViewPageModule);
    return GroupViewPageModule;
}());

//# sourceMappingURL=group-view.module.js.map

/***/ })

});
//# sourceMappingURL=22.js.map