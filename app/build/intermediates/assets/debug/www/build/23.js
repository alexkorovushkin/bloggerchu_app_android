webpackJsonp([23],{

/***/ 1033:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GiveaddPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_chat_service__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the GiveaddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GiveaddPage = /** @class */ (function () {
    function GiveaddPage(navCtrl, alertCtrl, profile, chatService, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.profile = profile;
        this.chatService = chatService;
        this.navParams = navParams;
        this.contact = '';
        this.account = '';
        this.givgift = '';
        this.aboutgiv = '';
        this.subnum = '';
        this.price = '';
        this.duration = '';
        this.dateStart = '';
        this.namegiv = '';
        this.myDate = new Date().toISOString();
        this.bloggerslist = [];
        this.usersgiv = [];
        this.sponsorsgiv = [];
        this.chatService.getBloggers().subscribe(function (res) {
            if (res.status) {
                _this.bloggerslist = res.list;
            }
        });
    }
    GiveaddPage.prototype.deleteFromGroup = function (id) {
        var index = this.findIndexByID(id);
        if (index >= 0) {
            this.usersgiv.splice(index, 1);
        }
    };
    GiveaddPage.prototype.findIndexByID = function (id) {
        return this.usersgiv.findIndex(function (e) { return e.id === id; });
    };
    GiveaddPage.prototype.deleteFromGroupS = function (id) {
        var index = this.findIndexByIDS(id);
        if (index >= 0) {
            this.sponsorsgiv.splice(index, 1);
        }
    };
    GiveaddPage.prototype.findIndexByIDS = function (id) {
        return this.sponsorsgiv.findIndex(function (e) { return e.id === id; });
    };
    GiveaddPage.prototype.addToBaseGive = function () {
        var _this = this;
        var users = [];
        for (var _i = 0, _a = this.usersgiv; _i < _a.length; _i++) {
            var user = _a[_i];
            users.push(user.id);
        }
        var sponsors = [];
        for (var _b = 0, _c = this.sponsorsgiv; _b < _c.length; _b++) {
            var sponsor = _c[_b];
            sponsors.push(sponsor.id);
        }
        this.profile.addToBaseGiveS(this.contact, this.dateStart, this.account, this.givgift, this.aboutgiv, this.subnum, this.price, this.duration, this.namegiv, sponsors, users).subscribe(function (res) {
            if (res.status === true) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Отлично',
                    subTitle: 'Ваш Giv успешно добавлен в систему',
                    buttons: [
                        {
                            text: 'Хорошо',
                            handler: function (data) {
                                _this.navCtrl.popToRoot();
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        });
    };
    GiveaddPage.prototype.addUsers = function () {
        var _this = this;
        var alert = this.alertCtrl.create();
        alert.setTitle('Выберите участников GiveAway?');
        for (var _i = 0, _a = this.bloggerslist; _i < _a.length; _i++) {
            var users = _a[_i];
            alert.addInput({
                type: 'checkbox',
                label: users.name,
                value: users,
                checked: false
            });
        }
        alert.addButton('Отмена');
        alert.addButton({
            text: 'Готово',
            handler: function (data) {
                _this.usersgiv = data;
                console.log(_this.bloggerslist);
                console.log(_this.usersgiv);
            }
        });
        alert.present();
    };
    GiveaddPage.prototype.addSponsors = function () {
        var _this = this;
        var alert = this.alertCtrl.create();
        alert.setTitle('Выберите спонсоров GiveAway?');
        for (var _i = 0, _a = this.bloggerslist; _i < _a.length; _i++) {
            var users = _a[_i];
            alert.addInput({
                type: 'checkbox',
                label: users.name,
                value: users,
                checked: false
            });
        }
        alert.addButton('Отмена');
        alert.addButton({
            text: 'Готово',
            handler: function (data) {
                _this.sponsorsgiv = data;
            }
        });
        alert.present();
    };
    GiveaddPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-giveadd',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/giveadd/giveadd.html"*/'<!--\n  Generated template for the GiveaddPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Добавить Giv</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-item>\n    <ion-label floating>Название:</ion-label>\n    <ion-input required type="text" [(ngModel)]="namegiv"></ion-input>\n  </ion-item>\n  <ion-label>Участники</ion-label>\n\n  <div class="listGroupUsers" *ngIf="usersgiv?.length!=0">\n    <span *ngFor="let row of usersgiv" class="manPlus">\n      <div class="itemManGroup">\n        <ion-avatar item-start class="imageAvatarGroup">\n          <img src="{{row.avatar}}">\n        </ion-avatar>\n          <span class="nameGroup">{{row.name}}</span>\n          <ion-icon (click)="deleteFromGroup(row.id)" class="delGroup" name="close-circle"></ion-icon>\n      </div>\n    </span>\n  </div>\n\n  <button class="buttonUpDanger"  (click)="addUsers()">Добавить участников</button>\n\n\n\n  <ion-item>\n    <ion-label>Старт</ion-label>\n    <ion-datetime  displayFormat="D MMMM YYYY" pickerFormat="D/MMMM/YYYY" cancelText="Отмена" doneText="Готово" min="{{this.myDate}}" max="2038-01-19"\n                   monthNames="январь, февраль, март, апрель, май, июнь, июль, август, сентябрь, октябрь, ноябрь, декабрь"\n                   monthShortNames="янв, фев, март, апр, май, июнь, июль, авг, сен, окт, ноя, дек"\n                   [(ngModel)]="dateStart"\n    ></ion-datetime>\n  </ion-item>\n\n  <ion-item>\n    <ion-label floating>Длительность(дней):</ion-label>\n    <ion-input required type="number" [(ngModel)]="duration"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label floating>Цена:</ion-label>\n    <ion-input required type="number" [(ngModel)]="price"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label floating>Гарант:</ion-label>\n    <ion-input required type="number" [(ngModel)]="subnum"></ion-input>\n  </ion-item>\n\n\n\n  <ion-item>\n    <ion-label floating>Описание:</ion-label>\n    <ion-textarea  elasticTextArea  type="text" [(ngModel)]="aboutgiv"></ion-textarea>\n  </ion-item>\n\n  <ion-item>\n    <ion-label floating>Призы:</ion-label>\n    <ion-textarea elasticTextArea  type="text" [(ngModel)]="givgift"></ion-textarea>\n  </ion-item>\n\n  <ion-item>\n    <ion-label floating>Конкурсный аккаунт:</ion-label>\n    <ion-input required type="text" [(ngModel)]="account"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Контакт для связи:</ion-label>\n    <ion-input required type="text" [(ngModel)]="contact"></ion-input>\n  </ion-item>\n  <ion-label>Спонсоров:  <span *ngIf="sponsorsgiv?.length!=0">{{sponsorsgiv.length}}</span></ion-label>\n\n  <ion-label>Действующие спонсоры</ion-label>\n\n  <div class="listGroupUsers" *ngIf="sponsorsgiv?.length!=0">\n    <span *ngFor="let row of sponsorsgiv" class="manPlus">\n      <div class="itemManGroup">\n        <ion-avatar item-start class="imageAvatarGroup">\n          <img src="{{row.avatar}}">\n        </ion-avatar>\n          <span class="nameGroup">{{row.name}}</span>\n          <ion-icon (click)="deleteFromGroupS(row.id)" class="delGroup" name="close-circle"></ion-icon>\n      </div>\n    </span>\n  </div>\n  <button class="buttonUpDanger"  (click)="addSponsors()">Добавить спонсоров</button>\n\n  <ion-buttons>\n    <button class="addGiv" ion-button (click)="addToBaseGive()">Опубликовать</button>\n  </ion-buttons>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/giveadd/giveadd.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_profile__["a" /* ProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], GiveaddPage);
    return GiveaddPage;
}());

//# sourceMappingURL=giveadd.js.map

/***/ }),

/***/ 854:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiveaddPageModule", function() { return GiveaddPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__giveadd__ = __webpack_require__(1033);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GiveaddPageModule = /** @class */ (function () {
    function GiveaddPageModule() {
    }
    GiveaddPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__giveadd__["a" /* GiveaddPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__giveadd__["a" /* GiveaddPage */]),
            ],
        })
    ], GiveaddPageModule);
    return GiveaddPageModule;
}());

//# sourceMappingURL=giveadd.module.js.map

/***/ })

});
//# sourceMappingURL=23.js.map