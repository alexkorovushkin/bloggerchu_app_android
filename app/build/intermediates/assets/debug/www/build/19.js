webpackJsonp([19],{

/***/ 1038:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaintabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chats_chats__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_chat_service__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MaintabsPage = /** @class */ (function () {
    function MaintabsPage(navCtrl, auth, chatService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.chatService = chatService;
        this.unreadMsg = 0;
        this.tabRoot3 = __WEBPACK_IMPORTED_MODULE_2__chats_chats__["a" /* ChatsPage */];
        this.tabRoot4 = 'ProfileOptionsPage';
        this.tabRoot1 = 'StatementPage';
        this.tabRoot10 = 'CalendarPage';
        this.tabRoot2 = 'SearchPage';
        this.getCountMsg();
        this.updateUnreadMsgCount = setInterval(function () {
            _this.getCountMsg();
        }, 5000);
    }
    MaintabsPage.prototype.getCountMsg = function () {
        var _this = this;
        if (__WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */].getJTW() && __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */].getJTW() !== '' && __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */].getJTW() !== undefined) {
            ////console.log("JTW - ",this.auth.jwt)
            this.chatService
                .unreadCountMsg()
                .subscribe(function (res) {
                ////console.log('Обновление непрочитанных сообщений в меню',res)
                _this.unreadMsg = res.count;
            });
        }
    };
    MaintabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-maintabs',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/maintabs/maintabs.html"*/'<ion-tabs >\n  <ion-tab tabindex="200" tabBadge="{{unreadMsg>0?unreadMsg:null}}" [root]="tabRoot3" tabTitle="Чаты" tabsHideOnSubPages="true" tabIcon="ios-chatbubbles" ></ion-tab><!--\n  <ion-tab tabindex="200" [root]="tabRoot1" tabTitle="Календарь" tabsHideOnSubPages="true" mode="ios" tabIcon="calendar" ></ion-tab>-->\n  <ion-tab tabindex="200" [root]="tabRoot10" tabTitle="Календарь" tabsHideOnSubPages="true" mode="ios" tabIcon="calendar" ></ion-tab>\n  <ion-tab tabindex="200" [root]="tabRoot2" tabTitle="Поиск" tabsHideOnSubPages="true" mode="ios" tabIcon="search" ></ion-tab>\n  <ion-tab tabindex="200" [root]="tabRoot4" tabTitle="Профиль" tabsHideOnSubPages="true" mode="ios" tabIcon="contact" ></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/maintabs/maintabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__providers_chat_service__["a" /* ChatService */]])
    ], MaintabsPage);
    return MaintabsPage;
}());

//# sourceMappingURL=maintabs.js.map

/***/ }),

/***/ 859:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaintabsPageModule", function() { return MaintabsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__maintabs__ = __webpack_require__(1038);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MaintabsPageModule = /** @class */ (function () {
    function MaintabsPageModule() {
    }
    MaintabsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__maintabs__["a" /* MaintabsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__maintabs__["a" /* MaintabsPage */]),
            ]
        })
    ], MaintabsPageModule);
    return MaintabsPageModule;
}());

//# sourceMappingURL=maintabs.module.js.map

/***/ })

});
//# sourceMappingURL=19.js.map