webpackJsonp([29],{

/***/ 1054:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CallinfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_profile__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_line_service__ = __webpack_require__(449);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_friend_friend__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_clipboard__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_photo_viewer__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__chat_chat__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












/**
 * Generated class for the CallinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CallinfoPage = /** @class */ (function () {
    function CallinfoPage(navCtrl, navParams, profile, chatService, viewCtrl, friends, auth, loadingCtrl, lineService, modalCtrl, config, toastCtrl, clipboard, popoverCtrl, browser, platform, actionSheetCtrl, photoViewer, alertCtrl) {
        //console.clear();
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.profile = profile;
        this.chatService = chatService;
        this.viewCtrl = viewCtrl;
        this.friends = friends;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.lineService = lineService;
        this.modalCtrl = modalCtrl;
        this.config = config;
        this.toastCtrl = toastCtrl;
        this.clipboard = clipboard;
        this.popoverCtrl = popoverCtrl;
        this.browser = browser;
        this.platform = platform;
        this.actionSheetCtrl = actionSheetCtrl;
        this.photoViewer = photoViewer;
        this.alertCtrl = alertCtrl;
        this.callstory = [];
        this.followersList = [];
        this.followList = [];
        this.tabsFollow = 'follow';
        this.tabs = 'info';
        this.profileInfo = new __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["b" /* User */]();
        this.serviceList = [];
        this.waitLoading = true;
        this.waitLoadingLine = true;
        this.mapReady = false;
        this.dayName = this.config.dayName;
        this.currentUser = this.auth.currentUser;
        this.userID = this.navParams.get('id');
        if (this.userID) {
            this.showLoading();
            this.getMasterServices(this.userID);
            this.chatService.getCallUs(this.auth.currentUser.id, this.userID).subscribe(function (res) {
                console.log(res);
                if (res.status) {
                    _this.callstory = res.list;
                }
            });
            this.profile.getProfileInfo(this.userID).subscribe(function (res) {
                //console.log(res)
                console.log(res);
                _this.waitLoading = false;
                if (res.status) {
                    _this.profileInfo = res.data;
                    if (_this.profileInfo.type == 2) {
                        _this.profile.getProfileReviews(_this.userID).subscribe(function (res) {
                            _this.masterReviews = res;
                        });
                        //console.log(this.profileInfo.workDay)
                        _this.workDay = _this.profileInfo.workDay.map(function (res) {
                            return _this.dayName[res].name;
                        }).join(', ');
                        if (_this.profileInfo.workDay.length == 7)
                            _this.everyDay = 'ежедневно';
                        //console.log(this.workDay)
                        _this.tabsCollection = [
                            { value: 'info', icon: 'information-circle' },
                            { value: 'line', icon: 'images' },
                            { value: 'service', icon: 'md-list' },
                            { value: 'reviews', icon: 'bookmarks' },
                            { value: 'myfollowers', icon: 'people' },
                        ];
                    }
                    else if (_this.profileInfo.type == 1) {
                        _this.tabsCollection = [
                            { value: 'info', icon: 'information-circle' },
                            { value: 'line', icon: 'images' },
                            { value: 'myfollowers', icon: 'people' },
                        ];
                    }
                    _this.profileNumber = _this.formatPhoneNumber(_this.profileInfo.phone);
                    _this.checkUserStatusInRoom();
                    _this.getLentaImg();
                    _this.getFollowList();
                    _this.getFollowersList();
                    _this.loading.dismissAll();
                }
                else {
                    _this.loading.dismissAll();
                    _this.viewCtrl.dismiss();
                }
            });
        }
        else {
            //console.log('net')
            this.loading.dismissAll();
            this.viewCtrl.dismiss();
            //this.appCtrl.navPop()
            if (this.navCtrl.canGoBack()) {
                this.navCtrl.pop();
            }
        }
    }
    CallinfoPage.prototype.getFollowList = function () {
        var _this = this;
        this.friends.getFollowList(this.userID).subscribe(function (res) {
            _this.followList = res;
        });
    };
    CallinfoPage.prototype.getFollowersList = function () {
        var _this = this;
        this.friends.getFollowersList(this.userID).subscribe(function (res) {
            _this.followersList = res;
        });
    };
    CallinfoPage.prototype.getLentaImg = function () {
        var _this = this;
        this.lineService.getLineListProfile(this.userID).subscribe(function (res) {
            _this.waitLoadingLine = false;
            if (res) {
                _this.lentaImgs = res;
            }
        });
    };
    CallinfoPage.prototype.ionViewWillLeave = function () {
    };
    CallinfoPage.prototype.ionViewWillEnter = function () {
    };
    CallinfoPage.prototype.viewAvatar = function (avatar, name) {
        this.photoViewer.show(avatar, name);
    };
    CallinfoPage.prototype.goToChat = function () {
        var _this = this;
        this.chatService.msgAddRoom(this.userID).subscribe(function (res) {
            if (res.status === true) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__chat_chat__["a" /* Chat */], { roomid: res.roomid });
            }
        });
    };
    CallinfoPage.prototype.checkUserStatusInRoom = function () {
        var _this = this;
        this.chatService
            .checkUserStatusView(this.userID)
            .subscribe(function (res) {
            //console.log(res)
            if (res.status == 'success') {
                _this.roomStatus = res.userinfo.status;
                _this.roomStatusDate = res.userinfo.date;
            }
        });
    };
    CallinfoPage.prototype.formatPhoneNumber = function (s) {
        var s2 = ("" + s).replace(/\D/g, '');
        var m = s2.match(/^(\d{1})(\d{3})(\d{3})(\d{4})$/);
        return (!m) ? "" : "+" + m[1] + " (" + m[2] + ") " + m[3] + "-" + m[4];
    };
    CallinfoPage.prototype.viewPlace = function (city) {
        this.browser.create('https://www.google.com/maps/place/' + city, '_system');
    };
    CallinfoPage.prototype.call = function (number, profileNum) {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: profileNum,
            buttons: [
                {
                    text: 'Позвонить по телефону',
                    handler: function () {
                        setTimeout(function () {
                            _this.browser.create("tel:" + number, '_system');
                        }, 100);
                    }
                },
                {
                    text: 'Позвонить через BuzChat',
                    handler: function () {
                        _this.newcall('audio');
                    }
                },
                {
                    text: 'Отменить',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    CallinfoPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Подождите...',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    CallinfoPage.prototype.addEvent = function (service) {
        if (service === void 0) { service = null; }
        this.navCtrl.push('EventAddPage', { masterID: this.userID, service: service });
    };
    CallinfoPage.prototype.newcall = function (type) {
        console.log(type);
        this.navCtrl.push('CallYesPage', { info: { userid: this.userID, myid: this.auth.currentUser.id, roomid: this.userID + '-' + this.auth.currentUser.id + '-' + new Date().getTime(), type: type, status: 'open' } });
    };
    CallinfoPage.prototype.follow = function () {
        var _this = this;
        if (this.profileInfo.signed) {
            this.friends.followDel(this.userID).subscribe(function (res) {
                if (res.status) {
                    _this.profileInfo.signed = 0;
                }
            });
        }
        else {
            this.friends.followAdd(this.userID).subscribe(function (res) {
                //console.log(res)
                if (res.status) {
                    _this.profileInfo.signed = 1;
                }
            });
        }
    };
    CallinfoPage.prototype.getMasterServices = function (masterID) {
        var _this = this;
        this.profile.getMasterService(masterID).subscribe(function (res) {
            //console.log(res)
            _this.serviceList = res;
        });
    };
    CallinfoPage.prototype.blockusr = function (id) {
        var _this = this;
        this.profile.blockUsrTo(id).subscribe(function (res) {
            if (res.status) {
                _this.profileInfo.block = '1';
            }
        });
    };
    CallinfoPage.prototype.unblockUsr = function (id) {
        var _this = this;
        this.profile.unblockUsrTo(id).subscribe(function (res) {
            if (res.status) {
                _this.profileInfo.block = '0';
            }
        });
    };
    CallinfoPage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    CallinfoPage.prototype.goToNavigator = function () {
        if (this.platform.is('ios')) {
            this.browser.create('maps://?q=' + this.profileInfo.lat + ',' + this.profileInfo.lng, '_system');
            //window.open('maps://?q=' + this.profileInfo.lat + ',' + this.profileInfo.lng, '_system');
        }
        if (this.platform.is('android')) {
            this.browser.create('geo://' + this.profileInfo.lat + ',' + this.profileInfo.lng + '?q=' + this.profileInfo.lat + ',' + this.profileInfo.lng + '(' + this.profileInfo.name + ')', '_system');
            //window.open('geo://' + this.profileInfo.lat + ',' + this.profileInfo.lng + '?q=' + this.profileInfo.lat + ',' + this.profileInfo.lng + '(' + this.profileInfo.name + ')', '_system');
        }
    };
    CallinfoPage.prototype.openLine = function (lentaImg) {
        //console.log(lentaImg)
        this.navCtrl.push('LineViewPage', { lentaImg: lentaImg });
    };
    CallinfoPage.prototype.profilePopover = function (myEvent) {
        var _this = this;
        //console.log(this.profileInfo)
        var popover = this.popoverCtrl.create('ProfilePopoverPage', { profileInfo: this.profileInfo });
        popover.present({
            ev: myEvent,
        });
        popover.onDidDismiss(function (data) {
            if (data) {
                if (data == 'follow')
                    _this.follow();
                else if (data == 'goToChat')
                    _this.goToChat();
                else if (data == 'addEvent')
                    _this.addEvent();
            }
        });
    };
    CallinfoPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            //console.log('Dismissed toast');
        });
        toast.present();
    };
    CallinfoPage.prototype.copuUserLink = function (link) {
        this.clipboard.copy(link);
        this.presentToast(link + ' скопирован');
    };
    CallinfoPage.prototype.delCallStoryUs = function (id) {
        var _this = this;
        this.chatService.dellCall(id).subscribe(function (res) {
            if (res.status) {
                _this.chatService.getCallUs(_this.auth.currentUser.id, _this.userID).subscribe(function (res) {
                    if (res.status) {
                        _this.callstory = res.list;
                    }
                });
            }
        });
    };
    CallinfoPage.prototype.showYes = function (name, id) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Отправить BuzCoin',
            message: "Введите количество BuzCoin для отправки " + name + " Внимание транзакция производится в течениие нескольких секунд и ее нельзя будет отменить!",
            inputs: [
                {
                    name: 'value',
                    type: 'number',
                    placeholder: 'Количество BuzCoin'
                },
            ],
            buttons: [
                {
                    text: 'Отменить',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Отправить',
                    handler: function (data) {
                        _this.profile.sendBuzCoin(id, data.value).subscribe(function (res) {
                            if (res.status) {
                                _this.auth.currentUser.coin = (parseInt(_this.auth.currentUser.coin) - parseInt(data.value)).toString();
                                _this.profileInfo.coin = (parseInt(_this.profileInfo.coin) + parseInt(data.value)).toString();
                            }
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    CallinfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-callinfo',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/callinfo/callinfo.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{ profileInfo.name }}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="goToChat(userID)" *ngIf="userID!=currentUser.id">\n        <ion-icon name="ios-create-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding>\n  <div ion-fixed class="wait-loading" *ngIf="waitLoading">\n    <ion-spinner name="crescent"></ion-spinner>\n  </div>\n\n  <ion-list class="headerUser">\n    <ion-item class="headerUserInfo">\n      <ion-avatar item-start>\n        <div class="avatar" tappable (click)="viewAvatar(profileInfo.avatar,profileInfo.name)" text-center>\n          <img src="{{ profileInfo.avatar }}" alt="">\n        </div>\n      </ion-avatar>\n      <h3 style="margin-bottom:0;" class="nameUserHeader">{{ profileInfo.name }} </h3>\n      <p class="user-status" *ngIf="roomStatus==1">онлайн</p>\n      <p class="user-status" *ngIf="roomStatus==4">был(а) {{roomStatusDate | relativeTime}}</p>\n      <div item-end>\n        <button clear ion-button icon-only (click)="newcall(\'video\')"><ion-icon name="ios-videocam-outline"></ion-icon></button>\n        <button clear ion-button icon-only (click)="newcall(\'audio\')"> <ion-icon name="ios-call-outline"></ion-icon></button>\n      </div>\n    </ion-item>\n  </ion-list>\n  <div>\n    <div class="infoProfile">\n      <span *ngIf="profileInfo.about" class="text-gray" >о себе:</span>\n      <div *ngIf="profileInfo.about" class="textSize-17" >{{ profileInfo.about?profileInfo.about:profileInfo.name+\' не поделился(лась) информацией о себе\' }}</div>\n      <hr *ngIf="profileInfo.about">\n      <span class="text-gray">buzcoin:</span>\n      <div class="textSize-17" >{{profileInfo.coin}}</div>\n      <hr>\n      <span class="text-gray">имя пользователя:</span>\n      <div class="textSize-17"><a class="userLink" (click)="copuUserLink(profileInfo.userLink)">{{profileInfo.userLink}}</a></div>\n      <hr>\n      <span *ngIf="profileInfo.phone" class="text-gray">телефон:</span>\n      <div *ngIf="profileInfo.phone" class="textSize-17" (click)="call(profileInfo.phone,profileNumber)">{{profileNumber}}</div>\n      <hr *ngIf="profileInfo.phone">\n      <span *ngIf="profileInfo.city" class="text-gray">город:</span>\n      <div class="textSize-17" *ngIf="profileInfo.city"> {{profileInfo.city}} <ion-icon name="globe" class="globeUser" (click)="viewPlace(profileInfo.city)"></ion-icon></div>\n      <hr *ngIf="profileInfo.city">\n      <span *ngIf="profileInfo.sex>0" class="text-gray">пол:</span>\n      <div *ngIf="profileInfo.sex==1" class="textSize-17" >Женский</div>\n      <div *ngIf="profileInfo.sex==2" class="textSize-17" >Мужской</div>\n      <hr *ngIf="profileInfo.sex>0">\n    </div>\n  </div>\n\n  <button class="buttonUpNew"  (click)="goToChat(userID)">Отправить сообщение</button>\n  <button class="buttonUpNew"  (click)="goToChat(userID)">Начать секретный чат</button>\n  <button class="buttonUpNew"  (click)="showYes(profileInfo.name,profileInfo.id)">Отправить Buzcoin</button>\n\n  <button *ngIf="profileInfo.block==\'0\'" class="buttonUpDanger" (click)="blockusr(profileInfo.id)"  >Заблокировать пользователя</button>\n  <button *ngIf="profileInfo.block==\'1\'" class="buttonUpDanger" (click)="unblockUsr(profileInfo.id)"  >Разблокировать пользователя</button>\n\n  <ion-list class="callHistory">\n    <ion-list-header>\n      <ion-icon name="call" icon-left></ion-icon> Звонки\n    </ion-list-header>\n    <ng-container *ngFor="let row of callstory">\n      <ion-item-sliding>\n        <ion-item color="none" >\n          <span *ngIf="row.moreoneday==0" item-left class="timeMes">сег. в {{row.date | formatTime:\'HH:mm\'}}</span>\n          <span *ngIf="row.moreoneday==1" item-left class="timeMes">{{row.date | formatTime:\'DD MMM HH:mm\'}}</span>\n          <p>\n            <ion-icon *ngIf="row.status===\'out\' || row.status===\'in\'" [name]="row.type==\'audio\'?\'call\':\'videocam\'"></ion-icon>\n            <ion-icon style="color: #ff6666!important;" *ngIf="row.status===\'missed\'" [name]="row.type==\'audio\'?\'call\':\'videocam\'"></ion-icon>\n            <span *ngIf="row.status===\'out\'">Исходящий</span>\n            <span *ngIf="row.status===\'in\'">Входящий</span>\n            <span style="color: #ff6666" *ngIf="row.status===\'missed\'">Пропущенный</span>\n          </p>\n        </ion-item>\n        <ion-item-options icon-start (ionSwipe)="delCallStoryUs(row.id)">\n          <button class="iconTrash" icon-only expandable (click)="delCallStoryUs(row.id)">\n            <ion-icon name="ios-trash-outline"></ion-icon>\n          </button>\n        </ion-item-options>\n      </ion-item-sliding>\n    </ng-container>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/callinfo/callinfo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_profile__["a" /* ProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_friend_friend__["a" /* FriendProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_line_service__["a" /* LineService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_clipboard__["a" /* Clipboard */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_photo_viewer__["a" /* PhotoViewer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], CallinfoPage);
    return CallinfoPage;
}());

//# sourceMappingURL=callinfo.js.map

/***/ }),

/***/ 880:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallinfoPageModule", function() { return CallinfoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__callinfo__ = __webpack_require__(1054);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic2_rating__ = __webpack_require__(450);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pipes_format_time_module__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_relative_time_module__ = __webpack_require__(435);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var CallinfoPageModule = /** @class */ (function () {
    function CallinfoPageModule() {
    }
    CallinfoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__callinfo__["a" /* CallinfoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__callinfo__["a" /* CallinfoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic2_rating__["a" /* Ionic2RatingModule */],
                __WEBPACK_IMPORTED_MODULE_4__pipes_format_time_module__["a" /* FormatTimeModule */],
                __WEBPACK_IMPORTED_MODULE_5__pipes_relative_time_module__["a" /* RelativeTimeModule */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__callinfo__["a" /* CallinfoPage */]
            ],
            providers: []
        })
    ], CallinfoPageModule);
    return CallinfoPageModule;
}());

//# sourceMappingURL=callinfo.module.js.map

/***/ })

});
//# sourceMappingURL=29.js.map