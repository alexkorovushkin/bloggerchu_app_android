webpackJsonp([17],{

/***/ 1039:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MykassaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the MykassaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MykassaPage = /** @class */ (function () {
    function MykassaPage(navCtrl, auth, alertCtrl, profile, navParams) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.profile = profile;
        this.navParams = navParams;
        this.kpost = null;
        this.kstory = null;
        this.kpoststory = null;
        this.getmykassa();
    }
    MykassaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MykassaPage');
    };
    MykassaPage.prototype.getmykassa = function () {
        var _this = this;
        this.profile.getKassaOn().subscribe(function (res) {
            console.log(res);
            if (res.status === true) {
                _this.kpost = res.data.kpost;
                _this.kstory = res.data.kstory;
                _this.kpoststory = res.data.kpoststory;
            }
            else {
                _this.kpost = null;
                _this.kstory = null;
                _this.kpoststory = null;
            }
        });
    };
    MykassaPage.prototype.saveKassa = function () {
        var _this = this;
        this.profile.saveKassaOn(this.kpost, this.kstory, this.kpoststory).subscribe(function (res) {
            if (res.status === true) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Отлично',
                    subTitle: 'Мы сохранили вашу информацию',
                    buttons: [
                        {
                            text: 'Хорошо',
                            handler: function (data) {
                                _this.navCtrl.popToRoot();
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        });
    };
    MykassaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-mykassa',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/mykassa/mykassa.html"*/'<!--\n  Generated template for the MykassaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Расценки на посты</ion-title>\n    <ion-buttons end>\n      <button end icon-only ion-button (click)="saveKassa()">\n        <ion-icon name="checkmark-circle-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-item>\n    <ion-label floating>Цена за пост:</ion-label>\n    <ion-input  minlength="2" maxlength="60" type="number" [(ngModel)]="kpost"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Цена за историю:</ion-label>\n    <ion-input  minlength="2" maxlength="60" type="number" [(ngModel)]="kstory"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Цена за пост и историю:</ion-label>\n    <ion-input  minlength="2" maxlength="60" type="number" [(ngModel)]="kpoststory"></ion-input>\n  </ion-item>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/mykassa/mykassa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_profile__["a" /* ProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], MykassaPage);
    return MykassaPage;
}());

//# sourceMappingURL=mykassa.js.map

/***/ }),

/***/ 861:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MykassaPageModule", function() { return MykassaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mykassa__ = __webpack_require__(1039);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MykassaPageModule = /** @class */ (function () {
    function MykassaPageModule() {
    }
    MykassaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__mykassa__["a" /* MykassaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__mykassa__["a" /* MykassaPage */]),
            ],
        })
    ], MykassaPageModule);
    return MykassaPageModule;
}());

//# sourceMappingURL=mykassa.module.js.map

/***/ })

});
//# sourceMappingURL=17.js.map