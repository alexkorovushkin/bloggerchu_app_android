webpackJsonp([24],{

/***/ 1032:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GetMediaModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_media_capture__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the GetMediaModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GetMediaModalPage = /** @class */ (function () {
    function GetMediaModalPage(navCtrl, navParams, viewCtrl, modalCtrl, mediaCapture, camera, config) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.mediaCapture = mediaCapture;
        this.camera = camera;
        this.config = config;
    }
    /*  getImage(){
        let options: CaptureImageOptions = { limit: 3 };
        this.mediaCapture.captureImage(options)
          .then(
            (data: MediaFile[]) => console.log(data),
            (err: CaptureError) => console.error(err)
          );
      }*/
    GetMediaModalPage.prototype.closePopover = function () {
        this.viewCtrl.dismiss();
    };
    GetMediaModalPage.prototype.getPhoto = function () {
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            allowEdit: false,
            encodingType: 0
        };
        this.getImageNext(options);
    };
    GetMediaModalPage.prototype.getVideo = function () {
        var _this = this;
        var options = { limit: 1, duration: 59 };
        this.mediaCapture.captureVideo(options)
            .then(function (data) {
            console.log(data);
            _this.goToUploadFile(_this.imageURI, 'video');
        }, function (err) { return console.error(err); });
    };
    GetMediaModalPage.prototype.getLibrary = function () {
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            allowEdit: false,
            encodingType: 0
        };
        this.getImageNext(options);
    };
    GetMediaModalPage.prototype.getAudio = function () {
        var options = { limit: 1, duration: 180 };
        this.mediaCapture.captureAudio(options)
            .then(function (data) { return console.log(data); }, function (err) { return console.error(err); });
    };
    GetMediaModalPage.prototype.getImageNext = function (options) {
        var _this = this;
        this.camera.getPicture(options).then(function (imageData) {
            _this.imageURI = imageData;
            _this.goToUploadFile(_this.imageURI, 'image');
            console.log(imageData);
        }, function (err) {
        });
    };
    GetMediaModalPage.prototype.goToUploadFile = function (mediaURI, type) {
        this.viewCtrl.dismiss({ mediaURI: mediaURI, type: type });
    };
    GetMediaModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-get-media-modal',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/get-media-modal/get-media-modal.html"*/'<ion-content no-padding>\n  <ion-grid text-center class="buttons-collect" no-padding>\n    <ion-row justify-content-center align-items-center>\n      <ion-col col-4>\n        <button ion-button mode="ios" color="primary2" round icon-only (click)="getPhoto()"><ion-icon name="camera"></ion-icon></button>\n      </ion-col>\n<!--      <ion-col col-3>\n        <button ion-button mode="ios" color="primary2" round icon-only (click)="getVideo()"><ion-icon name="videocam"></ion-icon></button>\n      </ion-col>-->\n      <ion-col col-4>\n        <button ion-button mode="ios" color="primary2" round icon-only (click)="getLibrary()"><ion-icon name="image"></ion-icon></button>\n      </ion-col>\n      <ion-col col-4>\n        <button ion-button mode="ios" color="getmedia6" round icon-only (click)="closePopover()"><ion-icon name="ios-arrow-down"></ion-icon></button>\n      </ion-col>\n    </ion-row>\n    <!--<ion-row justify-content-center align-items-center>\n      <ion-col>\n        <button ion-button mode="ios" color="primary2" round icon-only (click)="getAudio()"><ion-icon name="mic"></ion-icon></button>\n      </ion-col>\n      <ion-col>\n        &lt;!&ndash;<button ion-button mode="ios" color="primary2" round icon-only><ion-icon name="pin"></ion-icon></button>&ndash;&gt;\n      </ion-col>\n      <ion-col>\n        <button ion-button mode="ios" color="getmedia6" round icon-only (click)="closePopover()"><ion-icon name="ios-arrow-down"></ion-icon></button>\n      </ion-col>\n    </ion-row>-->\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/get-media-modal/get-media-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_media_capture__["a" /* MediaCapture */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_4__providers_config__["a" /* ConfigProvider */]])
    ], GetMediaModalPage);
    return GetMediaModalPage;
}());

//# sourceMappingURL=get-media-modal.js.map

/***/ }),

/***/ 853:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetMediaModalPageModule", function() { return GetMediaModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__get_media_modal__ = __webpack_require__(1032);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GetMediaModalPageModule = /** @class */ (function () {
    function GetMediaModalPageModule() {
    }
    GetMediaModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__get_media_modal__["a" /* GetMediaModalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__get_media_modal__["a" /* GetMediaModalPage */]),
            ],
        })
    ], GetMediaModalPageModule);
    return GetMediaModalPageModule;
}());

//# sourceMappingURL=get-media-modal.module.js.map

/***/ })

});
//# sourceMappingURL=24.js.map