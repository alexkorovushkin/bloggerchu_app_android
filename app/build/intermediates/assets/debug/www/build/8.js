webpackJsonp([8],{

/***/ 1055:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileViewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chat_chat__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_friend_friend__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_line_service__ = __webpack_require__(449);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_clipboard__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_photo_viewer__ = __webpack_require__(188);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var ProfileViewPage = /** @class */ (function () {
    function ProfileViewPage(navCtrl, navParams, profile, chatService, viewCtrl, friends, auth, loadingCtrl, lineService, modalCtrl, config, toastCtrl, clipboard, popoverCtrl, browser, platform, actionSheetCtrl, photoViewer, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.profile = profile;
        this.chatService = chatService;
        this.viewCtrl = viewCtrl;
        this.friends = friends;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.lineService = lineService;
        this.modalCtrl = modalCtrl;
        this.config = config;
        this.toastCtrl = toastCtrl;
        this.clipboard = clipboard;
        this.popoverCtrl = popoverCtrl;
        this.browser = browser;
        this.platform = platform;
        this.actionSheetCtrl = actionSheetCtrl;
        this.photoViewer = photoViewer;
        this.alertCtrl = alertCtrl;
        this.followersList = [];
        this.followList = [];
        this.tabsFollow = 'follow';
        this.tabs = 'info';
        this.profileInfo = new __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["b" /* User */]();
        this.serviceList = [];
        this.waitLoading = true;
        this.waitLoadingLine = true;
        this.mapReady = false;
        //console.clear();
        this.dayName = this.config.dayName;
        this.currentUser = this.auth.currentUser;
        this.userID = this.navParams.get('id');
        if (this.userID) {
            this.showLoading();
            this.getMasterServices(this.userID);
            this.profile.getProfileInfo(this.userID).subscribe(function (res) {
                //console.log(res)
                console.log(res);
                _this.waitLoading = false;
                if (res.status) {
                    _this.profileInfo = res.data;
                    if (_this.profileInfo.type == 2) {
                        _this.profile.getProfileReviews(_this.userID).subscribe(function (res) {
                            _this.masterReviews = res;
                        });
                        //console.log(this.profileInfo.workDay)
                        _this.workDay = _this.profileInfo.workDay.map(function (res) {
                            return _this.dayName[res].name;
                        }).join(', ');
                        if (_this.profileInfo.workDay.length == 7)
                            _this.everyDay = 'ежедневно';
                        //console.log(this.workDay)
                        _this.tabsCollection = [
                            { value: 'info', icon: 'information-circle' },
                            { value: 'line', icon: 'images' },
                            { value: 'service', icon: 'md-list' },
                            { value: 'reviews', icon: 'bookmarks' },
                            { value: 'myfollowers', icon: 'people' },
                        ];
                    }
                    else if (_this.profileInfo.type == 1) {
                        _this.tabsCollection = [
                            { value: 'info', icon: 'information-circle' },
                            { value: 'line', icon: 'images' },
                            { value: 'myfollowers', icon: 'people' },
                        ];
                    }
                    _this.profileNumber = _this.formatPhoneNumber(_this.profileInfo.phone);
                    _this.checkUserStatusInRoom();
                    _this.getLentaImg();
                    _this.getFollowList();
                    _this.getFollowersList();
                    _this.loading.dismissAll();
                }
                else {
                    _this.loading.dismissAll();
                    _this.viewCtrl.dismiss();
                }
            });
        }
        else {
            //console.log('net')
            this.loading.dismissAll();
            this.viewCtrl.dismiss();
            //this.appCtrl.navPop()
            if (this.navCtrl.canGoBack()) {
                this.navCtrl.pop();
            }
        }
    }
    ProfileViewPage.prototype.getFollowList = function () {
        var _this = this;
        this.friends.getFollowList(this.userID).subscribe(function (res) {
            _this.followList = res;
        });
    };
    ProfileViewPage.prototype.getFollowersList = function () {
        var _this = this;
        this.friends.getFollowersList(this.userID).subscribe(function (res) {
            _this.followersList = res;
        });
    };
    ProfileViewPage.prototype.getLentaImg = function () {
        var _this = this;
        this.lineService.getLineListProfile(this.userID).subscribe(function (res) {
            _this.waitLoadingLine = false;
            if (res) {
                _this.lentaImgs = res;
            }
        });
    };
    ProfileViewPage.prototype.ionViewWillLeave = function () {
    };
    ProfileViewPage.prototype.ionViewWillEnter = function () {
    };
    ProfileViewPage.prototype.viewAvatar = function (avatar, name) {
        this.photoViewer.show(avatar, name);
    };
    ProfileViewPage.prototype.goToChat = function () {
        var _this = this;
        this.chatService.msgAddRoom(this.userID).subscribe(function (res) {
            if (res.status === true) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__chat_chat__["a" /* Chat */], { roomid: res.roomid });
            }
        });
    };
    ProfileViewPage.prototype.checkUserStatusInRoom = function () {
        var _this = this;
        this.chatService
            .checkUserStatusView(this.userID)
            .subscribe(function (res) {
            //console.log(res)
            if (res.status == 'success') {
                _this.roomStatus = res.userinfo.status;
                _this.roomStatusDate = res.userinfo.date;
            }
        });
    };
    ProfileViewPage.prototype.formatPhoneNumber = function (s) {
        var s2 = ("" + s).replace(/\D/g, '');
        var m = s2.match(/^(\d{1})(\d{3})(\d{3})(\d{4})$/);
        return (!m) ? "" : "+" + m[1] + " (" + m[2] + ") " + m[3] + "-" + m[4];
    };
    ProfileViewPage.prototype.viewPlace = function (city) {
        this.browser.create('https://www.google.com/maps/place/' + city, '_system');
    };
    ProfileViewPage.prototype.call = function (number, profileNum) {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: profileNum,
            buttons: [
                {
                    text: 'Позвонить по телефону',
                    handler: function () {
                        setTimeout(function () {
                            _this.browser.create("tel:" + number, '_system');
                        }, 100);
                    }
                },
                {
                    text: 'Позвонить через BuzChat',
                    handler: function () {
                        _this.newcall('audio');
                    }
                },
                {
                    text: 'Отменить',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ProfileViewPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Подождите...',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    ProfileViewPage.prototype.addEvent = function (service) {
        if (service === void 0) { service = null; }
        this.navCtrl.push('EventAddPage', { masterID: this.userID, service: service });
    };
    ProfileViewPage.prototype.goToEvent = function (userid) {
        this.navCtrl.push('WalletPage', { userid: userid });
    };
    ProfileViewPage.prototype.newcall = function (type) {
        this.navCtrl.push('CallYesPage', { info: { userid: this.userID, myid: this.auth.currentUser.id, roomid: this.userID + '-' + this.auth.currentUser.id + '-' + new Date().getTime(), type: type, status: 'open' } });
    };
    ProfileViewPage.prototype.follow = function () {
        var _this = this;
        if (this.profileInfo.signed) {
            this.friends.followDel(this.userID).subscribe(function (res) {
                if (res.status) {
                    _this.profileInfo.signed = 0;
                }
            });
        }
        else {
            this.friends.followAdd(this.userID).subscribe(function (res) {
                //console.log(res)
                if (res.status) {
                    _this.profileInfo.signed = 1;
                }
            });
        }
    };
    ProfileViewPage.prototype.getMasterServices = function (masterID) {
        var _this = this;
        this.profile.getMasterService(masterID).subscribe(function (res) {
            //console.log(res)
            _this.serviceList = res;
        });
    };
    ProfileViewPage.prototype.blockusr = function (id) {
        var _this = this;
        this.profile.blockUsrTo(id).subscribe(function (res) {
            if (res.status) {
                _this.profileInfo.block = '1';
            }
        });
    };
    ProfileViewPage.prototype.unblockUsr = function (id) {
        var _this = this;
        this.profile.unblockUsrTo(id).subscribe(function (res) {
            if (res.status) {
                _this.profileInfo.block = '0';
            }
        });
    };
    ProfileViewPage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    ProfileViewPage.prototype.goToNavigator = function () {
        if (this.platform.is('ios')) {
            this.browser.create('maps://?q=' + this.profileInfo.lat + ',' + this.profileInfo.lng, '_system');
            //window.open('maps://?q=' + this.profileInfo.lat + ',' + this.profileInfo.lng, '_system');
        }
        if (this.platform.is('android')) {
            this.browser.create('geo://' + this.profileInfo.lat + ',' + this.profileInfo.lng + '?q=' + this.profileInfo.lat + ',' + this.profileInfo.lng + '(' + this.profileInfo.name + ')', '_system');
            //window.open('geo://' + this.profileInfo.lat + ',' + this.profileInfo.lng + '?q=' + this.profileInfo.lat + ',' + this.profileInfo.lng + '(' + this.profileInfo.name + ')', '_system');
        }
    };
    ProfileViewPage.prototype.openLine = function (lentaImg) {
        //console.log(lentaImg)
        this.navCtrl.push('LineViewPage', { lentaImg: lentaImg });
    };
    ProfileViewPage.prototype.profilePopover = function (myEvent) {
        var _this = this;
        //console.log(this.profileInfo)
        var popover = this.popoverCtrl.create('ProfilePopoverPage', { profileInfo: this.profileInfo });
        popover.present({
            ev: myEvent,
        });
        popover.onDidDismiss(function (data) {
            if (data) {
                if (data == 'follow')
                    _this.follow();
                else if (data == 'goToChat')
                    _this.goToChat();
                else if (data == 'addEvent')
                    _this.addEvent();
            }
        });
    };
    ProfileViewPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            //console.log('Dismissed toast');
        });
        toast.present();
    };
    ProfileViewPage.prototype.copuUserLink = function (link) {
        this.clipboard.copy(link);
        this.presentToast(link + ' скопирован');
    };
    ProfileViewPage.prototype.abortUser = function (name, id) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Пожаловаться',
            message: "По какой причине вы хотите пожаловаться на " + name + " В кротчайшие сроки администрация рассмотри ваше заявление и буду приняты меры",
            inputs: [
                {
                    name: 'value',
                    type: 'text',
                    placeholder: 'Причина'
                },
            ],
            buttons: [
                {
                    text: 'Отменить',
                    handler: function (data) {
                    }
                },
                {
                    text: 'Отправить',
                    handler: function (data) {
                        _this.profile.sendAbortUser(id, data.value).subscribe(function (res) {
                            if (res.status) {
                                var alert_1 = _this.alertCtrl.create({
                                    title: 'Запрос отправлен',
                                    subTitle: 'Благодарим за то что ты молодец короче(хз че написать)',
                                    buttons: [
                                        {
                                            text: 'Спасибочки',
                                            handler: function () {
                                                _this.navCtrl.pop();
                                            }
                                        }
                                    ]
                                });
                                alert_1.present();
                            }
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    ProfileViewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile-view',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/profile-view/profile-view.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{ profileInfo.name }}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="goToChat(userID)" *ngIf="userID!=currentUser.id">\n        <ion-icon name="ios-create-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding>\n  <div ion-fixed class="wait-loading" *ngIf="waitLoading">\n    <ion-spinner name="crescent"></ion-spinner>\n  </div>\n\n  <ion-list class="headerUser">\n    <ion-item class="headerUserInfo">\n      <ion-avatar item-start>\n        <div class="avatar" tappable (click)="viewAvatar(profileInfo.avatar,profileInfo.name)" text-center>\n          <img src="{{ profileInfo.avatar }}" alt="">\n        </div>\n      </ion-avatar>\n      <h3 style="margin-bottom:0;" class="nameUserHeader">{{ profileInfo.name }} </h3>\n      <p class="user-status" *ngIf="roomStatus==1">онлайн</p>\n      <p class="user-status" *ngIf="roomStatus==4">был(а) {{roomStatusDate | relativeTime}}</p>\n      <!--<div item-end>\n        <button clear ion-button icon-only (click)="newcall(\'video\')"><ion-icon name="ios-videocam-outline"></ion-icon></button>\n        <button clear ion-button icon-only (click)="newcall(\'audio\')"> <ion-icon name="ios-call-outline"></ion-icon></button>\n      </div>-->\n    </ion-item>\n  </ion-list>\n    <div>\n      <div class="infoProfile">\n\n        <span *ngIf="profileInfo.about" class="text-gray" >о себе:</span>\n        <div *ngIf="profileInfo.about" class="textSize-17" >{{ profileInfo.about?profileInfo.about:profileInfo.name+\' не поделился(лась) информацией о себе\' }}</div>\n        <hr *ngIf="profileInfo.about">\n\n        <span class="text-gray">имя пользователя:</span>\n        <div class="textSize-17"><a class="userLink" (click)="copuUserLink(profileInfo.userLink)">{{profileInfo.userLink}}</a></div>\n        <hr>\n\n        <!--<span *ngIf="profileInfo.phone" class="text-gray">телефон:</span>\n        <div *ngIf="profileInfo.phone" class="textSize-17" (click)="call(profileInfo.phone,profileNumber)">{{profileNumber}}</div>\n        <hr *ngIf="profileInfo.phone">-->\n\n        <span *ngIf="profileInfo.city" class="text-gray">город:</span>\n        <div class="textSize-17" *ngIf="profileInfo.city"> {{profileInfo.city}} <ion-icon name="globe" class="globeUser" (click)="viewPlace(profileInfo.city)"></ion-icon></div>\n        <div class="textSize-17" *ngIf="!profileInfo.city">Город не указан</div>\n        <hr *ngIf="profileInfo.city">\n\n        <span *ngIf="profileInfo.sex>0" class="text-gray">пол:</span>\n        <div *ngIf="profileInfo.sex==1" class="textSize-17" >Женский</div>\n        <div *ngIf="profileInfo.sex==2" class="textSize-17" >Мужской</div>\n        <hr *ngIf="profileInfo.sex>0">\n\n      </div>\n    </div>\n  <button class="buttonUpNew"  (click)="goToChat(userID)">Отправить сообщение</button>\n  <button class="buttonUpNew"  (click)="goToEvent(userID)">Заявка на рекламу</button>\n  <br>\n  <button class="buttonUpDanger"  (click)="abortUser(profileInfo.name,userID)">Пожаловаться</button>\n  <button *ngIf="profileInfo.block==\'0\'" class="buttonUpDanger" (click)="blockusr(profileInfo.id)"  >Заблокировать пользователя</button>\n  <button *ngIf="profileInfo.block==\'1\'" class="buttonUpDanger" (click)="unblockUsr(profileInfo.id)"  >Разблокировать пользователя</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/profile-view/profile-view.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_profile__["a" /* ProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_friend_friend__["a" /* FriendProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_line_service__["a" /* LineService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_clipboard__["a" /* Clipboard */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_photo_viewer__["a" /* PhotoViewer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ProfileViewPage);
    return ProfileViewPage;
}());

//# sourceMappingURL=profile-view.js.map

/***/ }),

/***/ 881:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileViewPageModule", function() { return ProfileViewPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_view__ = __webpack_require__(1055);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic2_rating__ = __webpack_require__(450);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pipes_format_time_module__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pipes_relative_time_module__ = __webpack_require__(435);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ProfileViewPageModule = /** @class */ (function () {
    function ProfileViewPageModule() {
    }
    ProfileViewPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_view__["a" /* ProfileViewPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_view__["a" /* ProfileViewPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic2_rating__["a" /* Ionic2RatingModule */],
                __WEBPACK_IMPORTED_MODULE_4__pipes_format_time_module__["a" /* FormatTimeModule */],
                __WEBPACK_IMPORTED_MODULE_5__pipes_relative_time_module__["a" /* RelativeTimeModule */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__profile_view__["a" /* ProfileViewPage */]
            ],
            providers: []
        })
    ], ProfileViewPageModule);
    return ProfileViewPageModule;
}());

//# sourceMappingURL=profile-view.module.js.map

/***/ })

});
//# sourceMappingURL=8.js.map