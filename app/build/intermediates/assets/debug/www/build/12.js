webpackJsonp([12],{

/***/ 1044:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PeoplePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_contacts__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_friend_friend__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_auth_service__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var PeoplePage = /** @class */ (function () {
    function PeoplePage(navCtrl, navParams, profileService, modalCtrl, auth, chatService, config, contacts, browser, friendService, sqlite, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.profileService = profileService;
        this.modalCtrl = modalCtrl;
        this.auth = auth;
        this.chatService = chatService;
        this.config = config;
        this.contacts = contacts;
        this.browser = browser;
        this.friendService = friendService;
        this.sqlite = sqlite;
        this.platform = platform;
        this.searchText = '';
        this.waitLoading = true;
        this.getLocalContacts();
    }
    PeoplePage.prototype.getPeople = function (limitFrom, limitTo, options) {
        var _this = this;
        if (limitFrom === void 0) { limitFrom = 0; }
        if (limitTo === void 0) { limitTo = 10; }
        if (options === void 0) { options = null; }
        return new Promise(function (resolve) {
            _this.profileService.getPeople(limitFrom, limitTo, options).subscribe(function (res) {
                if (!_this.peopleList)
                    _this.peopleList = [];
                //console.log(res)
                for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                    var people = res_1[_i];
                    _this.peopleList.push(people);
                }
                _this.waitLoading = false;
                resolve();
            });
        });
    };
    PeoplePage.prototype.onSearch = function (ev) {
        var _this = this;
        this.peopleList = [];
        //this.waitLoading=true;
        this.searchText = ev.target.value;
        if (this.searchText && this.searchText.trim() != '') {
            if (this.myPeopleList && this.myPeopleList.length > 0) {
                this.myPeopleList = this.myPeopleList.filter(function (item) {
                    console.log(item.name.toLowerCase().indexOf(_this.searchText.toLowerCase()));
                    return (item.name.toLowerCase().indexOf(_this.searchText.toLowerCase()) > -1 || item.invatePhone.indexOf(_this.searchText.toLowerCase()) > -1);
                });
            }
            this.getPeople(0, 10, { search: this.searchText.trim() });
        }
        else {
            this.waitLoading = true;
            this.peopleList = [];
            this.getLocalContacts();
        }
    };
    PeoplePage.prototype.doInfinite = function () {
        return this.getPeople(this.peopleList.length, 10, { search: this.searchText.trim() });
    };
    PeoplePage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    PeoplePage.prototype.inviteFriends = function () {
        this.navCtrl.push('InviteUsersPage');
    };
    PeoplePage.prototype.addContPage = function () {
        this.navCtrl.push('NewContactPage');
    };
    PeoplePage.prototype.goToChat = function (id) {
        var _this = this;
        this.chatService.msgAddRoom(id).subscribe(function (res) {
            if (res.status === true) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__chat_chat__["a" /* Chat */], { roomid: res.roomid });
            }
        });
    };
    PeoplePage.prototype.doRefresh = function (refresher) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            //console.log('Begin async operation', refresher);
            _this.sqlite.create({
                name: 'data.db',
                location: 'default'
            })
                .then(function (db) {
                db.executeSql('DELETE FROM mycontacts')
                    .then(function (delete_res) {
                    console.log('delete_res', delete_res);
                    _this.getLocalContacts();
                });
            });
            refresher.complete();
            resolve();
        });
    };
    PeoplePage.prototype.newcall = function (userid, type) {
        this.navCtrl.push('CallYesPage', { info: { userid: userid, myid: this.auth.currentUser.id, roomid: userid + '-' + this.auth.currentUser.id + '-' + new Date().getTime(), type: type, status: 'open' } });
    };
    PeoplePage.prototype.getLocalContacts = function () {
        var _this = this;
        this.waitLoading = true;
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        })
            .then(function (db) {
            //id INTEGER PRIMARY KEY,
            db.executeSql('CREATE TABLE IF NOT EXISTS mycontacts(id INT, invatePhone TEXT, name TEXT, avatar TEXT, profession TEXT, type INT )')
                .then(function () { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('SELECT * FROM mycontacts ORDER BY id DESC, name ASC')
                .then(function (res) {
                console.log('sqlite_contacts', res);
                _this.myPeopleList = [];
                if (res.rows.length == 0) {
                    _this.contacts.find(['*']).then(function (contacts_res) {
                        _this.myContacts = contacts_res;
                        console.log('contacts_res', _this.myContacts);
                        _this.friendService.saveMyContacts(_this.myContacts).subscribe(function (save_res) {
                            console.log('save_res', save_res);
                            if (save_res.status) {
                                _this.friendService.getMyContacts().subscribe(function (get_contact_res) {
                                    _this.myPeopleList = get_contact_res;
                                    _this.waitLoading = false;
                                    for (var _i = 0, _a = _this.myPeopleList; _i < _a.length; _i++) {
                                        var contact = _a[_i];
                                        db.executeSql('INSERT INTO mycontacts VALUES(?,?,?,?,?,?)', [contact.id, contact.invatePhone, contact.name, contact.avatar, contact.profession, contact.type])
                                            .then(function (insert_res) {
                                            console.log('insert_res', insert_res);
                                        });
                                    }
                                });
                            }
                        });
                    });
                }
                else {
                    _this.waitLoading = false;
                    for (var i = 0; i < res.rows.length; i++) {
                        console.log(res.rows.item(i));
                        _this.myPeopleList.push(res.rows.item(i));
                    }
                }
            })
                .catch(function (e) {
                console.log(e);
                _this.waitLoading = false;
            });
        })
            .catch(function (e) {
            console.log(e);
            _this.waitLoading = false;
        });
    };
    PeoplePage.prototype.invateSend = function (phone) {
        var _this = this;
        setTimeout(function () {
            if (_this.platform.is('ios'))
                _this.browser.create("sms:" + phone + ";body=" + encodeURIComponent('Привет, я использую bloggerchu. Присоединяйся!'), '_system');
            else
                _this.browser.create("sms:" + phone + "?body=" + encodeURIComponent('Привет, я использую bloggerchu. Присоединяйся!'), '_system');
        }, 100);
    };
    PeoplePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-people',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/people/people.html"*/'<ion-header>\n\n  <ion-navbar >\n    <ion-title >\n        Контакты\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="addContPage()">\n        <ion-icon name="add" ></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content no-padding>\n  <ion-searchbar\n    animated="true"\n    placeholder="Поиск"\n    autocomplete="on"\n    autocorrect="on"\n    (ionInput)="onSearch($event)">\n  </ion-searchbar>\n  <div ion-fixed class="wait-loading" *ngIf="waitLoading">\n    <ion-spinner name="crescent"></ion-spinner>\n  </div>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingIcon="arrow-dropdown"\n      pullingText="Обновить контакты"\n      refreshingSpinner="crescent">\n    </ion-refresher-content>\n  </ion-refresher>\n\n\n  <button (click)="inviteFriends()"  class="buttonUp"><ion-icon style="zoom:1.5;" name="person-add"></ion-icon><span> Пригласить</span></button>\n\n  <ion-list no-padding *ngIf="myPeopleList && myPeopleList.length>0">\n    <ion-list-header>\n      Мои контакты\n    </ion-list-header>\n    <ng-container *ngFor="let row of myPeopleList;" >\n      <ion-card *ngIf="row.id>0">\n        <ion-item >\n          <ion-avatar item-start tappable (click)="goToProfile(row.id)">\n            <img src="{{row.avatar}}">\n          </ion-avatar>\n          <h2 tappable (click)="goToChat(row.id)">{{row.name}}</h2>\n          <p tappable (click)="goToChat(row.id)" class="user-status" *ngIf="row.status==1">онлайн</p>\n          <p tappable (click)="goToChat(row.id)" class="user-status" *ngIf="row.status==4">был(а) {{row.lastLogin | relativeTime}}</p>\n          <button [color]="config.themeColor.primary" (click)="goToChat(row.id)" ion-button icon-only item-end clear>\n            <ion-icon name="chatbubbles"></ion-icon>\n          </button>\n          <button [color]="config.themeColor.primary" (click)="newcall(row.id,\'video\')" ion-button icon-only item-end clear>\n            <ion-icon name="videocam"></ion-icon>\n          </button>\n          <button [color]="config.themeColor.primary" (click)="newcall(row.id,\'audio\')" ion-button icon-only item-end clear>\n            <ion-icon name="call"></ion-icon>\n          </button>\n        </ion-item>\n      </ion-card>\n\n      <ion-card *ngIf="row.id==0">\n        <ion-item >\n          <h2 >{{row.name}}</h2>\n          <button [color]="config.themeColor.primary" ion-button (click)="invateSend(row.invatePhone)" item-end mode="ios">\n            Пригласить\n          </button>\n        </ion-item>\n      </ion-card>\n    </ng-container>\n  </ion-list>\n\n  <div style="clear: both;"></div>\n  <ion-list no-padding *ngIf="peopleList && peopleList.length>0">\n    <ion-list-header>\n      Глобальный поиск\n    </ion-list-header>\n    <ion-card *ngFor="let row of peopleList;" >\n      <ion-item >\n        <ion-avatar item-start tappable (click)="goToProfile(row.id)">\n          <img src="{{row.avatar}}">\n        </ion-avatar>\n        <h2 tappable (click)="goToProfile(row.id)">{{row.name}}</h2>\n        <p tappable (click)="goToProfile(row.id)" class="user-status" *ngIf="row.status==1">онлайн</p>\n        <p tappable (click)="goToProfile(row.id)" class="user-status" *ngIf="row.status==4">был(а) {{row.lastLogin | relativeTime}}</p>\n        <button [color]="config.themeColor.primary" (click)="goToChat(row.id)" ion-button icon-only item-end clear>\n          <ion-icon name="chatbubbles"></ion-icon>\n        </button>\n        <button [color]="config.themeColor.primary" (click)="newcall(row.id,\'video\')" ion-button icon-only item-end clear>\n          <ion-icon name="videocam"></ion-icon>\n        </button>\n        <button [color]="config.themeColor.primary" (click)="newcall(row.id,\'audio\')" ion-button icon-only item-end clear>\n          <ion-icon name="call"></ion-icon>\n        </button>\n      </ion-item>\n    </ion-card>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/people/people.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_profile__["a" /* ProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_10__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_contacts__["c" /* Contacts */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_7__providers_friend_friend__["a" /* FriendProvider */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */]])
    ], PeoplePage);
    return PeoplePage;
}());

//# sourceMappingURL=people.js.map

/***/ }),

/***/ 867:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeoplePageModule", function() { return PeoplePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__people__ = __webpack_require__(1044);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_relative_time_module__ = __webpack_require__(435);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PeoplePageModule = /** @class */ (function () {
    function PeoplePageModule() {
    }
    PeoplePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__people__["a" /* PeoplePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__people__["a" /* PeoplePage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_relative_time_module__["a" /* RelativeTimeModule */],
            ],
        })
    ], PeoplePageModule);
    return PeoplePageModule;
}());

//# sourceMappingURL=people.module.js.map

/***/ })

});
//# sourceMappingURL=12.js.map