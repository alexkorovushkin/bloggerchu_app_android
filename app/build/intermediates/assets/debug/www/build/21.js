webpackJsonp([21],{

/***/ 1035:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryActionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_history_history__ = __webpack_require__(458);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HistoryActionPage = /** @class */ (function () {
    function HistoryActionPage(navCtrl, navParams, config, historyService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.config = config;
        this.historyService = historyService;
        this.waitLoading = true;
        this.historyList = [];
        this.getHistoryList();
    }
    HistoryActionPage.prototype.getHistoryList = function (limitFrom, limitTo) {
        var _this = this;
        if (limitFrom === void 0) { limitFrom = 0; }
        if (limitTo === void 0) { limitTo = 30; }
        return new Promise(function (resolve) {
            _this.historyService.getHistoryList(limitFrom, limitTo).subscribe(function (res) {
                //this.historyList=res;
                if (!_this.historyList)
                    _this.historyList = [];
                console.log(res);
                for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                    var history_1 = res_1[_i];
                    _this.historyList.push(history_1);
                }
                _this.waitLoading = false;
                resolve();
            });
        });
    };
    HistoryActionPage.prototype.viewEvent = function (id) {
        this.navCtrl.push('EventViewPage', { eventID: id });
    };
    HistoryActionPage.prototype.openLine = function (id) {
        this.navCtrl.push('LineViewPage', { lineid: id });
    };
    HistoryActionPage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    HistoryActionPage.prototype.doInfinite = function () {
        return this.getHistoryList(this.historyList.length, 30);
    };
    HistoryActionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-history-action',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/history-action/history-action.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Уведомления</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n\n  <div ion-fixed class="wait-loading" *ngIf="waitLoading">\n    <ion-spinner name="crescent"></ion-spinner>\n  </div>\n\n  <ion-list >\n    <ion-card *ngFor="let item of historyList">\n      <ion-item >\n\n        <ion-avatar item-start (click)="goToProfile(item.whoID)">\n          <img [src]="item.whoAvatar">\n        </ion-avatar>\n\n        <h2>{{item.whoName}}</h2>\n        <p>{{item.text}}<br> {{item.date | relativeTime}}</p>\n\n        <ion-thumbnail *ngIf="item.type==\'line_like\' || item.type==\'line_comment\'" item-end (click)="openLine(item.targetid)">\n          <img [src]="item.img">\n        </ion-thumbnail>\n\n        <div item-end *ngIf="item.type==\'event_view\'" >\n          <button class="eye-button" large [color]="config.themeColor.primary" ion-button clear icon-only (click)="viewEvent(item.targetid)"><ion-icon name="eye"></ion-icon></button>\n        </div>\n\n      </ion-item>\n    </ion-card>\n\n  </ion-list>\n\n  <h5 padding text-center *ngIf="!historyList || historyList.length==0">У Вас нет уведомлений</h5>\n\n  <ion-infinite-scroll (ionInfinite)="$event.waitFor(doInfinite())">\n    <ion-infinite-scroll-content loadingSpinner="crescent"></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/history-action/history-action.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_history_history__["a" /* HistoryProvider */]])
    ], HistoryActionPage);
    return HistoryActionPage;
}());

//# sourceMappingURL=history-action.js.map

/***/ }),

/***/ 856:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryActionPageModule", function() { return HistoryActionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history_action__ = __webpack_require__(1035);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_relative_time_module__ = __webpack_require__(435);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var HistoryActionPageModule = /** @class */ (function () {
    function HistoryActionPageModule() {
    }
    HistoryActionPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__history_action__["a" /* HistoryActionPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__pipes_relative_time_module__["a" /* RelativeTimeModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__history_action__["a" /* HistoryActionPage */]),
            ],
        })
    ], HistoryActionPageModule);
    return HistoryActionPageModule;
}());

//# sourceMappingURL=history-action.module.js.map

/***/ })

});
//# sourceMappingURL=21.js.map