webpackJsonp([32],{

/***/ 1024:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CalendarListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_calendar__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CalendarListPage = /** @class */ (function () {
    function CalendarListPage(navCtrl, navParams, calendarService, auth, config, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.calendarService = calendarService;
        this.auth = auth;
        this.config = config;
        this.events = events;
        this.tabs = 'upcoming';
        this.eventListNotCompletedHide = 'display:none';
        this.waitLoading = true;
        this.currentUser = this.auth.currentUser;
        this.eventStatus = this.config.eventStatus;
        this.events.subscribe('events:needUpdatePage', function () {
            _this.eventListUpcoming = [];
            _this.eventListCompleted = [];
            _this.eventListNotCompleted = [];
            _this.getAllMasterEventsUpcoming();
            _this.getAllMasterEventsCompleted();
            _this.getAllMasterEventsNotCompleted();
        });
    }
    CalendarListPage.prototype.ionViewWillEnter = function () {
        this.waitLoading = true;
        this.eventListUpcoming = [];
        this.eventListCompleted = [];
        this.eventListNotCompleted = [];
        this.getAllMasterEventsUpcoming();
        this.getAllMasterEventsCompleted();
        this.getAllMasterEventsNotCompleted();
    };
    CalendarListPage.prototype.getAllMasterEventsUpcoming = function (startFrom) {
        var _this = this;
        if (startFrom === void 0) { startFrom = 0; }
        return new Promise(function (resolve) {
            if (!_this.eventListUpcoming)
                _this.eventListUpcoming = [];
            _this.calendarService.getAllMasterEvents(startFrom, 'upcoming').subscribe(function (res) {
                for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                    var event_1 = res_1[_i];
                    _this.eventListUpcoming.push(event_1);
                }
                resolve();
            });
        });
    };
    CalendarListPage.prototype.getAllMasterEventsCompleted = function (startFrom) {
        var _this = this;
        if (startFrom === void 0) { startFrom = 0; }
        return new Promise(function (resolve) {
            if (!_this.eventListCompleted)
                _this.eventListCompleted = [];
            _this.calendarService.getAllMasterEvents(startFrom, 'completed').subscribe(function (res) {
                for (var _i = 0, res_2 = res; _i < res_2.length; _i++) {
                    var event_2 = res_2[_i];
                    _this.eventListCompleted.push(event_2);
                }
                resolve();
            });
        });
    };
    CalendarListPage.prototype.getAllMasterEventsNotCompleted = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (!_this.eventListNotCompleted)
                _this.eventListNotCompleted = [];
            _this.calendarService.getAllMasterEvents(0, 'notcompleted').subscribe(function (res) {
                _this.waitLoading = false;
                for (var _i = 0, res_3 = res; _i < res_3.length; _i++) {
                    var event_3 = res_3[_i];
                    _this.eventListNotCompleted.push(event_3);
                    _this.tabs = 'notcompleted';
                }
                resolve();
            });
        });
    };
    CalendarListPage.prototype.viewEvent = function (event) {
        this.navCtrl.push('EventViewPage', { event: event });
    };
    CalendarListPage.prototype.doInfiniteUpcoming = function () {
        return this.getAllMasterEventsCompleted(this.eventListUpcoming.length);
    };
    CalendarListPage.prototype.doInfiniteCompleted = function () {
        return this.getAllMasterEventsCompleted(this.eventListCompleted.length);
    };
    CalendarListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-calendar-list',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/calendar-list/calendar-list.html"*/'<!--\n  Generated template for the CalendarListPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar [color]="config.themeColor.headerColor">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Список сеансов</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n  <div ion-fixed class="wait-loading" *ngIf="waitLoading">\n    <ion-spinner name="crescent"></ion-spinner>\n  </div>\n\n  <ion-segment mode="md" [(ngModel)]="tabs" [color]="config.themeColor.segmentColor" *ngIf="!waitLoading">\n    <ion-segment-button value="upcoming" >\n      Предстоящие\n      <ion-badge [color]="config.themeColor.primary" item-end *ngIf="eventListUpcoming?.length>0">{{eventListUpcoming.length}}</ion-badge>\n    </ion-segment-button>\n    <ion-segment-button value="completed" >\n      Завершенные\n    </ion-segment-button>\n    <ion-segment-button style="flex: 0.3;" value="notcompleted" *ngIf="eventListNotCompleted?.length>0">\n      <ion-icon style="color: #ffb300;" name="warning"></ion-icon>\n    </ion-segment-button>\n  </ion-segment>\n\n\n  <div [ngSwitch]="tabs">\n    <div *ngSwitchCase="\'upcoming\'">\n      <ion-list no-padding  *ngIf="eventListUpcoming?.length>0">\n        <ion-card *ngFor="let row of eventListUpcoming" (click)="viewEvent(row)">\n          <ion-item >\n            <ion-avatar item-start>\n              <img src="{{row.userAvatar}}">\n            </ion-avatar>\n            <h2 *ngIf="currentUser.id!=row.userID">{{row.userName}}</h2>\n            <h5 *ngIf="currentUser.id==row.userID">{{row.comment}}</h5>\n            <p>{{row.serviceTitle}}</p>\n          </ion-item>\n          <hr style="margin: 0 10px;">\n          <ion-item>\n            <div class="eventDate" item-start>\n              <ion-icon name="calendar"></ion-icon>\n              <span style="display: inline-block;">{{row.startTime | formatTime:\'DD MMMM YYYY\'}}</span>\n              <ion-icon name="md-time"></ion-icon>\n              <span style="display: inline-block;">{{row.startTime | formatTime:\'HH:mm\'}}</span>\n            </div>\n            <div item-end class="eventStatus {{eventStatus[row.status].color}}">{{eventStatus[row.status].name}}</div>\n          </ion-item>\n        </ion-card>\n      </ion-list>\n      <p text-center *ngIf="!eventListUpcoming || eventListUpcoming.length==0">У Вас нет предстоящих сеансов</p>\n      <ion-infinite-scroll (ionInfinite)="$event.waitFor(doInfiniteUpcoming())">\n        <ion-infinite-scroll-content loadingSpinner="crescent"></ion-infinite-scroll-content>\n      </ion-infinite-scroll>\n    </div>\n    <div *ngSwitchCase="\'completed\'">\n      <ion-list no-padding  *ngIf="eventListCompleted?.length>0">\n        <ion-card *ngFor="let row of eventListCompleted" (click)="viewEvent(row)">\n\n          <ion-item >\n            <ion-avatar item-start>\n              <img src="{{row.userAvatar}}">\n            </ion-avatar>\n\n            <h2 *ngIf="currentUser.id!=row.userID">{{row.userName}}</h2>\n            <h5 *ngIf="currentUser.id==row.userID">{{row.comment}}</h5>\n            <p>{{row.serviceTitle}}</p>\n          </ion-item>\n          <hr style="margin: 0 10px;">\n          <ion-item>\n            <div class="eventDate" item-start>\n              <ion-icon name="calendar"></ion-icon>\n              <span style="display: inline-block;">{{row.startTime | formatTime:\'DD MMMM YYYY\'}}</span>\n              <ion-icon name="md-time"></ion-icon>\n              <span style="display: inline-block;">{{row.startTime | formatTime:\'HH:mm\'}}</span>\n            </div>\n            <div item-end class="eventStatus {{eventStatus[row.status].color}}">{{eventStatus[row.status].name}}</div>\n          </ion-item>\n\n        </ion-card>\n      </ion-list>\n      <p text-center *ngIf="!eventListCompleted || eventListCompleted.length==0">У Вас нет завершенных сеансов</p>\n      <ion-infinite-scroll (ionInfinite)="$event.waitFor(doInfiniteCompleted())">\n        <ion-infinite-scroll-content loadingSpinner="crescent"></ion-infinite-scroll-content>\n      </ion-infinite-scroll>\n    </div>\n\n    <div *ngSwitchCase="\'notcompleted\'">\n      <ion-list no-padding *ngIf="eventListNotCompleted?.length>0">\n        <ion-card *ngFor="let row of eventListNotCompleted" (click)="viewEvent(row)">\n\n          <ion-item >\n            <ion-avatar item-start>\n              <img src="{{row.userAvatar}}">\n            </ion-avatar>\n\n            <h2 *ngIf="currentUser.id!=row.userID">{{row.userName}}</h2>\n            <h5 *ngIf="currentUser.id==row.userID">{{row.comment}}</h5>\n            <p>{{row.serviceTitle}}</p>\n          </ion-item>\n          <hr style="margin: 0 10px;">\n          <ion-item>\n            <div class="eventDate" item-start>\n              <ion-icon name="calendar"></ion-icon>\n              <span style="display: inline-block;">{{row.startTime | formatTime:\'DD MMMM YYYY\'}}</span>\n              <ion-icon name="md-time"></ion-icon>\n              <span style="display: inline-block;">{{row.startTime | formatTime:\'HH:mm\'}}</span>\n            </div>\n            <div item-end class="eventStatus {{eventStatus[row.status].color}}">{{eventStatus[row.status].name}}</div>\n          </ion-item>\n\n        </ion-card>\n      </ion-list>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/calendar-list/calendar-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_calendar__["a" /* CalendarProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */]])
    ], CalendarListPage);
    return CalendarListPage;
}());

//# sourceMappingURL=calendar-list.js.map

/***/ }),

/***/ 847:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarListPageModule", function() { return CalendarListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__calendar_list__ = __webpack_require__(1024);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_format_time_module__ = __webpack_require__(438);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CalendarListPageModule = /** @class */ (function () {
    function CalendarListPageModule() {
    }
    CalendarListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__calendar_list__["a" /* CalendarListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__pipes_format_time_module__["a" /* FormatTimeModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__calendar_list__["a" /* CalendarListPage */]),
            ],
        })
    ], CalendarListPageModule);
    return CalendarListPageModule;
}());

//# sourceMappingURL=calendar-list.module.js.map

/***/ })

});
//# sourceMappingURL=32.js.map