webpackJsonp([3],{

/***/ 1049:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_profile__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_photo_viewer__ = __webpack_require__(188);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WalletPage = /** @class */ (function () {
    function WalletPage(navCtrl, auth, alertCtrl, profile, photoViewer, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.profile = profile;
        this.photoViewer = photoViewer;
        this.navParams = navParams;
        this.profileInfo = new __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["b" /* User */]();
        this.myDate = new Date().toISOString();
        this.siting = '';
        this.dateStart = '';
        this.timeStart = '';
        this.aboutman = '';
        this.Requirements = '';
        this.conditions = '';
        this.userID = this.navParams.get('userid');
        console.log(this.userID);
        this.profile.getProfileInfo(this.userID).subscribe(function (res) {
            _this.profileInfo = res.data;
        });
    }
    WalletPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WalletPage');
    };
    WalletPage.prototype.viewAvatar = function (avatar, name) {
        this.photoViewer.show(avatar, name);
    };
    WalletPage.prototype.saveOrder = function () {
        var _this = this;
        this.profile.sendOrder(this.siting, this.dateStart, this.timeStart, this.aboutman, this.Requirements, this.conditions, this.profileInfo.id, this.auth.currentUser.name).subscribe(function (res) {
            if (res.status === true) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Отлично',
                    subTitle: 'Ваш заказ принят в обработку',
                    buttons: [
                        {
                            text: 'Хорошо',
                            handler: function (data) {
                                _this.navCtrl.popToRoot();
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        });
    };
    WalletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-wallet',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/wallet/wallet.html"*/'<!--\n  Generated template for the WalletPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Заказ для {{profileInfo.name}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n\n  <ion-list class="headerUser">\n    <ion-item class="headerUserInfo">\n      <ion-avatar item-start>\n        <div class="avatar" tappable (click)="viewAvatar(profileInfo.avatar,profileInfo.name)" text-center>\n          <img src="{{ profileInfo.avatar }}" alt="">\n        </div>\n      </ion-avatar>\n      <h3 style="margin-bottom:0;" class="nameUserHeader">{{ profileInfo.name }} </h3>\n      <h4 style="margin-bottom:0;" class="nameUserHeader" *ngIf="profileInfo.city !== null">{{ profileInfo.city }} </h4>\n      <h4 style="margin-bottom:0;" class="nameUserHeader" *ngIf="profileInfo.city === null">Город не указан </h4>\n    </ion-item>\n  </ion-list>\n  <ion-item>\n    <ion-label floating>Размещение: </ion-label>\n    <ion-select cancelText="Отмена" okText="Выбрать" [(ngModel)]="siting">\n      <ion-option value="История">История</ion-option>\n      <ion-option value="Пост">Пост</ion-option>\n      <ion-option value="История и пост">История и пост</ion-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label>Дата</ion-label>\n    <ion-datetime  displayFormat="D MMMM YYYY" pickerFormat="D/MMMM/YYYY" cancelText="Отмена" doneText="Готово" min="{{this.myDate}}" max="2038-01-19"\n                  monthNames="январь, февраль, март, апрель, май, июнь, июль, август, сентябрь, октябрь, ноябрь, декабрь"\n                  monthShortNames="янв, фев, март, апр, май, июнь, июль, авг, сен, окт, ноя, дек"\n                  [(ngModel)]="dateStart"\n    ></ion-datetime>\n  </ion-item>\n  <ion-item>\n    <ion-label>Время</ion-label>\n    <ion-datetime  cancelText="Отмена" doneText="Готово" displayFormat="H:mm" pickerFormat="H:m"\n                  [(ngModel)]="timeStart"></ion-datetime>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>О себе:</ion-label>\n    <ion-input type="text" name="about"\n               [(ngModel)]="aboutman"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Требования к публикации:</ion-label>\n    <ion-input type="text" name="about"\n               [(ngModel)]="Requirements"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Особые условия: </ion-label>\n    <ion-select cancelText="Отмена" okText="Выбрать"\n                [(ngModel)]="conditions">\n      <ion-option value="Не выбраны">Не выбраны</ion-option>\n      <ion-option value="Бартер">Бартер</ion-option>\n      <ion-option value="ВП">ВП</ion-option>\n    </ion-select>\n  </ion-item>\n  <button class="button-next" mode="ios" type="submit" ion-button color="stable" block (click)="saveOrder()">\n    Отправить запрос\n  </button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/wallet/wallet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_profile__["a" /* ProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_photo_viewer__["a" /* PhotoViewer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], WalletPage);
    return WalletPage;
}());

//# sourceMappingURL=wallet.js.map

/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPageModule", function() { return WalletPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__wallet__ = __webpack_require__(1049);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var WalletPageModule = /** @class */ (function () {
    function WalletPageModule() {
    }
    WalletPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__wallet__["a" /* WalletPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__wallet__["a" /* WalletPage */]),
            ],
        })
    ], WalletPageModule);
    return WalletPageModule;
}());

//# sourceMappingURL=wallet.module.js.map

/***/ })

});
//# sourceMappingURL=3.js.map