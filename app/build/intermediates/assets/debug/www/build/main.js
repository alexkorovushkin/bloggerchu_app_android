webpackJsonp([43],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Chat; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_date_fns_format__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_date_fns_format___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_date_fns_format__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_keyboard__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_photo_viewer__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_vibration__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_media__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_file__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_camera__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_media_capture__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_ng_socket_io__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_clipboard__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_sqlite__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_in_app_browser__ = __webpack_require__(185);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import {NativeAudio} from "@ionic-native/native-audio";
















//@IonicPage()
var Chat = /** @class */ (function () {
    function Chat(platform, navParams, chatService, navCtrl, config, events, auth, 
        //private nativeAudio: NativeAudio,
        modalCtrl, geolocation, popoverCtrl, browser, toastCtrl, transfer, loadingCtrl, keyboard, photoViewer, vibration, media, sqlite, actionSheetCtrl, mediaCapture, socket, camera, clipboard, file, sanitizer) {
        var _this = this;
        this.platform = platform;
        this.chatService = chatService;
        this.navCtrl = navCtrl;
        this.config = config;
        this.events = events;
        this.auth = auth;
        this.modalCtrl = modalCtrl;
        this.geolocation = geolocation;
        this.popoverCtrl = popoverCtrl;
        this.browser = browser;
        this.toastCtrl = toastCtrl;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.keyboard = keyboard;
        this.photoViewer = photoViewer;
        this.vibration = vibration;
        this.media = media;
        this.sqlite = sqlite;
        this.actionSheetCtrl = actionSheetCtrl;
        this.mediaCapture = mediaCapture;
        this.socket = socket;
        this.camera = camera;
        this.clipboard = clipboard;
        this.file = file;
        this.sanitizer = sanitizer;
        this.giflist = [];
        this.stclist = [];
        this.chatMyList = [];
        this.roomAvatar = 'assets/img/profile.png';
        this.waitLoading = true;
        this.progressUploadMedia = false;
        this.gif = false;
        this.stc = false;
        this.recordingAudio = false;
        this.isModal = false;
        this.countMsg = 0;
        //private countMsgView=0;
        //private countUnread=0;
        //private updateMsgCount;
        this.loading = false;
        this.msgList = [];
        //user: UserInfo;
        this.editorMsg = '';
        this.editorMsgStart = '';
        this.textMessageAnswer = '';
        this.answerMsgStart = '';
        this.messageIdstart = '';
        this.answerMsgName = '';
        this.imageMsg = '';
        this.videoMsg = '';
        this.showEmojiPicker = false;
        this.recordOn = false;
        this.platform.ready().then(function () {
            if (platform.is('ios')) {
                var appEl_1 = (document.getElementsByTagName('ION-APP')[0]), appElHeight_1 = appEl_1.clientHeight;
                keyboard.disableScroll(true);
                window.addEventListener('native.keyboardshow', function (e) {
                    appEl_1.style.height = (appElHeight_1 - e.keyboardHeight) + 'px';
                });
                window.addEventListener('native.keyboardhide', function () {
                    appEl_1.style.height = '100%';
                });
            }
        });
        this.roomID = navParams.get("roomid");
        this.roomName = navParams.get("roomname");
        this.updCurRoom(this.roomID);
        this.chatService.connectSocket(this.roomID);
        //this.nativeAudio.preloadComplex('beepGetSend', 'assets/sounds/water_droplet.mp3', 1, 3, 0);
        //this.nativeAudio.preloadComplex('beepSendSuccess', 'assets/sounds/bloop.mp3', 1, 1, 0);
        this.checkUserStatusInRoom();
        this.resumeEvent = platform.resume.subscribe(function (e) {
            console.log("resume called in roomid-" + _this.roomID);
            _this.updCurRoom(_this.roomID);
            _this.getMsg();
            _this.chatService.connectSocket(_this.roomID);
            _this.chatService.setReadMessages();
            _this.checkUserStatusInterval = setInterval(function () {
                _this.checkUserStatusInRoom();
            }, 5000);
        });
        this.pauseEvent = platform.pause.subscribe(function (e) {
            console.log("pause called in roomid-" + _this.roomID);
            _this.updCurRoom(0);
            _this.chatService.disconectSocket();
            clearInterval(_this.checkUserStatusInterval);
        });
        this.checkUserStatusInterval = setInterval(function () {
            _this.checkUserStatusInRoom();
        }, 5000);
        this.getNewMessages = this.chatService.getNewMessages().subscribe(function (message) {
            var index_msg = _this.getMsgIndexById(message.messageId);
            //console.log(index_msg)
            if (index_msg >= 0) {
                _this.msgList[index_msg].status = 'success';
                //this.nativeAudio.play('beepSendSuccess');
            }
            else {
                message.status = 'success';
                _this.pushNewMsg(message);
                // this.nativeAudio.play('beepGetSend');
            }
            if (message.userid != _this.auth.currentUser.id) {
                setTimeout(function () {
                    _this.chatService.setReadMessages(_this.roomID).subscribe(function (data) {
                        //console.log('респонс от запроса прочитанных сообщений',data)
                    });
                }, 3000);
            }
        });
        this.getReadMessages = this.chatService.getReadMessages().subscribe(function (data) {
            //console.log(data)
            if (_this.auth.currentUser.id != data.userid) {
                for (var i = 0; i < _this.msgList.length; i++) {
                    if (_this.msgList[i].userid == _this.auth.currentUser.id)
                        _this.msgList[i].status = 'read';
                }
            }
        });
        this.chatService.getGif().subscribe(function (res) {
            if (res.status) {
                console.log('gif', res);
                _this.giflist = res.gif;
            }
        });
        this.chatService.getStc().subscribe(function (res) {
            if (res.status) {
                console.log('stick', res);
                _this.stclist = res.stc;
            }
        });
    }
    Chat.prototype.ionViewDidLoad = function () {
        this.getMsg();
        //console.log('event - ionViewDidLoad')
    };
    Chat.prototype.ionViewWillEnter = function () {
        //console.log('event - ionViewWillEnter')
    };
    Chat.prototype.ionViewDidLeave = function () {
        //console.log('event ionViewDidLeave')
    };
    Chat.prototype.ionViewWillLeave = function () {
        //console.log('event ionViewWillLeave')
        clearInterval(this.checkUserStatusInterval);
        //this.nativeAudio.unload('beepGetSend');
        //this.nativeAudio.unload('beepSendSuccess');
        this.resumeEvent.unsubscribe();
        this.pauseEvent.unsubscribe();
        this.getNewMessages.unsubscribe();
        this.getReadMessages.unsubscribe();
        // unsubscribe
        this.chatService.disconectSocket();
        this.updCurRoom(0);
        this.events.unsubscribe('chat:received');
        //clearInterval(this.updateMsgCount);
        //this.navCtrl.pop()
    };
    Chat.prototype.ionViewDidEnter = function () {
        var _this = this;
        //console.log('event - ionViewDidEnter')
        //get message list
        this.chatService.setReadMessages();
        // Subscribe to received  new message events
        this.events.subscribe('chat:received', function (msg) {
            _this.pushNewMsg(msg);
        });
    };
    Chat.prototype.onFocus = function () {
        this.showEmojiPicker = false;
        this.content.resize();
        this.scrollToBottom();
    };
    Chat.prototype.switchEmojiPicker = function () {
        this.showEmojiPicker = !this.showEmojiPicker;
        if (!this.showEmojiPicker) {
            this.messageInput.setFocus();
        }
        this.content.resize();
        this.scrollToBottom();
    };
    /**
     * @name getMsg
     * @returns {Promise<ChatMessage[]>}
     */
    Chat.prototype.getMsg = function (startFrom, limitTo, direct) {
        var _this = this;
        if (startFrom === void 0) { startFrom = 0; }
        if (limitTo === void 0) { limitTo = 30; }
        if (direct === void 0) { direct = 'up'; }
        var tmp;
        return this.chatService
            .getMsgListIfExt(this.roomID, this.auth.currentUser.id, startFrom, limitTo)
            .subscribe(function (res) {
            console.log('сообщения', res);
            tmp = res;
            var toBottom = true;
            if (startFrom == 0)
                _this.msgList = [];
            if (_this.msgList.length > 0)
                toBottom = false;
            _this.roomAvatar = tmp.roomInfo.avatar;
            _this.countMsg = tmp.roomInfo.count;
            _this.roomName = tmp.roomInfo.name;
            _this.roomUserID = tmp.roomInfo.userid;
            _this.roomUsers = tmp.roomInfo.users;
            //this.msgList = tmp.msgList;
            for (var _i = 0, _a = tmp.msgList; _i < _a.length; _i++) {
                var row = _a[_i];
                if (direct == 'up')
                    _this.msgList.splice(0, 0, row);
                else
                    _this.msgList.push(row);
            }
            _this.loading = false;
            if (toBottom)
                _this.scrollToBottom(0);
            setTimeout(function () {
                _this.waitLoading = false;
            }, 205);
        });
    };
    /**
     * @name sendMsg
     */
    Chat.prototype.sendMsg = function () {
        var _this = this;
        if (!this.editorMsg.trim())
            return;
        var id = Date.now().toString();
        var tmpDate = new Date();
        var newMsg = {
            messageId: this.roomID + '_' + id,
            userid: this.auth.currentUser.id,
            name: this.auth.currentUser.name,
            avatar: this.auth.currentUser.avatar,
            date: __WEBPACK_IMPORTED_MODULE_4_date_fns_format___default()(tmpDate.getUTCFullYear().toString() + '-' + ('0' + (tmpDate.getMonth() + 1)).slice(-2).toString() + '-' + ('0' + tmpDate.getDate()).slice(-2).toString() + ' ' + ('0' + tmpDate.getUTCHours()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCMinutes()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCSeconds()).slice(-2).toString(), 'YYYY-MM-DD HH:mm:ss'),
            //date: format(new Date().toUTCString(), 'YYYY-MM-DD HH:mm:ss'),
            //date: new Date().toISOString(),
            text: this.editorMsg,
            status: 'pending',
            image: this.imageMsg,
            video: this.videoMsg,
            audio: '',
            audioPlaying: false,
            audioOnPause: false,
            audioDuration: '',
            typeMsg: 'simple'
        };
        this.editorMsg = '';
        this.pushNewMsg(newMsg);
        this.countMsg++;
        if (!this.showEmojiPicker) {
            this.messageInput.setFocus();
        }
        this.chatService.sendMsg(newMsg, this.roomID)
            .subscribe(function (res) {
            //console.log(res)
            var index = _this.getMsgIndexById(id);
            if (index !== -1 && res.status == 'success') {
                //this.msgList[index].status = 'success';
            }
        });
    };
    Chat.prototype.sendGif = function (gif) {
        var _this = this;
        console.log(gif);
        var id = Date.now().toString();
        var tmpDate = new Date();
        var newMsg = {
            messageId: this.roomID + '_' + id,
            userid: this.auth.currentUser.id,
            name: this.auth.currentUser.name,
            avatar: this.auth.currentUser.avatar,
            date: __WEBPACK_IMPORTED_MODULE_4_date_fns_format___default()(tmpDate.getUTCFullYear().toString() + '-' + ('0' + (tmpDate.getMonth() + 1)).slice(-2).toString() + '-' + ('0' + tmpDate.getDate()).slice(-2).toString() + ' ' + ('0' + tmpDate.getUTCHours()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCMinutes()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCSeconds()).slice(-2).toString(), 'YYYY-MM-DD HH:mm:ss'),
            //date: format(new Date().toUTCString(), 'YYYY-MM-DD HH:mm:ss'),
            //date: new Date().toISOString(),
            text: gif,
            status: 'pending',
            image: this.imageMsg,
            video: this.videoMsg,
            audio: '',
            audioPlaying: false,
            audioOnPause: false,
            audioDuration: '',
            typeMsg: 'gif'
        };
        this.editorMsg = '';
        this.pushNewMsg(newMsg);
        this.countMsg++;
        if (!this.showEmojiPicker) {
            this.messageInput.setFocus();
        }
        this.gif = false;
        this.chatService.sendMsg(newMsg, this.roomID)
            .subscribe(function (res) {
            //console.log(res)
            var index = _this.getMsgIndexById(id);
            if (index !== -1 && res.status == 'success') {
                //this.msgList[index].status = 'success';
            }
        });
    };
    Chat.prototype.sendStc = function (stick) {
        var _this = this;
        console.log(stick);
        var id = Date.now().toString();
        var tmpDate = new Date();
        var newMsg = {
            messageId: this.roomID + '_' + id,
            userid: this.auth.currentUser.id,
            name: this.auth.currentUser.name,
            avatar: this.auth.currentUser.avatar,
            date: __WEBPACK_IMPORTED_MODULE_4_date_fns_format___default()(tmpDate.getUTCFullYear().toString() + '-' + ('0' + (tmpDate.getMonth() + 1)).slice(-2).toString() + '-' + ('0' + tmpDate.getDate()).slice(-2).toString() + ' ' + ('0' + tmpDate.getUTCHours()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCMinutes()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCSeconds()).slice(-2).toString(), 'YYYY-MM-DD HH:mm:ss'),
            //date: format(new Date().toUTCString(), 'YYYY-MM-DD HH:mm:ss'),
            //date: new Date().toISOString(),
            text: stick,
            status: 'pending',
            image: this.imageMsg,
            video: this.videoMsg,
            audio: '',
            audioPlaying: false,
            audioOnPause: false,
            audioDuration: '',
            typeMsg: 'stick'
        };
        this.editorMsg = '';
        this.pushNewMsg(newMsg);
        this.countMsg++;
        if (!this.showEmojiPicker) {
            this.messageInput.setFocus();
        }
        this.stc = false;
        this.chatService.sendMsg(newMsg, this.roomID)
            .subscribe(function (res) {
            //console.log(res)
            var index = _this.getMsgIndexById(id);
            if (index !== -1 && res.status == 'success') {
                //this.msgList[index].status = 'success';
            }
        });
    };
    /**
     * @name pushNewMsg
     * @param msg
     */
    Chat.prototype.pushNewMsg = function (msg) {
        console.log('сообщение добавлено в массив', msg);
        try {
            this.msgList.push(msg);
        }
        catch (e) {
            console.log(e);
            console.log('ERROR -----------------------');
        }
        console.log(this.msgList);
        this.scrollToBottom();
    };
    Chat.prototype.getMsgIndexById = function (id) {
        return this.msgList.findIndex(function (e) { return e.messageId === id; });
    };
    Chat.prototype.scrollToBottom = function (duration) {
        var _this = this;
        if (duration === void 0) { duration = 300; }
        setTimeout(function () {
            if (_this.content.scrollToBottom) {
                //if(this.content._scroll)
                _this.content.scrollToBottom(duration);
            }
        }, 200);
    };
    /*private getCountMsg() {
      return this.chatService
        .checkCountMsg(this.roomID)
        .subscribe(res => {
          ////console.log(res)
          //console.log('Обновление количества сообщений чата',res)
          if(this.countMsg!=res.count) {
            this.getMsg(0,res.count-this.countMsg, 'down')
            this.scrollToBottom(0)
          }
          this.countMsg=res.count;
          this.countMsgView = res.countView;
  
        });
    }*/
    Chat.prototype.checkUserStatusInRoom = function () {
        var _this = this;
        this.chatService
            .checkUserStatus(this.roomID)
            .subscribe(function (res) {
            //console.log(res)
            if (res.status == 'success') {
                _this.roomStatus = res.userinfo.status;
                _this.roomStatusDate = res.userinfo.date;
            }
        });
    };
    Chat.prototype.updCurRoom = function (roomid) {
        return this.chatService
            .updateCurrentRoom(roomid)
            .subscribe(function (res) {
            ////console.log(res)
            //console.log('Обновление текущей комнаты',res)
        });
    };
    Chat.prototype.doRefresh = function (refresher) {
        //console.log('Begin async operation', refresher);
        setTimeout(function () {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 500);
    };
    Chat.prototype.doInfinite = function (event) {
        //console.log(event)
        //console.log('Begin async operation');
        var _this = this;
        return new Promise(function (resolve) {
            //this.getMsg(this.msgList.length)
            var tmp;
            return _this.chatService
                .getMsgList(_this.roomID, _this.auth.currentUser.id, _this.msgList.length)
                .subscribe(function (res) {
                //console.log(res)
                tmp = res;
                for (var _i = 0, _a = tmp.msgList; _i < _a.length; _i++) {
                    var row = _a[_i];
                    _this.msgList.splice(0, 0, row);
                }
                //console.log(this.msgList)
                resolve();
            });
        });
    };
    Chat.prototype.goToProfile = function () {
        if (this.roomUserID) {
            this.navCtrl.push('ProfileViewPage', { id: this.roomUserID });
            /*      let modal = this.modalCtrl.create('ProfileViewPage', {id: this.roomUserID});
                  modal.present();*/
        }
        else if (this.roomUsers) {
            this.navCtrl.push('GroupViewPage', { id: this.roomID });
        }
    };
    Chat.prototype.goToProfileA = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    Chat.prototype.goToLocate = function (loc, name) {
        loc = JSON.parse(loc);
        var destination = loc.lat + ',' + loc.lng;
        if (this.platform.is('ios')) {
            window.open('maps://?q=' + destination, '_system');
        }
        else {
            var label = encodeURI(name);
            window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
        }
    };
    Chat.prototype.openmap = function () {
        var _this = this;
        this.isModal = true;
        var modal = this.modalCtrl.create('MastersMapPage');
        modal.present();
        modal.onDidDismiss(function (loc) {
            _this.isModal = false;
            if (loc) {
                console.log(loc);
                var id = Date.now().toString();
                var tmpDate = new Date();
                var newMsg = {
                    messageId: _this.roomID + '_' + id,
                    userid: _this.auth.currentUser.id,
                    name: _this.auth.currentUser.name,
                    avatar: _this.auth.currentUser.avatar,
                    date: __WEBPACK_IMPORTED_MODULE_4_date_fns_format___default()(tmpDate.getUTCFullYear().toString() + '-' + ('0' + (tmpDate.getMonth() + 1)).slice(-2).toString() + '-' + ('0' + tmpDate.getDate()).slice(-2).toString() + ' ' + ('0' + tmpDate.getUTCHours()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCMinutes()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCSeconds()).slice(-2).toString(), 'YYYY-MM-DD HH:mm:ss'),
                    //date: format(new Date().toUTCString(), 'YYYY-MM-DD HH:mm:ss'),
                    //date: new Date().toISOString(),
                    text: JSON.stringify(loc.latLng),
                    status: 'pending',
                    image: '',
                    video: '',
                    audio: '',
                    audioPlaying: false,
                    audioOnPause: false,
                    audioDuration: '',
                    typeMsg: 'geo'
                };
                _this.chatService.sendMsg(newMsg, _this.roomID)
                    .subscribe(function (res) {
                });
            }
        });
        //this.navCtrl.push(MastersMapPage, {callback: this.location});
    };
    Chat.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            //console.log('Dismissed toast');
        });
        toast.present();
    };
    Chat.prototype.uploadVideoFile = function (type) {
        var _this = this;
        var id = Date.now().toString();
        var tmpDate = new Date();
        var image = '';
        var video = '';
        var audio = '';
        var loader = this.loadingCtrl.create({
            content: "Отправка видео..."
        });
        loader.present();
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'file',
            fileName: 'ionicfile.mp4',
            chunkedMode: false,
            mimeType: "video/mp4",
            headers: {
                'Authorization': 'Bearer ' + __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */].jwt2
            }
        };
        if (type == 'video')
            video = this.mediaURI;
        var newMsg = {
            messageId: this.roomID + '_' + id,
            userid: this.auth.currentUser.id,
            name: this.auth.currentUser.name,
            avatar: this.auth.currentUser.avatar,
            date: __WEBPACK_IMPORTED_MODULE_4_date_fns_format___default()(tmpDate.getUTCFullYear().toString() + '-' + ('0' + (tmpDate.getMonth() + 1)).slice(-2).toString() + '-' + ('0' + tmpDate.getDate()).slice(-2).toString() + ' ' + ('0' + tmpDate.getUTCHours()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCMinutes()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCSeconds()).slice(-2).toString(), 'YYYY-MM-DD HH:mm:ss'),
            //date: format(new Date().toUTCString(), 'YYYY-MM-DD HH:mm:ss'),
            //date: new Date().toISOString(),
            text: this.editorMsg,
            status: 'pending',
            image: image,
            video: video,
            audio: audio,
            audioPlaying: false,
            audioOnPause: false,
            audioDuration: '',
            typeMsg: 'simple'
        };
        fileTransfer.upload(this.imageURI, this.config.apiURL + 'fn/upload_file.php?type=' + type + '&roomid=' + this.roomID, options)
            .then(function (data) {
            //console.log(data);
            _this.pushNewMsg(newMsg);
            console.log(data);
            var tmp_data = JSON.parse(data.response);
            console.log(tmp_data);
            if (tmp_data.isSuccess) {
                var index = _this.getMsgIndexById(newMsg.messageId);
                _this.msgList[index].video = tmp_data.rfile;
                newMsg.video = tmp_data.files[0].name;
                loader.dismissAll();
                _this.presentToast("Видеофайл успешн загружен. .");
                _this.chatService.sendMsg(newMsg, _this.roomID)
                    .subscribe(function (res) {
                });
            }
            else {
                loader.dismissAll();
                _this.presentToast("Не удалось загрузить Видеофайл. Повторите позже");
            }
        }, function (err) {
            //console.log(err);
            loader.dismissAll();
            _this.presentToast("Не удалось обновить Видеофайл. Повторите позже");
        });
        fileTransfer.onProgress(function (progressEvent) { });
    };
    Chat.prototype.uploadFile = function (type) {
        var _this = this;
        var id = Date.now().toString();
        var tmpDate = new Date();
        var image = '';
        var video = '';
        var audio = '';
        if (this.platform.is('ios'))
            this.mediaURI = this.mediaURI.replace(/^file:\/\//, '');
        var options = {
            fileKey: 'file',
            fileName: 'ionicfile.jpg',
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {
                'Authorization': 'Bearer ' + __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */].jwt2
            }
        };
        if (type == 'image')
            image = this.mediaURI;
        else if (type == 'video') {
            video = this.mediaURI;
        }
        else if (type == 'audio') {
            audio = this.mediaURI;
            options.fileName = 'ionicfile.mp3';
            options.mimeType = "audio/mpeg";
        }
        var newMsg = {
            messageId: this.roomID + '_' + id,
            userid: this.auth.currentUser.id,
            name: this.auth.currentUser.name,
            avatar: this.auth.currentUser.avatar,
            date: __WEBPACK_IMPORTED_MODULE_4_date_fns_format___default()(tmpDate.getUTCFullYear().toString() + '-' + ('0' + (tmpDate.getMonth() + 1)).slice(-2).toString() + '-' + ('0' + tmpDate.getDate()).slice(-2).toString() + ' ' + ('0' + tmpDate.getUTCHours()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCMinutes()).slice(-2).toString() + ':' + ('0' + tmpDate.getUTCSeconds()).slice(-2).toString(), 'YYYY-MM-DD HH:mm:ss'),
            //date: format(new Date().toUTCString(), 'YYYY-MM-DD HH:mm:ss'),
            //date: new Date().toISOString(),
            text: this.editorMsg,
            status: 'pending',
            image: image,
            video: video,
            audio: audio,
            audioPlaying: false,
            audioOnPause: false,
            audioDuration: '',
            typeMsg: 'simple'
        };
        console.log(newMsg);
        var fileTransfer = this.transfer.create();
        console.log(this.mediaURI);
        console.log(this.msgList);
        fileTransfer.upload(this.mediaURI, this.config.apiURL + 'fn/upload_file.php?type=chat' + type + '&roomid=' + this.roomID, options)
            .then(function (data) {
            _this.pushNewMsg(newMsg);
            //console.log(data);
            console.log(_this.msgList);
            var tmp_data = JSON.parse(data.response);
            console.log(tmp_data);
            //loader.dismissAll();
            if (tmp_data.isSuccess) {
                var index = _this.getMsgIndexById(newMsg.messageId);
                console.log('------------------------------');
                console.log(index);
                console.log(_this.msgList);
                console.log('------------------------------');
                if (type == 'image') {
                    console.log(_this.msgList[index]);
                    _this.msgList[index].image = tmp_data.rfile;
                    newMsg.image = tmp_data.files[0].name;
                }
                else if (type == 'video') {
                    _this.msgList[index].video = tmp_data.rfile;
                    newMsg.video = tmp_data.files[0].name;
                }
                else if (type == 'audio') {
                    _this.msgList[index].audio = tmp_data.rfile;
                    newMsg.audio = tmp_data.files[0].name;
                }
                console.log(_this.msgList[index]);
                _this.chatService.sendMsg(newMsg, _this.roomID)
                    .subscribe(function (res) {
                    //console.log(res)
                    //let index = this.getMsgIndexById(id);
                    /*              if (index !== -1 && res.status=='success') {
                                    //this.msgList[index].status = 'success';
                                  }*/
                });
                if (type == 'image') {
                    _this.msgList[index].image = tmp_data.rfile;
                }
                else if (type == 'video') {
                    _this.msgList[index].video = tmp_data.rfile;
                }
                else if (type == 'audio') {
                    _this.msgList[index].audio = tmp_data.rfile;
                }
                //this.presentToast("Изображение профиля успешно обновлено");
            }
            else
                _this.presentToast("1 Не удалось загрузить Изображение. Повторите позже");
        }, function (err) {
            //console.log(err);
            //loader.dismissAll();
            _this.presentToast("2 Не удалось загрузить Изображение. Повторите позже");
        });
        fileTransfer.onProgress(function (progressEvent) {
            /*      if (progressEvent.lengthComputable) {
                    this.loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
                  } else {
                    this.loadingStatus.increment();
                  }*/
            ////console.log(progressEvent)
            //this.progressUploadMedia=((progressEvent.loaded / progressEvent.total)*100).toString()+'%';
        });
    };
    Chat.prototype.imageView = function (url) {
        this.photoViewer.show(url, '', { share: true });
    };
    Chat.prototype.prepareRecord = function () {
        delete this.recordingAudioFile;
    };
    Chat.prototype.startRecord = function (e) {
        var _this = this;
        this.recordOn = true;
        console.log('start recording Audio');
        this.vibration.vibrate(200);
        //this.file.createFile(this.file.tempDirectory, 'recordaudio.mp3', true).then(() => {
        if (this.platform.is('ios')) {
            this.file.createFile(this.file.tempDirectory, 'qr_recordaudio.m4a', true).then(function () {
                _this.recordingAudioFile = _this.media.create(_this.file.tempDirectory.replace(/^file:\/\//, '') + 'qr_recordaudio.m4a');
                _this.mediaURI = _this.file.tempDirectory.replace(/^file:\/\//, '') + 'qr_recordaudio.m4a';
                _this.recordingAudioFile.startRecord();
                _this.recordingAudioFile.onStatusUpdate.subscribe(function (status) { return console.log(status); }); // fires when file status changes
                _this.recordingAudioFile.onSuccess.subscribe(function () {
                    console.log('Action is successful');
                    _this.uploadFile('audio');
                });
                _this.recordingAudioFile.onError.subscribe(function (error) { return console.log('Error!', error); });
            });
        }
        else {
            this.recordingAudioFile = this.media.create(this.file.externalCacheDirectory + 'qr_recordaudio.mp3');
            this.mediaURI = this.file.externalCacheDirectory + 'qr_recordaudio.mp3';
            this.recordingAudioFile.startRecord();
            this.recordingAudioFile.onStatusUpdate.subscribe(function (status) { return console.log(status); }); // fires when file status changes
            this.recordingAudioFile.onSuccess.subscribe(function () {
                console.log('Action is successful');
                _this.uploadFile('audio');
            });
            this.recordingAudioFile.onError.subscribe(function (error) { return console.log('Error!', error); });
        }
        //});
    };
    Chat.prototype.stopRecord = function () {
        this.recordOn = false;
        console.log('stop recording Audio');
        if (this.recordingAudioFile) {
            this.recordingAudioFile.stopRecord();
            this.recordingAudioFile.release();
        }
    };
    Chat.prototype.playAudioRecord = function (id, url) {
        var _this = this;
        var index = this.getMsgIndexById(id);
        if (!this.msgList[index].audioPlaying) {
            this.msgList[index].audioPlaying = true;
            if (this.recordingAudioFile)
                this.recordingAudioFile.release();
            this.recordingAudioFile = this.media.create(url);
        }
        else {
        }
        console.log('Duration', this.recordingAudioFile.getDuration());
        this.recordingAudioFile.play();
        this.recordingAudioFile.getCurrentAmplitude().then(function (res) {
            console.log('Amplitude', res);
        });
        this.recordingAudioFile.getCurrentPosition().then(function (position) {
            console.log(position);
        });
        this.recordingAudioFile.onSuccess.subscribe(function () {
            _this.msgList[index].audioPlaying = false;
        });
    };
    Chat.prototype.hideEditMes = function () {
        console.log('hide');
        this.editorMsgStart = '';
        this.answerMsgStart = '';
        this.editorMsg = '';
    };
    Chat.prototype.hideGif = function () {
        this.gif = false;
    };
    Chat.prototype.hideStc = function () {
        this.stc = false;
    };
    Chat.prototype.editMsgById = function (messId, text) {
        var _this = this;
        console.log(messId);
        this.chatService.editMsgById(messId, text).subscribe(function (res) {
            if (res.status) {
                var index = _this.getMsgIndexById(messId);
                _this.msgList[index].text = text;
                _this.editorMsgStart = '';
                _this.editorMsg = '';
            }
        });
    };
    Chat.prototype.answerMsgById = function (messId, text) {
        console.log(messId);
        console.log(text);
    };
    Chat.prototype.holdItem = function (message) {
        var _this = this;
        this.vibration.vibrate(100);
        if (message.userid == this.auth.currentUser.id && !message.audio && !message.image && message.typeMsg !== 'gif' && message.typeMsg !== 'stick') {
            var actionSheet = this.actionSheetCtrl.create({
                buttons: [
                    {
                        text: 'Скопировать',
                        handler: function () {
                            _this.clipboard.copy(message.text);
                        }
                    },
                    {
                        text: 'Удалить у меня',
                        handler: function () {
                            _this.chatService.msgRemoveId(message.messageId).subscribe(function (res) {
                                if (res.status) {
                                    var messageId = _this.getMsgIndexById(message.messageId);
                                    _this.msgList.splice(messageId, 1);
                                }
                            });
                        }
                    },
                    {
                        text: 'Удалить у всех',
                        handler: function () {
                            _this.chatService.msgRemoveIdAll(message.messageId).subscribe(function (res) {
                                if (res.status) {
                                    var messageId = _this.getMsgIndexById(message.messageId);
                                    _this.msgList.splice(messageId, 1);
                                }
                            });
                        }
                    },
                    {
                        text: 'Изменить',
                        handler: function () {
                            _this.messageIdstart = message.messageId;
                            _this.editorMsg = message.text;
                            _this.editorMsgStart = message.text;
                        }
                    },
                    {
                        text: 'Отмена',
                        role: 'cancel',
                        handler: function () {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
        else if (message.userid !== this.auth.currentUser.id && (message.audio || message.image || message.typeMsg == 'gif' || message.typeMsg == 'stick')) {
            var actionSheet = this.actionSheetCtrl.create({
                buttons: [
                    {
                        text: 'Ответить',
                        handler: function () {
                            console.log('ответить');
                            _this.messageIdstart = message.messageId;
                            if (message.text) {
                                _this.textMessageAnswer = message.text;
                            }
                            if (message.audio) {
                                _this.textMessageAnswer = 'Аудиофайл';
                            }
                            if (message.image) {
                                _this.textMessageAnswer = 'Изображение';
                            }
                            _this.answerMsgStart = _this.textMessageAnswer;
                            if (message.userid == _this.auth.currentUser.id) {
                                _this.answerMsgName = 'Вы';
                            }
                            else {
                                _this.answerMsgName = message.name;
                            }
                        }
                    },
                    {
                        text: 'Удалить у меня',
                        handler: function () {
                            _this.chatService.msgRemoveId(message.messageId).subscribe(function (res) {
                                if (res.status) {
                                    var messageId = _this.getMsgIndexById(message.messageId);
                                    _this.msgList.splice(messageId, 1);
                                }
                            });
                        }
                    },
                    {
                        text: 'Отмена',
                        role: 'cancel',
                        handler: function () {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
        else if (message.userid !== this.auth.currentUser.id && !message.audio && !message.image && message.typeMsg !== 'gif' && message.typeMsg !== 'stick') {
            var actionSheet = this.actionSheetCtrl.create({
                buttons: [
                    {
                        text: 'Ответить',
                        handler: function () {
                            console.log('ответить');
                            _this.messageIdstart = message.messageId;
                            if (message.text) {
                                _this.textMessageAnswer = message.text;
                            }
                            if (message.audio) {
                                _this.textMessageAnswer = 'Аудиофайл';
                            }
                            if (message.image) {
                                _this.textMessageAnswer = 'Изображение';
                            }
                            _this.answerMsgStart = _this.textMessageAnswer;
                            if (message.userid == _this.auth.currentUser.id) {
                                _this.answerMsgName = 'Вы';
                            }
                            else {
                                _this.answerMsgName = message.name;
                            }
                        }
                    },
                    {
                        text: 'Скопировать',
                        handler: function () {
                            _this.clipboard.copy(message.text);
                        }
                    },
                    {
                        text: 'Удалить у меня',
                        handler: function () {
                            _this.chatService.msgRemoveId(message.messageId).subscribe(function (res) {
                                if (res.status) {
                                    var messageId = _this.getMsgIndexById(message.messageId);
                                    _this.msgList.splice(messageId, 1);
                                }
                            });
                        }
                    },
                    {
                        text: 'Отмена',
                        role: 'cancel',
                        handler: function () {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
        else if (message.userid == this.auth.currentUser.id && (message.audio || message.image || message.typeMsg == 'gif' || message.typeMsg == 'stick')) {
            var actionSheet = this.actionSheetCtrl.create({
                buttons: [
                    {
                        text: 'Ответить',
                        handler: function () {
                            console.log('ответить');
                            _this.messageIdstart = message.messageId;
                            if (message.text) {
                                _this.textMessageAnswer = message.text;
                            }
                            if (message.audio) {
                                _this.textMessageAnswer = 'Аудиофайл';
                            }
                            if (message.image) {
                                _this.textMessageAnswer = 'Изображение';
                            }
                            _this.answerMsgStart = _this.textMessageAnswer;
                            if (message.userid == _this.auth.currentUser.id) {
                                _this.answerMsgName = 'Вы';
                            }
                            else {
                                _this.answerMsgName = message.name;
                            }
                        }
                    },
                    {
                        text: 'Удалить у меня',
                        handler: function () {
                            _this.chatService.msgRemoveId(message.messageId).subscribe(function (res) {
                                if (res.status) {
                                    var messageId = _this.getMsgIndexById(message.messageId);
                                    _this.msgList.splice(messageId, 1);
                                }
                            });
                        }
                    },
                    {
                        text: 'Удалить у всех',
                        handler: function () {
                            _this.chatService.msgRemoveIdAll(message.messageId).subscribe(function (res) {
                                if (res.status) {
                                    var messageId = _this.getMsgIndexById(message.messageId);
                                    _this.msgList.splice(messageId, 1);
                                }
                            });
                        }
                    },
                    {
                        text: 'Отмена',
                        role: 'cancel',
                        handler: function () {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            actionSheet.present();
        }
    };
    Chat.prototype.pauseAudioRecord = function (id) {
        var index = this.getMsgIndexById(id);
        this.msgList[index].audioPlaying = false;
        if (this.recordingAudioFile)
            this.recordingAudioFile.stop();
    };
    Chat.prototype.photoStart = function () {
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            allowEdit: false,
            encodingType: 0
        };
        this.getImageNext(options);
    };
    Chat.prototype.stick = function () {
        this.stc = true;
    };
    Chat.prototype.getMedia = function (num) {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'Фото',
                    handler: function () {
                        var options = {
                            quality: 100,
                            destinationType: _this.camera.DestinationType.FILE_URI,
                            sourceType: _this.camera.PictureSourceType.PHOTOLIBRARY,
                            mediaType: _this.camera.MediaType.PICTURE,
                            correctOrientation: true,
                            allowEdit: false,
                            encodingType: 0
                        };
                        _this.getImageNext(options);
                    }
                },
                {
                    text: 'Видео',
                    handler: function () {
                        var options = {
                            quality: 100,
                            destinationType: _this.camera.DestinationType.DATA_URL,
                            sourceType: _this.camera.PictureSourceType.PHOTOLIBRARY,
                            mediaType: _this.camera.MediaType.VIDEO,
                            correctOrientation: true,
                            allowEdit: false,
                            encodingType: 0
                        };
                        _this.getVideoNext(options);
                    }
                },
                {
                    text: 'Геопозиция',
                    handler: function () {
                        _this.openmap();
                    }
                },
                {
                    text: 'Gif',
                    handler: function () {
                        _this.gif = true;
                    }
                },
                {
                    text: 'Отмена',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    Chat.prototype.newcall = function (type) {
        console.log(type);
        this.navCtrl.push('CallYesPage', { info: { userid: this.roomUserID, myid: this.auth.currentUser.id, roomid: this.roomUserID + '-' + this.auth.currentUser.id + '-' + new Date().getTime(), type: type, status: 'open' } });
    };
    Chat.prototype.getImageNext = function (options) {
        var _this = this;
        this.camera.getPicture(options).then(function (imageData) {
            _this.imageURI = imageData;
            _this.goToUploadFile(_this.imageURI, 'image');
            console.log(imageData);
        }, function (err) {
        });
    };
    Chat.prototype.getVideoNext = function (options) {
        var _this = this;
        this.camera.getPicture(options).then(function (imageData) {
            _this.imageURI = imageData;
            _this.goToUploadVideoFile(_this.imageURI, 'video');
            console.log(imageData);
        }, function (err) {
        });
    };
    Chat.prototype.goToUploadFile = function (mediaURI, type) {
        this.mediaURI = mediaURI;
        this.uploadFile(type);
    };
    Chat.prototype.goToUploadVideoFile = function (mediaURI, type) {
        this.mediaURI = mediaURI;
        this.uploadVideoFile(type);
    };
    Chat.prototype.hinumb = function (tel) {
        this.browser.create("tel:(tel)", '_system');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], Chat.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('chat_input'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* TextInput */])
    ], Chat.prototype, "messageInput", void 0);
    Chat = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chat',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/chat/chat.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      <ion-item class="user-header" tappable (click)="goToProfile()">\n        <ion-avatar item-start>\n          <img src="{{roomAvatar}}">\n        </ion-avatar>\n        <h4 class="user-title">{{roomName}}</h4>\n        <p class="user-status" *ngIf="roomStatus==1 && roomUserID">онлайн</p>\n        <p class="user-status" *ngIf="roomStatus==4 && roomUserID">был(а) {{roomStatusDate | relativeTime}}</p>\n        <p class="user-status" *ngIf="roomUsers">Вы, {{roomUsers}}</p>\n      </ion-item>\n    </ion-title>\n    <!--<ion-buttons end>\n      <button ion-button icon-only (click)="newcall(\'video\')" >\n        <ion-icon name="ios-videocam-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons end>\n    <button ion-button icon-only (click)="newcall(\'audio\')" >\n      <ion-icon name="ios-call-outline"></ion-icon>\n    </button>\n  </ion-buttons>-->\n  </ion-navbar>\n</ion-header>\n<ion-content [style.opacity]="isModal ? 0 : 1">\n  <div ion-fixed class="wait-loading" *ngIf="waitLoading">\n    <ion-spinner name="crescent"></ion-spinner>\n  </div>\n  <ion-infinite-scroll *ngIf="countMsg!=msgList.length" position="top" threshold="25%" (ionInfinite)="$event.waitFor(doInfinite())">\n    <ion-infinite-scroll-content\n      loadingSpinner="crescent">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n  <div class="message-wrap">\n    <ng-container *ngFor="let msg of msgList">\n\n\n      <div *ngIf="msg.typeMsg==\'system\'" class="sysMsg">\n        {{msg.text}}\n      </div>\n\n      <div *ngIf="msg.typeMsg==\'gif\'"\n           [class.left]=" msg.userid !== auth.currentUser.id "\n           [class.right]=" msg.userid === auth.currentUser.id "\n           class="message"\n           (press)="holdItem(msg)"\n           >\n        <img  (click)="goToProfileA(msg.userid)"  *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="msg-avatar" src="{{msg.avatar}}">\n        <div class="msg-detail">\n          <div class="msg-content">\n            <span class="triangle"></span>\n            <div class="iconAnswer" *ngIf="msg.userid==auth.currentUser.id && msg.image">\n              <ion-icon  name="arrow-dropright-circle"></ion-icon>\n            </div>\n            <div class="msg-media">\n              <b *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="nameUserGroup">{{msg.name}}<br></b>\n              <div class="msg-media-loading" *ngIf="msg.status === \'pending\'"><ion-spinner name="crescent" ></ion-spinner></div>\n              <div class="msg-image">\n                <img class="gifImage" [src]="msg.text" >\n              </div>\n            </div>\n          </div>\n          <div class="msg-info">\n            <p>{{msg.date | relativeTime}} <ion-icon class="iconEdit" *ngIf="msg.edit==1" name="md-create"></ion-icon> <ion-icon [ngStyle]="{\'color\': msg.status===\'read\'? \'#5ecae4\' : \'gray\'}" *ngIf="(msg.status === \'success\' || msg.status === \'read\') && msg.userid==auth.currentUser.id" name="md-done-all"></ion-icon><ion-spinner name="crescent" *ngIf="msg.status === \'pending\'"></ion-spinner></p>\n          </div>\n        </div>\n      </div>\n\n\n      <div *ngIf="msg.typeMsg==\'geo\'"\n           [class.left]=" msg.userid !== auth.currentUser.id "\n           [class.right]=" msg.userid === auth.currentUser.id "\n           class="message"\n           (press)="holdItem(msg)"\n           >\n        <img  (click)="goToProfileA(msg.userid)"  *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="msg-avatar" src="{{msg.avatar}}">\n        <div class="msg-detail">\n          <div class="msg-content">\n            <span class="triangle"></span>\n            <div class="iconAnswer" *ngIf="msg.userid==auth.currentUser.id && msg.image">\n              <ion-icon  name="arrow-dropright-circle"></ion-icon>\n            </div>\n            <div class="msg-media">\n              <b *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="nameUserGroup">{{msg.name}}<br></b>\n              <div class="msg-media-loading" *ngIf="msg.status === \'pending\'"><ion-spinner name="crescent" ></ion-spinner></div>\n              <button ion-button icon-only clear (click)="goToLocate(msg.text,msg.name)">\n                <ion-icon name="pin"></ion-icon> Местоположение\n              </button>\n            </div>\n          </div>\n          <div class="msg-info">\n            <p>{{msg.date | relativeTime}} <ion-icon class="iconEdit" *ngIf="msg.edit==1" name="md-create"></ion-icon> <ion-icon [ngStyle]="{\'color\': msg.status===\'read\'? \'#5ecae4\' : \'gray\'}" *ngIf="(msg.status === \'success\' || msg.status === \'read\') && msg.userid==auth.currentUser.id" name="md-done-all"></ion-icon><ion-spinner name="crescent" *ngIf="msg.status === \'pending\'"></ion-spinner></p>\n          </div>\n        </div>\n      </div>\n\n      <div *ngIf="msg.typeMsg==\'stick\'"\n           [class.left]=" msg.userid !== auth.currentUser.id "\n           [class.right]=" msg.userid === auth.currentUser.id "\n           class="message"\n           (press)="holdItem(msg)">\n        <img (click)="goToProfileA(msg.userid)" *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="msg-avatar" src="{{msg.avatar}}">\n        <div class="msg-detail">\n          <div class="msg-content msgticks">\n            <div class="iconAnswer" *ngIf="msg.userid==auth.currentUser.id && msg.image">\n              <ion-icon  name="arrow-dropright-circle"></ion-icon>\n            </div>\n            <div class="msg-media msgMediaticks">\n              <b *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="nameUserGroup">{{msg.name}}<br></b>\n              <div class="msg-media-loading" *ngIf="msg.status === \'pending\'"><ion-spinner name="crescent" ></ion-spinner></div>\n              <div class="msg-image">\n                <img class="gifImage" [src]="msg.text" >\n              </div>\n            </div>\n          </div>\n          <div class="msg-info">\n            <p>{{msg.date | relativeTime}} <ion-icon class="iconEdit" *ngIf="msg.edit==1" name="md-create"></ion-icon> <ion-icon [ngStyle]="{\'color\': msg.status===\'read\'? \'#5ecae4\' : \'gray\'}" *ngIf="(msg.status === \'success\' || msg.status === \'read\') && msg.userid==auth.currentUser.id" name="md-done-all"></ion-icon><ion-spinner name="crescent" *ngIf="msg.status === \'pending\'"></ion-spinner></p>\n          </div>\n        </div>\n      </div>\n\n\n      <div *ngIf="msg.typeMsg==\'simple\'"\n        class="message"\n        [class.left]=" msg.userid !== auth.currentUser.id "\n        [class.right]=" msg.userid === auth.currentUser.id "\n        (press)="holdItem(msg)">\n        <img  (click)="goToProfileA(msg.userid)"  *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="msg-avatar" src="{{msg.avatar}}">\n        <div class="msg-detail">\n          <div class="msg-content">\n            <span class="triangle"></span>\n            <div class="iconAnswer" *ngIf="msg.userid==auth.currentUser.id && msg.image">\n              <ion-icon  name="arrow-dropright-circle"></ion-icon>\n            </div>\n            <div *ngIf="msg.image" class="msg-media">\n              <b *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="nameUserGroup">{{msg.name}}<br></b>\n              <div class="msg-media-loading" *ngIf="msg.status === \'pending\'"><ion-spinner name="crescent" ></ion-spinner></div>\n              <div class="msg-image">\n                <img  *ngIf="msg.image" [src]="msg.image" (click)="imageView(msg.image)">\n              </div>\n            </div>\n            <div *ngIf="msg.video" class="msg-media">\n              <b *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="nameUserGroup">{{msg.name}}<br></b>\n              <div class="msg-media-loading" *ngIf="msg.status === \'pending\'"><ion-spinner name="crescent" ></ion-spinner></div>\n              <div class="msg-image">\n                <video controls  [poster]="msg.video+\'.jpg\'">\n                  <source [src]="msg.video" type="video/mp4">\n                </video>\n              </div>\n            </div>\n            <div *ngIf="msg.audio" class="msg-audio">\n              <b *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="nameUserGroup">{{msg.name}}<br></b>\n              <ion-grid no-padding>\n                <ion-row justify-content-center align-items-center>\n                  <ion-col col-auto>\n                    <button *ngIf="!msg.audioPlaying" ion-button mode="ios" icon-only clear [color]="config.themeColor.primary" class="audioMessage" (click)="playAudioRecord(msg.messageId, msg.audio)"><ion-icon name="md-play"></ion-icon></button>\n                    <button *ngIf="msg.audioPlaying" ion-button mode="ios" icon-only clear [color]="config.themeColor.primary" class="audioMessageStop" (click)="pauseAudioRecord(msg.messageId)"><ion-icon name="md-square"></ion-icon></button>\n                  </ion-col>\n                  <ion-col col-auto>\n                    <div padding-right>Аудио сообщение{{msg.audioDuration}}</div>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </div>\n\n            <p *ngIf="msg.text" class="line-breaker" ><b *ngIf="!roomUserID && msg.userid !== auth.currentUser.id" class="nameUserGroup">{{msg.name}}<br></b>\n             <span [innerHTML]="msg.text | formatMessages"></span>\n            </p>\n\n          </div>\n          <div class="msg-info">\n            <p>{{msg.date | relativeTime}} <ion-icon class="iconEdit" *ngIf="msg.edit==1" name="md-create"></ion-icon> <ion-icon [ngStyle]="{\'color\': msg.status===\'read\'? \'#5ecae4\' : \'gray\'}" *ngIf="(msg.status === \'success\' || msg.status === \'read\') && msg.userid==auth.currentUser.id" name="md-done-all"></ion-icon><ion-spinner name="crescent" *ngIf="msg.status === \'pending\'"></ion-spinner></p>\n          </div>\n        </div>\n      </div>\n    </ng-container>\n  </div>\n</ion-content>\n<ion-footer id="chat_footer" no-border >\n  <div class="editMessage" id="editMessage" *ngIf="editorMsgStart.length>0">\n    <div class="borderLeft">\n      Редактирование сообщения\n      <button class="closeEditMessage" (click)="hideEditMes()" ion-button clear icon-only item-right mode="ios" >\n        <ion-icon name="ios-close-outline"></ion-icon>\n      </button>\n      <p class="editMessageText" >{{editorMsgStart}}</p>\n    </div>\n  </div>\n  <div class="gifMessage" *ngIf="gif">\n    <button class="closeEditMessage" (click)="hideGif()" ion-button clear icon-only item-right mode="ios" >\n      <ion-icon name="ios-close-outline"></ion-icon>\n    </button>\n    <div class="borderLeft">\n      <div class="imageGif">\n        <ng-container *ngFor="let gif of giflist">\n          <img  (click)="sendGif(gif.name)" class="gifs" [src]="gif.name">\n        </ng-container>\n      </div>\n    </div>\n  </div>\n\n  <div class="stcMessage" *ngIf="stc">\n    <button class="closeEditMessage" (click)="hideStc()" ion-button clear icon-only item-right mode="ios" >\n      <ion-icon name="ios-close-outline"></ion-icon>\n    </button>\n    <div class="borderLeft">\n      <div class="imageGif">\n        <ng-container *ngFor="let stc of stclist">\n          <img  (click)="sendStc(stc.name)" class="sticks" [src]="stc.name">\n        </ng-container>\n      </div>\n    </div>\n  </div>\n\n  <div class="editMessage" id="answerMessage" *ngIf="answerMsgStart.length>0">\n    <div class="borderLeft">\n      {{answerMsgName}}\n      <button class="closeEditMessage" (click)="hideEditMes()" ion-button clear icon-only item-right mode="ios" >\n        <ion-icon name="ios-close-outline"></ion-icon>\n      </button>\n      <p class="editMessageText" >{{textMessageAnswer}}</p>\n    </div>\n  </div>\n\n  <ion-toolbar class="no-border">\n    <ion-buttons left *ngIf="editorMsg.length==0" class="attachIcon">\n      <button  ion-button clear icon-only (click)="getMedia($event)">\n        <ion-icon name="ios-attach-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-textarea elasticTextArea type="text" #chat_input id="chat_input"\n                  placeholder="Введите сообщение"\n                  [(ngModel)]="editorMsg"\n                  (keyup.enter)="sendMsg()"\n                  (ionFocus)="onFocus()">\n    </ion-textarea>\n    <div class="recordOn" *ngIf="recordOn">\n      <ion-icon name="ios-mic-outline"></ion-icon>\n      <span class="recOn">Идет запись...</span>\n    </div>\n    <ion-buttons *ngIf="editorMsg.length>0 && editorMsgStart.length==0" end>\n      <button  ion-button clear icon-only item-right mode="ios" (click)="sendMsg()">\n        <ion-icon name="md-send" ios="md-send" md="md-send"></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <ion-buttons *ngIf="editorMsgStart.length>0" end>\n      <button  ion-button clear icon-only item-right mode="ios"  (click)="editMsgById(messageIdstart,editorMsg)">\n        <ion-icon name="ios-checkmark-circle-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <ion-buttons *ngIf="editorMsg.length==0" end class="stick">\n      <button  ion-button clear icon-only item-right mode="ios" (click)="stick()">\n        <ion-icon name="radio-button-on"  style="color: grey"></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <ion-buttons *ngIf="editorMsg.length==0" end class="cameraSetSend">\n      <button  ion-button clear icon-only item-right (click)="photoStart()">\n        <ion-icon name="ios-camera-outline" ></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <ion-buttons *ngIf="editorMsg.length==0" end>\n      <button  ion-button clear icon-only item-right mode="ios" (press)="startRecord($event)" (touchend)="stopRecord($event)">\n        <ion-icon name="ios-mic-outline" ></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n  <emoji-picker *ngIf="showEmojiPicker" [(ngModel)]="editorMsg"></emoji-picker>\n</ion-footer>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/chat/chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_19__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_keyboard__["a" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_photo_viewer__["a" /* PhotoViewer */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_vibration__["a" /* Vibration */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_media__["a" /* Media */],
            __WEBPACK_IMPORTED_MODULE_17__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_media_capture__["a" /* MediaCapture */],
            __WEBPACK_IMPORTED_MODULE_15_ng_socket_io__["Socket"],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_16__ionic_native_clipboard__["a" /* Clipboard */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_18__angular_platform_browser__["c" /* DomSanitizer */]])
    ], Chat);
    return Chat;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImgUploadServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_file_transfer__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_crop__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/*
  Generated class for the ImgUploadServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ImgUploadServiceProvider = /** @class */ (function () {
    function ImgUploadServiceProvider(http, auth, camera, toastCtrl, transfer, loadingCtrl, crop, config) {
        this.http = http;
        this.auth = auth;
        this.camera = camera;
        this.toastCtrl = toastCtrl;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.crop = crop;
        this.config = config;
    }
    ImgUploadServiceProvider.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            //console.log('Dismissed toast');
        });
        toast.present();
    };
    ImgUploadServiceProvider.prototype.getImageAvatar = function (selectedSourceType, type, roomid) {
        var _this = this;
        if (roomid === void 0) { roomid = null; }
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: selectedSourceType,
            mediaType: 0,
            correctOrientation: true,
            allowEdit: false,
            encodingType: 0
        };
        var optionCrop = {
            quality: 100,
            widthRatio: 0,
            heightRatio: 0,
            targetWidth: 0,
            targetHeight: 0
        };
        if (type == 'avatar' || type == 'groupavatar') {
            options.targetWidth = 300;
            options.targetHeight = 300;
            optionCrop.heightRatio = 1;
            optionCrop.widthRatio = 1;
        }
        else if (type == 'background') {
            /*      options.targetWidth = 1024;
                  options.targetHeight = 600;*/
        }
        var loader = this.loadingCtrl.create({
            content: "Подождите...",
            dismissOnPageChange: true
            /*      spinner: 'hide',
                  content: `
                  <div class="custom-spinner-container">
                    <div class="custom-spinner-box"></div>
                  </div>`,*/
        });
        loader.present();
        return this.camera.getPicture(options).then(function (imageData) {
            _this.imageURI = imageData;
            return _this.crop.crop(imageData, optionCrop).then(function (newImage) {
                _this.imageURI = newImage;
                loader.present();
                return _this.uploadFile(type, roomid).then(function (res) {
                    if (res && type == 'groupavatar')
                        return _this.imageURI;
                });
            }, function (err) {
                //console.log(err);
                loader.dismissAll();
            });
        }, function (err) {
            //console.log(err);
            _this.presentToast(err);
        });
    };
    ImgUploadServiceProvider.prototype.uploadFile = function (type, roomid) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Отправка изображения..."
            /*      spinner: 'hide',
                  content: `
                  <div class="custom-spinner-container">
                    <div class="custom-spinner-box"></div>
                  </div>`,*/
        });
        loader.present();
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'file',
            fileName: 'ionicfile.jpg',
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {
                'Authorization': 'Bearer ' + __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */].jwt2
            }
            /*      headers: {
                    'Authorization': 'Bearer '+this.auth.jwt
                  }*/
        };
        return fileTransfer.upload(this.imageURI, this.config.apiURL + 'fn/upload_file.php?type=' + type + '&roomid=' + roomid, options)
            .then(function (data) {
            //console.log(data);
            var tmp_data = JSON.parse(data.response);
            //console.log(tmp_data);
            loader.dismissAll();
            if (tmp_data.isSuccess) {
                if (type == 'avatar')
                    _this.auth.currentUser.avatar = tmp_data.files[0].file;
                if (type == 'background')
                    _this.auth.currentUser.background = tmp_data.files[0].file;
                if (type == 'groupavatar')
                    return true;
                //this.presentToast("Изображение профиля успешно обновлено");
            }
            else
                _this.presentToast("Не удалось загрузить Изображение. Повторите позже");
        }, function (err) {
            //console.log(err);
            loader.dismissAll();
            _this.presentToast("Не удалось обновить Изображение. Повторите позже");
        });
        /*    fileTransfer.onProgress(progressEvent=>{
              /!*      if (progressEvent.lengthComputable) {
                      this.loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
                    } else {
                      this.loadingStatus.increment();
                    }*!/
              ////console.log(progressEvent)
            })*/
    };
    ImgUploadServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["t" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_crop__["a" /* Crop */],
            __WEBPACK_IMPORTED_MODULE_7__config__["a" /* ConfigProvider */]])
    ], ImgUploadServiceProvider);
    return ImgUploadServiceProvider;
}());

//# sourceMappingURL=img-upload-service.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WelcomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_img_upload_service__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_diagnostic__ = __webpack_require__(191);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WelcomePage = /** @class */ (function () {
    function WelcomePage(navCtrl, navParams, menuCtrl, loadingCtrl, alertCtrl, auth, events, platform, imgUploadService, actionSheetCtrl, camera, diagnostic) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.events = events;
        this.platform = platform;
        this.imgUploadService = imgUploadService;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.diagnostic = diagnostic;
        this.registerCredentials = { tel: '', code: '+7' };
        this.registerCredentialsName = {
            name: ''
        };
        this.stepokey = true;
        this.stepsign = false;
        this.stepopt = false;
        this.stepNameAva = false;
        this.phone = '';
        this.code = '';
        this.repeatButton = false;
        this.repeatButtonText = 60;
    }
    WelcomePage.prototype.ionViewDidEnter = function () {
        console.log('menu disable');
        this.menuCtrl.enable(false, 'side-menu1');
        this.menuCtrl.swipeEnable(false);
    };
    WelcomePage.prototype.ionViewWillLeave = function () {
        console.log('menu enable');
        this.menuCtrl.enable(true, 'side-menu1');
        this.menuCtrl.swipeEnable(true);
    };
    WelcomePage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Подождите...',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    WelcomePage.prototype.showError = function (text) {
        this.loading.dismiss();
        var alert = this.alertCtrl.create({
            title: 'Ошибка',
            subTitle: text,
            buttons: ['Ок']
        });
        alert.present();
    };
    WelcomePage.prototype.signStepOne = function () {
        var _this = this;
        var phone = this.registerCredentials.code + this.registerCredentials.tel;
        this.showLoading();
        this.auth
            .verifyPhone(phone)
            .subscribe(function (res) {
            if (res.status) {
                _this.phone = phone;
                var params = {
                    phone: phone
                };
                _this.stepsign = false;
                _this.stepopt = true;
                _this.loading.dismiss();
                /*this.navCtrl.push(OptPage, params);*/
            }
            else {
                _this.showError(res.text);
            }
        });
    };
    WelcomePage.prototype.checkOnInput = function () {
        if (this.code.length >= 4) {
            this.signStepTwo();
        }
    };
    WelcomePage.prototype.signStepTwo = function () {
        var _this = this;
        this.showLoading();
        this.auth
            .verifyCode(this.phone, this.code)
            .subscribe(function (res) {
            console.log(res);
            if (res.status) {
                _this.auth.setUserInfo(res.user, res.user.jwt);
                __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */].jwt2 = res.user.jwt;
                _this.events.publish('myPushInit');
                if (res.user.name == null || res.user.name == '') {
                    _this.stepopt = false;
                    _this.stepNameAva = true;
                    _this.loading.dismiss();
                }
                else
                    _this.navCtrl.setRoot('MaintabsPage');
                //this.navCtrl.push(OptPage, params);
            }
            else {
                /*this.loading.dismiss();*/
                _this.showError('Неверный код подтверждения');
            }
        });
    };
    WelcomePage.prototype.choiceTypeAvatarSource = function (type) {
        var _this = this;
        var buttons;
        if (this.platform.is('ios')) {
            buttons = [{
                    text: 'Выбрать из библиотеки',
                    //icon: 'photos',
                    role: 'destructive',
                    handler: function () { _this.imgUploadService.getImageAvatar(_this.camera.PictureSourceType.PHOTOLIBRARY, type); }
                }, {
                    text: 'Сделать фотографию',
                    // icon: 'camera',
                    role: 'destructive',
                    handler: function () { _this.imgUploadService.getImageAvatar(_this.camera.PictureSourceType.CAMERA, type); }
                }, {
                    text: 'Отмена',
                    role: 'cancel',
                }];
        }
        else {
            buttons = [{
                    text: 'Выбрать из библиотеки',
                    icon: 'photos',
                    role: 'destructive',
                    handler: function () { _this.imgUploadService.getImageAvatar(_this.camera.PictureSourceType.PHOTOLIBRARY, type); }
                }, {
                    text: 'Сделать фотографию',
                    icon: 'camera',
                    role: 'destructive',
                    handler: function () { _this.imgUploadService.getImageAvatar(_this.camera.PictureSourceType.CAMERA, type); }
                }, {
                    text: 'Отмена',
                    role: 'cancel',
                    icon: 'close',
                }];
        }
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Источник изображения',
            buttons: buttons
        });
        actionSheet.present();
    };
    WelcomePage.prototype.signStepThree = function () {
        var _this = this;
        this.showLoading();
        this.auth
            .setUserName(this.registerCredentialsName.name)
            .subscribe(function (res) {
            //console.log(res);
            if (res.status) {
                _this.auth.currentUser.name = _this.registerCredentialsName.name;
                _this.navCtrl.setRoot('MaintabsPage');
            }
            else {
                _this.showError(res.text);
            }
        });
    };
    WelcomePage.prototype.stepOkey = function () {
        this.stepokey = false;
        this.stepsign = true;
    };
    WelcomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-welcome',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/welcome/welcome.html"*/'<ion-content text-center>\n  <div class="backgrounddiv" style="width:100vw;height:100%;margin-top:0px;padding:0px;">\n    <video webkit-playsinline playsinline autoplay loop muted class="backgroundvideo">\n      <source src="assets/video/backgroundvideoFullmp4.mp4" type=\'video/mp4; codecs="h.264"\'>\n      <source src="assets/video/backgroundvideoFullwebm.webm" type="video/webm">\n    </video>\n  <div style="height:80px;" ></div>\n  <div [class]="stepokey?\'box\':\'box boxAnimation\'" >\n    <img *ngIf="stepNameAva==false" class="logo" src="assets/img/logo_icon.png">\n    <img *ngIf="stepNameAva==false" class="textlogo" src="assets/img/logo_text.png">\n  <div padding id="footer" *ngIf="stepokey">\n    <p class="footerText">Для продолжения нужно принять "Политику конфеденциальнеости" и "Условия лицензионного соглашения".</p>\n    <button class="buttonFooter" (click)="stepOkey()" ion-button mode="ios" color="primary2" block >\n      Принять и продолжить\n    </button>\n  </div>\n    <div class="nextPage" *ngIf="stepsign"  padding >\n            <form id="sign-step-one-form1" (ngSubmit)="signStepOne()" id="ngForm2" #registerForm="ngForm">\n            <h2 ion-text class="verifyText">Подтверждение номера</h2>\n            <p class="sendText">Мы отправим SMS-сообщение, чтобы подтвердить ваш номер телефона.</p>\n              <ion-grid no-padding>\n                <ion-row>\n                  <ion-col>\n                    <ion-item no-padding>\n                      <ion-label floating>\n                        Код страны\n                      </ion-label>\n                      <ion-select okText="Выбрать" cancelText="Отмена" name="code"  [(ngModel)]="registerCredentials.code" required>\n                        <ion-option value="+7">Россия +7</ion-option>\n                      </ion-select>\n\n                    </ion-item>\n                  </ion-col>\n                  <ion-col col-7>\n                    <ion-item no-padding>\n                      <ion-label floating>\n                        Номер телефона\n                      </ion-label>\n                      <ion-input type="tel" name="tel" placeholder="" [brmasker]="{mask:\'0000000000\', len:10}" minlength="10" maxlength="10" value="" [(ngModel)]="registerCredentials.tel" required></ion-input>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n              <div style="height:30px;"></div>\n              <button class="button-next" mode="ios" id="sign-step-one-button1" form="ngForm2" type="submit" ion-button color="stable" block [disabled]="!registerForm.form.valid">\n                Продолжить\n              </button>\n            </form>\n     </div>\n\n    <form *ngIf="stepopt"  [class]="stepopt?\'formSignTrue\':\'formSign\'" (ngSubmit)="signStepTwo()" id="ngForm3" #registerForm2="ngForm" padding>\n      <div class="spacer" style="height:10px;" id="spacer1"></div>\n      <h2 ion-text  class="verifyText">Подтверждение номера</h2>\n      <p class="sendText">Введите проверочный код отправленный Вам в SMS на номер: {{phone}}</p>\n      <ion-item no-padding>\n        <ion-label floating>\n          4-значный код\n        </ion-label>\n        <ion-input type="tel" name="code" placeholder="" (ionChange)="checkOnInput()" [brmasker]="{mask:\'0000\', len:4}"  minlength="4" maxlength="4" value="" [(ngModel)]="code" required></ion-input>\n      </ion-item>\n\n      <div style="height:30px;"></div>\n      <button class="button-next" mode="ios" form="ngForm3" type="submit" ion-button color="stable" block [disabled]="!registerForm2.form.valid">\n        Далее\n      </button>\n    </form>\n\n\n    <div class="nameAndAva" *ngIf="stepNameAva">\n      <h2 ion-text class="verifyText">Профиль</h2>\n      <p class="sendText">Введите свое имя и при желании установите фото профиля</p>\n      <form (ngSubmit)="signStepThree()" id="ngForm4" #registerForm3="ngForm">\n        <div class="spacer" style="height:20px;" ></div>\n        <img class="avatar" src="{{auth.currentUser.avatar}}"  (click)="choiceTypeAvatarSource(\'avatar\')"/>\n\n        <ion-item padding>\n          <ion-label floating>Введите имя</ion-label>\n          <ion-input type="text" name="name" placeholder=""  minlength="2" maxlength="60" value="" [(ngModel)]="registerCredentialsName.name" required></ion-input>\n        </ion-item>\n        <div padding>\n          <button class="button-next" mode="ios" form="ngForm4" type="submit" ion-button color="stable" block [disabled]="!registerForm3.form.valid" >\n            Далее\n          </button>\n        </div>\n      </form>\n    </div>\n  </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/welcome/welcome.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__providers_img_upload_service__["a" /* ImgUploadServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_diagnostic__["a" /* Diagnostic */]])
    ], WelcomePage);
    return WelcomePage;
}());

//# sourceMappingURL=welcome.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NameandavaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_img_upload_service__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NameandavaPage = /** @class */ (function () {
    function NameandavaPage(navCtrl, navParams, auth, menuCtrl, alertCtrl, loadingCtrl, imgUploadService, actionSheetCtrl, platform, camera) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.menuCtrl = menuCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.imgUploadService = imgUploadService;
        this.actionSheetCtrl = actionSheetCtrl;
        this.platform = platform;
        this.camera = camera;
        this.registerCredentials = {
            name: ''
        };
    }
    NameandavaPage.prototype.ionViewDidEnter = function () {
        //console.log('menu disable');
        this.menuCtrl.enable(false, 'side-menu1');
        this.menuCtrl.swipeEnable(false);
    };
    NameandavaPage.prototype.ionViewWillLeave = function () {
        //console.log('menu enable');
        this.menuCtrl.enable(true, 'side-menu1');
        this.menuCtrl.swipeEnable(true);
    };
    NameandavaPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Подождите...',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    NameandavaPage.prototype.showError = function (text) {
        this.loading.dismiss();
        var alert = this.alertCtrl.create({
            title: 'Ошибка',
            subTitle: text,
            buttons: ['Ок']
        });
        alert.present();
    };
    NameandavaPage.prototype.choiceTypeAvatarSource = function (type) {
        var _this = this;
        var buttons;
        if (this.platform.is('ios')) {
            buttons = [{
                    text: 'Выбрать из библиотеки',
                    //icon: 'photos',
                    role: 'destructive',
                    handler: function () { _this.imgUploadService.getImageAvatar(_this.camera.PictureSourceType.PHOTOLIBRARY, type); }
                }, {
                    text: 'Сделать фотографию',
                    // icon: 'camera',
                    role: 'destructive',
                    handler: function () { _this.imgUploadService.getImageAvatar(_this.camera.PictureSourceType.CAMERA, type); }
                }, {
                    text: 'Отмена',
                    role: 'cancel',
                }];
        }
        else {
            buttons = [{
                    text: 'Выбрать из библиотеки',
                    icon: 'photos',
                    role: 'destructive',
                    handler: function () { _this.imgUploadService.getImageAvatar(_this.camera.PictureSourceType.PHOTOLIBRARY, type); }
                }, {
                    text: 'Сделать фотографию',
                    icon: 'camera',
                    role: 'destructive',
                    handler: function () { _this.imgUploadService.getImageAvatar(_this.camera.PictureSourceType.CAMERA, type); }
                }, {
                    text: 'Отмена',
                    role: 'cancel',
                    icon: 'close',
                }];
        }
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Источник изображения',
            buttons: buttons
        });
        actionSheet.present();
    };
    NameandavaPage.prototype.signStepThree = function () {
        var _this = this;
        this.showLoading();
        this.auth
            .setUserName(this.registerCredentials.name)
            .subscribe(function (res) {
            //console.log(res);
            if (res.status) {
                _this.auth.currentUser.name = _this.registerCredentials.name;
                _this.navCtrl.setRoot('MaintabsPage');
            }
            else {
                _this.showError(res.text);
            }
        });
    };
    NameandavaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-nameandava',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/nameandava/nameandava.html"*/'<ion-content text-center>\n  <div class="backgrounddiv" style="width:100vw;height:100%;margin-top:0px;padding:0px;">\n    <video webkit-playsinline playsinline autoplay loop muted class="backgroundvideo">\n      <source src="assets/video/backgroundvideoFullmp4.mp4" type=\'video/mp4; codecs="h.264"\'>\n      <source src="assets/video/backgroundvideoFullwebm.webm" type="video/webm">\n    </video>\n  <div class="spacer" style="height:10px;" id="spacer1"></div>\n    <div class="formok">\n      <h2 ion-text >Профиль</h2>\n      <p>Введите свое имя и при желании установите фото профиля</p>\n      <form (ngSubmit)="signStepThree()" id="ngForm4" #registerForm="ngForm">\n        <div class="spacer" style="height:20px;" ></div>\n        <img class="avatar" src="{{auth.currentUser.avatar}}"  (click)="choiceTypeAvatarSource(\'avatar\')"/>\n\n        <ion-item>\n          <ion-label floating>Введите имя</ion-label>\n          <ion-input type="text" name="name" placeholder=""  minlength="2" maxlength="60" value="" [(ngModel)]="registerCredentials.name" required></ion-input>\n        </ion-item>\n\n        <button class="button-next" mode="ios" form="ngForm4" type="submit" ion-button color="stable" block [disabled]="!registerForm.form.valid">\n          Далее\n        </button>\n      </form>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/nameandava/nameandava.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_img_upload_service__["a" /* ImgUploadServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */]])
    ], NameandavaPage);
    return NameandavaPage;
}());

//# sourceMappingURL=nameandava.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_push__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PushServiceProvider = /** @class */ (function () {
    function PushServiceProvider(auth, push, platform) {
        this.auth = auth;
        this.push = push;
        this.platform = platform;
        this.initPushStatus = false;
        //console.log('Hello PushServiceProvider Provider');
    }
    PushServiceProvider.prototype.initPushNotification = function () {
        var _this = this;
        if (!this.platform.is('cordova')) {
            console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
            return;
        }
        var options = {
            android: {
                senderID: '525559497709',
                clearBadge: true,
                forceShow: true
            },
            ios: {
                alert: 'true',
                badge: false,
                sound: 'true'
            },
            windows: {}
        };
        this.pushObject = this.push.init(options);
        this.pushObject.on('registration').subscribe(function (data) {
            //console.log('device token -> ' + data.registrationId);
            //TODO - send device token to server
            _this.auth.saveToken(data.registrationId, _this.platform.platforms())
                .subscribe(function (res) {
                //console.log('Обновление токена устройства',res)
            });
        });
        this.initPushStatus = true;
        return this.pushObject;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* Nav */])
    ], PushServiceProvider.prototype, "navCtrl", void 0);
    PushServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_push__["a" /* Push */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["p" /* Platform */]])
    ], PushServiceProvider);
    return PushServiceProvider;
}());

//# sourceMappingURL=push-service.js.map

/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export CalendarEvent */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CalendarProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_date_fns_parse__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_date_fns_parse___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_date_fns_parse__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { format } from 'date-fns'
var CalendarEvent = /** @class */ (function () {
    function CalendarEvent(data) {
        if (data === void 0) { data = {}; }
        this.id = data.id;
        this.title = data.title;
        this.startTime = __WEBPACK_IMPORTED_MODULE_4_date_fns_parse___default()(data.startTime); //new Date();
        this.endTime = __WEBPACK_IMPORTED_MODULE_4_date_fns_parse___default()(data.endTime);
        this.allDay = data.allDay;
        this.userName = data.userName;
        this.userAvatar = data.userAvatar;
        this.serviceTitle = data.serviceTitle;
        this.status = data.status;
        this.userID = data.userID;
        this.serviceTime = data.serviceTime;
        this.serviceID = data.serviceID;
        this.servicePrice = data.servicePrice;
        this.comment = data.comment;
        this.userPhone = data.userPhone;
        this.masterID = data.masterID;
        this.masterName = data.masterName;
        this.masterAvatar = data.masterAvatar;
        this.masterPhone = data.masterPhone;
    }
    return CalendarEvent;
}());

var CalendarProvider = /** @class */ (function () {
    function CalendarProvider(http, auth, config) {
        this.http = http;
        this.auth = auth;
        this.config = config;
        //console.log('Hello CalendarProvider Provider');
    }
    CalendarProvider.prototype.getCalendarEvents = function (startTime, endTime, masterID) {
        if (masterID === void 0) { masterID = '0'; }
        var sUrl = this.config.apiURL + 'fn/calendar.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('getCalendarEvents', '1');
            dataForm.append('start', startTime);
            dataForm.append('end', endTime);
            dataForm.append('masterID', masterID);
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            masterID: masterID,
            end: endTime,
            start: startTime,
            getCalendarEvents: 1,
        });
        return this.http.post(sUrl, body)
            .map(function (res) {
            if (res.status) {
                return res.list.map(function (item) {
                    return new CalendarEvent(item);
                });
            }
            else
                return [];
        });
    };
    CalendarProvider.prototype.getEvent = function (id) {
        var sUrl = this.config.apiURL + 'fn/calendar.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('getEvent', '1');
            dataForm.append('id', id);
             const body = dataForm.toString();
        */
        var body = JSON.stringify({
            id: id,
            getEvent: 1,
        });
        return this.http.post(sUrl, body)
            .map(function (res) {
            //console.log('getEvent:'+id ,res)
            if (res.status) {
                return new CalendarEvent(res.data);
            }
        });
    };
    CalendarProvider.prototype.addCalendarEvents = function (startTime, endTime, masterid, serviceid, comment, title, allDay) {
        if (comment === void 0) { comment = ''; }
        if (title === void 0) { title = ''; }
        if (allDay === void 0) { allDay = null; }
        var sUrl = this.config.apiURL + 'fn/calendar.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('addCalendarEvent', '1');
            dataForm.append('options', JSON.stringify({
              startTime:startTime,
              endTime:endTime,
              masterid:masterid,
              serviceid:serviceid,
              comment:comment,
              title:title,
              allDay:allDay
            }));
        
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            options: {
                startTime: startTime,
                endTime: endTime,
                masterid: masterid,
                serviceid: serviceid,
                comment: comment,
                title: title,
                allDay: allDay
            },
            addCalendarEvent: 1,
        });
        return this.http.post(sUrl, body);
    };
    CalendarProvider.prototype.changeEventStatus = function (id, status) {
        var sUrl = this.config.apiURL + 'fn/calendar.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('changeEventStatus', '1');
            dataForm.append('id', id);
            dataForm.append('status', status);
        
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            id: id,
            status: status,
            changeEventStatus: 1,
        });
        return this.http.post(sUrl, body);
    };
    CalendarProvider.prototype.getAllMasterEvents = function (startFrom, type) {
        var sUrl = this.config.apiURL + 'fn/calendar.php';
        /*    const headers = {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + this.auth.jwt
              }
            };*/
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('getAllMasterEvents', '1');
            dataForm.append('startFrom', startFrom);
            dataForm.append('type', type);
        
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            type: type,
            startFrom: startFrom,
            getAllMasterEvents: 1,
        });
        return this.http.post(sUrl, body)
            .map(function (res) {
            //console.log(res)
            if (res.status) {
                return res.list.map(function (item) {
                    return new CalendarEvent(item);
                });
            }
            else
                return [];
        });
    };
    CalendarProvider.prototype.getAllClientEvents = function (startFrom, type) {
        var sUrl = this.config.apiURL + 'fn/calendar.php';
        /*    const headers = {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + this.auth.jwt
              }
            };*/
        /*    const dataForm = new URLSearchParams();
            dataForm.append('getAllClientEvents', '1');
            dataForm.append('startFrom', startFrom);
            dataForm.append('type', type);
        
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            type: type,
            startFrom: startFrom,
            getAllClientEvents: 1,
        });
        return this.http.post(sUrl, body)
            .map(function (res) {
            //console.log(res)
            if (res.status) {
                return res.list.map(function (item) {
                    return new CalendarEvent(item);
                });
            }
            else
                return [];
        });
    };
    CalendarProvider.prototype.getClientEventCount = function () {
        var sUrl = this.config.apiURL + 'fn/calendar.php';
        var body = JSON.stringify({
            getClientEventCount: 1,
        });
        return this.http.post(sUrl, body);
    };
    CalendarProvider.prototype.getMasterEventCount = function () {
        var sUrl = this.config.apiURL + 'fn/calendar.php';
        var body = JSON.stringify({
            getMastertEventCount: 1,
        });
        return this.http.post(sUrl, body);
    };
    CalendarProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__config__["a" /* ConfigProvider */]])
    ], CalendarProvider);
    return CalendarProvider;
}());

//# sourceMappingURL=calendar.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__chat_chat__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__ = __webpack_require__(75);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ChatsPage = /** @class */ (function () {
    function ChatsPage(platform, navCtrl, auth, chatService, config, alertCtrl, sqlite) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.chatService = chatService;
        this.config = config;
        this.alertCtrl = alertCtrl;
        this.sqlite = sqlite;
        this.expenses = [];
        this.totalIncome = 0;
        this.totalExpense = 0;
        this.balance = 0;
        this.messageList = [];
        this.waitLoading = true;
        this.resumeEvent = platform.resume.subscribe(function (e) {
            if (_this.navCtrl.getActive().name == 'ChatsPage') {
                //console.log("resume called");
                _this.updateUnreadMsgCount = setInterval(function () {
                    _this.getCountMsg();
                }, 2000);
            }
        });
        this.pauseEvent = platform.pause.subscribe(function (e) {
            if (_this.navCtrl.getActive().name == 'ChatsPage') {
                //console.log("pause called");
                clearInterval(_this.updateUnreadMsgCount);
            }
        });
    }
    ChatsPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.getRooms();
        this.updateUnreadMsgCount = setInterval(function () {
            _this.getCountMsg();
        }, 2000);
    };
    ChatsPage.prototype.ionViewWillLeave = function () {
        // unsubscribe
        this.resumeEvent.unsubscribe();
        this.pauseEvent.unsubscribe();
        clearInterval(this.updateUnreadMsgCount);
    };
    ChatsPage.prototype.getRooms = function () {
        var _this = this;
        var tmp;
        return this.chatService
            .getUserList()
            .subscribe(function (res) {
            tmp = res;
            if (tmp.status == 'success') {
                _this.messageList = tmp.msgList;
                _this.waitLoading = false;
            }
            else {
                console.log('no internet');
            }
        });
    };
    ChatsPage.prototype.getCountMsg = function () {
        var _this = this;
        return this.chatService
            .unreadCountMsg()
            .subscribe(function (res) {
            //console.log('Обновление списка чатов',res)
            if (_this.unreadMsg != res.count)
                _this.getRooms();
            _this.unreadMsg = res.count;
        });
    };
    ChatsPage.prototype.delChatConfirm = function (roomid) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Удаление чата",
            message: 'Вы действительно хотите удалить чат? Данный чат невозможно будет восстановить.',
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                },
                {
                    text: 'Удалить',
                    handler: function () {
                        _this.chatService.msgRemoveRoom(roomid).subscribe(function (res) {
                            if (res.status) {
                                _this.getRooms();
                            }
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    ChatsPage.prototype.goToProfile = function (id, type) {
        if (type == 'single')
            this.navCtrl.push('ProfileViewPage', { id: id });
        if (type == 'group')
            this.navCtrl.push('GroupViewPage', { id: id });
    };
    ChatsPage.prototype.goToChat = function (params) {
        this.navCtrl.popToRoot();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__chat_chat__["a" /* Chat */], params);
    };
    ChatsPage.prototype.peopleSearch = function () {
        this.navCtrl.push('NewChatPage');
    };
    ChatsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chats',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/chats/chats.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Чаты\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="peopleSearch()">\n        <ion-icon name="ios-create-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content id="chats" class="chatsPading">\n  <!--<ion-searchbar (ionInput)="getItems($event)" placeholder="Поиск по сообщениям"></ion-searchbar>-->\n  <div ion-fixed class="wait-loading" *ngIf="waitLoading">\n    <ion-spinner name="crescent"></ion-spinner>\n  </div>\n  <ion-list id="chats-list4" >\n    <ion-item-sliding *ngFor="let row of messageList;">\n      <ion-item color="none"  id="chats-list-item4">\n        <ion-avatar item-left  (click)="goToProfile(row.id, row.type)">\n          <img src="{{row.avatar ? row.avatar: \'assets/img/profile.png\'}}"/>\n        </ion-avatar>\n        <h2 class="nameUserChats" (click)="goToChat({\'roomid\':row.roomid, \'roomname\':row.name})">\n          <b>{{row.name}}</b>\n          <span *ngIf="row.moreoneday==0" class="timeMes">{{row.date | formatTime:\'HH:mm\'}}</span>\n          <span *ngIf="row.moreoneday==1" class="timeMes">{{row.date | formatTime:\'DD MMM\'}}</span>\n        </h2>\n        <p class="textUserChats" (click)="goToChat({\'roomid\':row.roomid, \'roomname\':row.name})">\n          <ion-icon class="iconRead" [ngStyle]="{\'color\': row.statusView===\'read\'? \'#5ecae4\' : \'gray\'}" *ngIf="(row.statusView === \'success\' || row.statusView === \'read\') && row.clientid==auth.currentUser.id" name="md-done-all"></ion-icon>\n          <span *ngIf="row.clientid!=auth.currentUser.id && row.type==\'group\'" class="teamMenChats">{{row.nameuser}}:</span>\n          <ion-icon style="font-size: 15px!important;" *ngIf="row.text==\'GIF\'" name="image"></ion-icon>\n          <ion-icon style="font-size: 15px!important;" *ngIf="row.text==\'Аудиозапись\'" name="mic"></ion-icon>\n          <ion-icon style="font-size: 15px!important;" *ngIf="row.text==\'Изображение\'" name="camera"></ion-icon>\n          <ion-icon style="font-size: 15px!important;" *ngIf="row.text==\'Видео\'" name="videocam"></ion-icon>\n          <ion-icon style="font-size: 15px!important;" *ngIf="row.text==\'Местоположение\'" name="pin"></ion-icon>\n          <ion-icon style="font-size: 15px!important;" *ngIf="row.text==\'Стикер\'" name="radio-button-on"></ion-icon> {{row.text}}\n        </p>\n        <ion-badge class="newMesColor" item-end *ngIf="row.unread>0">{{row.unread}}</ion-badge>\n      </ion-item>\n      <ion-item-options icon-start (ionSwipe)="delChatConfirm(row.roomid)">\n        <button class="iconTrash" icon-only expandable (click)="delChatConfirm(row.roomid)">\n          <ion-icon name="ios-trash-outline"></ion-icon>\n        </button>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/chats/chats.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_7__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__["a" /* SQLite */]])
    ], ChatsPage);
    return ChatsPage;
}());

//# sourceMappingURL=chats.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OptPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_push_service__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__nameandava_nameandava__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the OptPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OptPage = /** @class */ (function () {
    function OptPage(navCtrl, navParams, menuCtrl, auth, alertCtrl, loadingCtrl, pushService, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.pushService = pushService;
        this.events = events;
        this.phone = '';
        this.code = '';
        this.repeatButton = false;
        this.repeatButtonText = 60;
        this.phone = this.navParams.get('phone');
        this.repeatButtonInterval = setInterval(function () {
            _this.repeatButtonText--;
            if (_this.repeatButtonText < 0) {
                clearInterval(_this.repeatButtonInterval);
                _this.repeatButtonText = 'Повторить';
                _this.repeatButton = true;
            }
        }, 1000);
    }
    OptPage.prototype.ionViewDidEnter = function () {
        //console.log('menu disable');
        this.menuCtrl.enable(false, 'side-menu1');
        this.menuCtrl.swipeEnable(false);
    };
    OptPage.prototype.ionViewWillLeave = function () {
        //console.log('menu enable');
        this.menuCtrl.enable(true, 'side-menu1');
        this.menuCtrl.swipeEnable(true);
    };
    OptPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Подождите...',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    OptPage.prototype.showError = function (text) {
        this.loading.dismiss();
        var alert = this.alertCtrl.create({
            title: 'Ошибка',
            subTitle: text,
            buttons: ['Ок']
        });
        alert.present();
    };
    OptPage.prototype.checkOnInput = function () {
        if (this.code.length >= 4) {
            this.signStepTwo();
        }
    };
    OptPage.prototype.signStepTwo = function () {
        var _this = this;
        this.showLoading();
        this.auth
            .verifyCode(this.phone, this.code)
            .subscribe(function (res) {
            //console.log(res);
            if (res.status) {
                _this.auth.setUserInfo(res.user, res.user.jwt);
                __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */].jwt2 = res.user.jwt;
                _this.events.publish('myPushInit');
                if (res.user.name == null || res.user.name == '')
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__nameandava_nameandava__["a" /* NameandavaPage */]);
                else
                    _this.navCtrl.setRoot('MaintabsPage');
                //this.navCtrl.push(OptPage, params);
            }
            else {
                _this.loading.dismissAll();
                _this.showError('Неверный код подтверждения');
            }
        });
    };
    OptPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-opt',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/opt/opt.html"*/'<ion-content padding text-center>\n  <form (ngSubmit)="signStepTwo()" id="ngForm3" #registerForm="ngForm">\n    <div class="spacer" style="height:10px;" id="spacer1"></div>\n    <h2 ion-text >Подтверждение номера</h2>\n    <p>Введите проверочный код отправленный Вам в SMS на номер: {{phone}}</p>\n    <ion-item no-padding>\n      <ion-label floating>\n        4-значный код\n      </ion-label>\n      <ion-input type="tel" name="code" placeholder="" (ionChange)="checkOnInput()" [brmasker]="{mask:\'0000\', len:4}"  minlength="4" maxlength="4" value="" [(ngModel)]="code" required></ion-input>\n    </ion-item>\n\n    <button class="button-next" mode="ios" form="ngForm3" type="submit" ion-button color="stable" block [disabled]="!registerForm.form.valid">\n      Далее\n    </button>\n  </form>\n</ion-content>\n\n<ion-footer class="footer-button" no-border ion-fixed>\n\n</ion-footer>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/opt/opt.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_push_service__["a" /* PushServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */]])
    ], OptPage);
    return OptPage;
}());

//# sourceMappingURL=opt.js.map

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return User; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import * as $ from 'jquery';



var User = /** @class */ (function () {
    function User() {
        this.name = '';
        this.type = 1;
    }
    return User;
}());

var AuthService = /** @class */ (function () {
    function AuthService(http, config) {
        this.http = http;
        this.config = config;
        this.currentUser = new User();
        //console.log(this.currentUser)
    }
    AuthService_1 = AuthService;
    AuthService.prototype.login = function (credentials) {
        if (credentials.email === null || credentials.password === null) {
            return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw("Please insert credentials");
        }
        else {
            var sUrl = this.config.apiURL + 'fn/auth.php';
            var dataForm = new URLSearchParams();
            dataForm.append('login', credentials.email);
            dataForm.append('password', credentials.password);
            var body = dataForm.toString();
            return this.http.post(sUrl, body);
        }
    };
    AuthService.prototype.register = function (credentials) {
        if (credentials.email === null || credentials.password === null) {
            return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].throw("Please insert credentials");
        }
        else {
            // At this point store the credentials to your backend!
            return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].create(function (observer) {
                observer.next(true);
                observer.complete();
            });
        }
    };
    AuthService.prototype.checkHashID = function (hashid) {
        if (hashid === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/check_session.php';
            /*
                  const dataForm = new URLSearchParams();
                  //dataForm.append('hashid', hashid);
                  const body = dataForm.toString();*/
            var body = JSON.stringify({});
            return this.http.post(sUrl, body);
        }
    };
    AuthService.prototype.updateStatus = function () {
        var sUrl = this.config.apiURL + 'fn/user.php';
        /*
              const dataForm = new URLSearchParams();
              dataForm.append('updStatus', '1');
              const body = dataForm.toString();
        */
        var body = JSON.stringify({
            updStatus: 1,
        });
        return this.http.post(sUrl, body);
    };
    AuthService.prototype.getUserInfo = function () {
        return this.currentUser;
    };
    AuthService.prototype.setUserInfo = function (user, saveJWT) {
        if (saveJWT === void 0) { saveJWT = false; }
        this.currentUser = user;
        if (saveJWT) {
            this.storage = new __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]({
                name: '__mydbbloggerchu',
                driverOrder: ['indexeddb', 'sqlite', 'websql']
            });
            this.storage.set('hashid', saveJWT);
        }
        //console.log(this.currentUser)
    };
    AuthService.prototype.logout = function () {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].create(function (observer) {
            _this.storage = new __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]({
                name: '__mydbbloggerchu',
                driverOrder: ['indexeddb', 'sqlite', 'websql']
            });
            _this.storage.remove('hashid');
            AuthService_1.jwt2 = false;
            observer.next(true);
            observer.complete();
        });
    };
    AuthService.getJTW = function () {
        return this.jwt2;
    };
    AuthService.prototype.saveToken = function (token, platform) {
        if (token === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/save_token.php';
            /*
                  const dataForm = new URLSearchParams();
                  dataForm.append('token', token);
                  dataForm.append('platform', platform[2]);
                  const body = dataForm.toString();
            */
            var body = JSON.stringify({
                token: token,
                platform: platform[2],
            });
            return this.http.post(sUrl, body);
        }
    };
    AuthService.prototype.saveTimezone = function (timezone, iana_timezone) {
        var sUrl = this.config.apiURL + 'fn/save_timezone.php';
        var body = JSON.stringify({
            timezone: timezone,
            iana_timezone: iana_timezone,
        });
        return this.http.post(sUrl, body);
    };
    AuthService.prototype.verifyPhone = function (phone) {
        if (phone === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/auth.php';
            /*      const dataForm = new URLSearchParams();
                  dataForm.append('phone', phone);
                  dataForm.append('verifyPhone', phone);
                  const body = dataForm.toString();*/
            var body = JSON.stringify({ phone: phone, verifyPhone: phone });
            return this.http.post(sUrl, body);
        }
    };
    AuthService.prototype.verifyCode = function (phone, code) {
        if (phone === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/auth.php';
            /*      const dataForm = new URLSearchParams();
                  dataForm.append('phone', phone);
                  dataForm.append('code', code);
                  dataForm.append('verifyCode', phone);
                  const body = dataForm.toString();*/
            var body = JSON.stringify({ phone: phone, code: code, verifyCode: phone });
            return this.http.post(sUrl, body);
        }
    };
    AuthService.prototype.setUserName = function (name) {
        if (name === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/user.php';
            /*      const dataForm = new URLSearchParams();
                  dataForm.append('name', name);
                  dataForm.append('setUserName', '1');
                  const body = dataForm.toString();*/
            var body = JSON.stringify({
                name: name,
                setUserName: 1,
            });
            return this.http.post(sUrl, body);
        }
    };
    AuthService.prototype.setUserCity = function (name) {
        if (name === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/user.php';
            /*
            
                  const dataForm = new URLSearchParams();
                  dataForm.append('name', name);
                  dataForm.append('setUserCity', '1');
                  const body = dataForm.toString();
            */
            var body = JSON.stringify({
                name: name,
                setUserCity: 1,
            });
            return this.http.post(sUrl, body);
        }
    };
    AuthService.prototype.setUserFullAddress = function (address) {
        if (name === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/user.php';
            /*
                  const dataForm = new URLSearchParams();
                  dataForm.append('full_address', address.full_address);
                  dataForm.append('lat', address.lat);
                  dataForm.append('lng', address.lng);
                  dataForm.append('setUserFullAddress', '1');
                  const body = dataForm.toString();
            */
            var body = JSON.stringify({
                full_address: address.full_address,
                lat: address.lat,
                lng: address.lng,
                setUserFullAddress: 1,
            });
            return this.http.post(sUrl, body);
        }
    };
    AuthService.prototype.setUserSettings = function (data) {
        var sUrl = this.config.apiURL + 'fn/user.php';
        var body = JSON.stringify({
            data: data,
            setUserSettings: 1,
        });
        return this.http.post(sUrl, body);
    };
    AuthService.jwt2 = false;
    AuthService = AuthService_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__config__["a" /* ConfigProvider */]])
    ], AuthService);
    return AuthService;
    var AuthService_1;
}());

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NointernetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__welcome_welcome__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import {LoginPage} from "../login/login";



/**
 * Generated class for the NointernetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NointernetPage = /** @class */ (function () {
    function NointernetPage(navCtrl, navParams, network, platform, auth, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.network = network;
        this.platform = platform;
        this.auth = auth;
        this.storage = storage;
        this.checkIntenet();
        this.resumeEvent = platform.resume.subscribe(function (e) {
            _this.checkIntenet();
        });
        this.pauseEvent = platform.pause.subscribe(function (e) {
            _this.connectSubscription.unsubscribe();
        });
    }
    NointernetPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad NointernetPage');
    };
    NointernetPage.prototype.ionViewWillLeave = function () {
        this.connectSubscription.unsubscribe();
        this.resumeEvent.unsubscribe();
        this.pauseEvent.unsubscribe();
    };
    NointernetPage.prototype.checkIntenet = function () {
        var _this = this;
        this.connectSubscription = this.network.onConnect().subscribe(function () {
            //console.log('network connected!');
            // We just got a connection but we need to wait briefly
            // before we determine the connection type. Might need to wait.
            // prior to doing any api requests as well.
            _this.storage.ready().then(function () {
                _this.storage.get('hashid').then(function (hashid) {
                    //console.log(hashid)
                    if (hashid && hashid != '' && hashid != undefined) {
                        __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */].jwt2 = hashid;
                        _this.auth
                            .checkHashID(hashid)
                            .subscribe(function (res) {
                            //console.log(res)
                            if (res.status) {
                                _this.auth.setUserInfo(res.user);
                                _this.navCtrl.setRoot('MaintabsPage');
                                // //console.log(this.navCtrl.getActive().name)
                                //if (this.navCtrl.getActive().name == 'LoginPage') this.navCtrl.setRoot(HomePage);
                                ////console.log(this.navCtrl.getActive())
                            }
                        });
                    }
                    else {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__welcome_welcome__["a" /* WelcomePage */]);
                    }
                });
            });
        });
    };
    NointernetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-nointernet',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/nointernet/nointernet.html"*/'<ion-content padding text-center>\n  <ion-icon name="alert" style="zoom:6.0;color:#e8e8e8"></ion-icon>\n  <h2>Нет интернета</h2>\n  <p>Для функционирования приложения необходимо подключение к интернету</p>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/nointernet/nointernet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], NointernetPage);
    return NointernetPage;
}());

//# sourceMappingURL=nointernet.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeoServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_geocoder__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var GeoServiceProvider = /** @class */ (function () {
    function GeoServiceProvider(http, geolocation, config, nativeGeocoder) {
        //console.log('Hello GeoServiceProvider Provider');
        this.http = http;
        this.geolocation = geolocation;
        this.config = config;
        this.nativeGeocoder = nativeGeocoder;
        this.lat = 55.75370903771494;
        this.lng = 37.61981338262558;
    }
    GeoServiceProvider.prototype.geocode = function () {
        var _this = this;
        return this.geolocation.getCurrentPosition().then(function (resp) {
            // resp.coords.latitude
            // resp.coords.longitude
            _this.lat = resp.coords.latitude;
            _this.lng = resp.coords.longitude;
            return _this.nativeGeocoder.reverseGeocode(_this.lat, _this.lng)
                .then(function (result) {
                //console.log(result)
                return result;
            })
                .catch(function (error) {
                //console.log(error)
            });
        }).catch(function (error) {
            //console.log('Error getting location', error);
        });
    };
    GeoServiceProvider.prototype.getCurrentPosition = function () {
        return this.geolocation.getCurrentPosition();
    };
    GeoServiceProvider.prototype.geocodeHTTP = function (latitude, longitude) {
        var sUrl = this.config.apiURL + 'fn/users.php';
        var body = JSON.stringify({
            url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&key=AIzaSyCOTKyaRVx1ZcvRoavrFQnCCUrymlLjts4&language=RU',
            getGooglePlace: 1,
        });
        return this.http.post(sUrl, body);
        //return this.httpIonic.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&key=AIzaSyCOTKyaRVx1ZcvRoavrFQnCCUrymlLjts4',{}, {})
    };
    GeoServiceProvider.prototype.placeDetail = function (placeid) {
        var sUrl = this.config.apiURL + 'fn/users.php';
        var body = JSON.stringify({
            url: 'https://maps.googleapis.com/maps/api/place/details/json?placeid=' + placeid + '&key=AIzaSyCOTKyaRVx1ZcvRoavrFQnCCUrymlLjts4&language=RU',
            getGooglePlace: 1,
        });
        return this.http.post(sUrl, body);
        //return this.httpIonic.get('https://maps.googleapis.com/maps/api/place/details/json?placeid='+placeid+'&key=AIzaSyCOTKyaRVx1ZcvRoavrFQnCCUrymlLjts4', {}, {})
    };
    GeoServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_5__config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_geocoder__["a" /* NativeGeocoder */]])
    ], GeoServiceProvider);
    return GeoServiceProvider;
}());

//# sourceMappingURL=geo-service.js.map

/***/ }),

/***/ 212:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 212;

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(487);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ConfigProvider = /** @class */ (function () {
    function ConfigProvider(platform) {
        this.platform = platform;
        //apiURL: string='/';
        this.apiURL = 'http://ezmaven.com/bloggerchu/';
        this.eventStatus = {
            0: { name: 'запланирован', color: 'blue' },
            1: { name: 'выполнен', color: 'green' },
            2: { name: 'в процессе', color: 'orange' },
            3: { name: 'отменен', color: 'red' },
            4: { name: 'отказ', color: 'red' },
            5: { name: 'фиктивный', color: 'red' },
            6: { name: 'не завершен', color: 'orange' },
            7: { name: 'не подтвержден', color: 'orange' },
        };
        this.dayName = {
            0: { name: 'воскресенье' },
            1: { name: 'понедельник' },
            2: { name: 'вторник' },
            3: { name: 'среда' },
            4: { name: 'четверг' },
            5: { name: 'пятница' },
            6: { name: 'суббота' },
        };
        this.themeColor = {
            headerColor: 'primary',
            segmentColor: 'dark',
            primary: 'primary',
            lineButtons: 'dark'
        };
        this.theme = new __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["BehaviorSubject"]('light-theme');
        //this.themeColor.headerColor='dark';
        this.themeColor.headerColor = platform.is('android') ? 'primary' : 'light';
        //console.warn('platform: ',platform.is('android') ? 'android' : 'ios')
    }
    ConfigProvider.prototype.setActiveTheme = function (val) {
        this.theme.next(val);
        if (val == 'light-theme') {
            this.themeColor.headerColor = this.platform.is('android') ? 'primary' : 'light';
            this.themeColor.segmentColor = 'dark';
            this.themeColor.lineButtons = 'dark';
            this.themeColor.primary = 'primary';
        }
        if (val == 'dark-theme') {
            this.themeColor.headerColor = 'dark';
            this.themeColor.segmentColor = 'light';
            this.themeColor.lineButtons = 'light';
            this.themeColor.primary = 'primary2';
        }
    };
    ConfigProvider.prototype.getActiveTheme = function () {
        return this.theme.asObservable();
    };
    ConfigProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */]])
    ], ConfigProvider);
    return ConfigProvider;
}());

//# sourceMappingURL=config.js.map

/***/ }),

/***/ 256:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about-app/about-app.module": [
		841,
		37
	],
	"../pages/acceptorder/acceptorder.module": [
		842,
		36
	],
	"../pages/add-to-group/add-to-group.module": [
		843,
		35
	],
	"../pages/app-review/app-review.module": [
		844,
		34
	],
	"../pages/archive/archive.module": [
		845,
		33
	],
	"../pages/calendar-list/calendar-list.module": [
		847,
		32
	],
	"../pages/calendar/calendar.module": [
		846,
		0
	],
	"../pages/call-yes/call-yes.module": [
		879,
		31
	],
	"../pages/call/call.module": [
		848,
		30
	],
	"../pages/callinfo/callinfo.module": [
		880,
		29
	],
	"../pages/callstory/callstory.module": [
		849,
		28
	],
	"../pages/chat-group/chat-group.module": [
		850,
		27
	],
	"../pages/chats-group/chats-group.module": [
		851,
		26
	],
	"../pages/choice-city/choice-city.module": [
		852,
		1
	],
	"../pages/create-group/create-group.module": [
		878,
		25
	],
	"../pages/get-media-modal/get-media-modal.module": [
		853,
		24
	],
	"../pages/giveadd/giveadd.module": [
		854,
		23
	],
	"../pages/group-view/group-view.module": [
		855,
		22
	],
	"../pages/history-action/history-action.module": [
		856,
		21
	],
	"../pages/invite-users/invite-users.module": [
		857,
		20
	],
	"../pages/maintabs/maintabs.module": [
		859,
		19
	],
	"../pages/masters-map/masters-map.module": [
		858,
		18
	],
	"../pages/mykassa/mykassa.module": [
		861,
		17
	],
	"../pages/nameandava/nameandava.module": [
		860,
		42
	],
	"../pages/new-chanel/new-chanel.module": [
		862,
		16
	],
	"../pages/new-chat/new-chat.module": [
		863,
		15
	],
	"../pages/new-contact/new-contact.module": [
		864,
		14
	],
	"../pages/nointernet/nointernet.module": [
		871,
		41
	],
	"../pages/opt/opt.module": [
		866,
		40
	],
	"../pages/order/order.module": [
		865,
		13
	],
	"../pages/people/people.module": [
		867,
		12
	],
	"../pages/peoplecall/peoplecall.module": [
		868,
		11
	],
	"../pages/profile-options/profile-options.module": [
		882,
		10
	],
	"../pages/profile-popover/profile-popover.module": [
		869,
		9
	],
	"../pages/profile-view/profile-view.module": [
		881,
		8
	],
	"../pages/search/search.module": [
		870,
		7
	],
	"../pages/secret/secret.module": [
		876,
		6
	],
	"../pages/signup/signup.module": [
		877,
		39
	],
	"../pages/statement/statement.module": [
		883,
		5
	],
	"../pages/wait-loading/wait-loading.module": [
		872,
		4
	],
	"../pages/wallet/wallet.module": [
		873,
		3
	],
	"../pages/welcome/welcome.module": [
		874,
		38
	],
	"../pages/youcall/youcall.module": [
		875,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 256;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmojiProvider; });
var EmojiProvider = /** @class */ (function () {
    function EmojiProvider() {
    }
    EmojiProvider.prototype.getEmojis = function () {
        var EMOJIS = "😀 😃 😄 😁 😆 😅 😂 🤣 ☺️ 😊 😇 🙂 🙃 😉 😌 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 🤡 🤠 😏 😒 😞 😔 😟 😕 🙁" +
            " ☹️ 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 🤤 😭 😓 😪 😴 🙄 🤔 🤥 😬 🤐 🤢 🤧 😷 🤒 🤕 😈 👿" +
            " 👹 👺 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊ 🤛 🤜 🤞 ✌️ 🤘 👌 👈 👉 👆 👇 ☝️ ✋ 🤚" +
            " 🖐 🖖 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 👶 👦 👧 👨 👩 👱‍♀️ 👱 👴 👵 👲 👳‍♀️ 👳 👮‍♀️ 👮 👷‍♀️ 👷" +
            " 💂‍♀️ 💂 🕵️‍♀️ 🕵️ 👩‍⚕️ 👨‍⚕️ 👩‍🌾 👨‍🌾 👩‍🍳 👨‍🍳 👩‍🎓 👨‍🎓 👩‍🎤 👨‍🎤 👩‍🏫 👨‍🏫 👩‍🏭 👨‍🏭 👩‍💻 👨‍💻 👩‍💼 👨‍💼 👩‍🔧 👨‍🔧 👩‍🔬 👨‍🔬" +
            " 👩‍🎨 👨‍🎨 👩‍🚒 👨‍🚒 👩‍✈️ 👨‍✈️ 👩‍🚀 👨‍🚀 👩‍⚖️ 👨‍⚖️ 🤶 🎅 👸 🤴 👰 🤵 👼 🤰 🙇‍♀️ 🙇 💁 💁‍♂️ 🙅 🙅‍♂️ 🙆 🙆‍♂️ 🙋 🙋‍♂️ 🤦‍♀️ 🤦‍♂️ 🤷‍♀" +
            "️ 🤷‍♂️ 🙎 🙎‍♂️ 🙍 🙍‍♂️ 💇 💇‍♂️ 💆 💆‍♂️ 🕴 💃 🕺 👯 👯‍♂️ 🚶‍♀️ 🚶 🏃‍♀️ 🏃 👫 👭 👬 💑 👩‍❤️‍👩 👨‍❤️‍👨 💏 👩‍❤️‍💋‍👩 👨‍❤️‍💋‍👨 👪 👨‍👩‍👧" +
            " 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👩‍👩‍👦 👩‍👩‍👧 👩‍👩‍👧‍👦 👩‍👩‍👦‍👦 👩‍👩‍👧‍👧 👨‍👨‍👦 👨‍👨‍👧 👨‍👨‍👧‍👦 👨‍👨‍👦‍👦 👨‍👨‍👧‍👧 👩‍👦 👩‍👧" +
            " 👩‍👧‍👦 👩‍👦‍👦 👩‍👧‍👧 👨‍👦 👨‍👧 👨‍👧‍👦 👨‍👦‍👦 👨‍👧‍👧 👚 👕 👖 👔 👗 👙 👘 👠 👡 👢 👞 👟 👒 🎩 🎓 👑 ⛑ 🎒 👝 👛 👜 💼 👓" +
            " 🕶 🌂 ☂️";
        var EmojiArr = EMOJIS.split(' ');
        var groupNum = Math.ceil(EmojiArr.length / (24));
        var items = [];
        for (var i = 0; i < groupNum; i++) {
            items.push(EmojiArr.slice(i * 24, (i + 1) * 24));
        }
        return items;
    };
    return EmojiProvider;
}());

//# sourceMappingURL=emoji.js.map

/***/ }),

/***/ 434:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Professions */
/* unused harmony export MasterService */
/* unused harmony export MasterReview */
/* unused harmony export People */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import {map} from "rxjs/operator/map";
/*
  Generated class for the ProfileProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var Professions = /** @class */ (function () {
    function Professions() {
    }
    return Professions;
}());

var MasterService = /** @class */ (function () {
    function MasterService(id, title, description, price, time_minutes, active) {
        if (id === void 0) { id = ''; }
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        if (price === void 0) { price = null; }
        if (time_minutes === void 0) { time_minutes = null; }
        if (active === void 0) { active = 1; }
        this.title = '';
        this.description = '';
        this.active = 1;
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.time_minutes = time_minutes;
        this.active = active;
    }
    return MasterService;
}());

var MasterReview = /** @class */ (function () {
    function MasterReview() {
    }
    return MasterReview;
}());

var People = /** @class */ (function () {
    function People() {
    }
    return People;
}());

var ProfileProvider = /** @class */ (function () {
    function ProfileProvider(http, config) {
        this.http = http;
        this.config = config;
        //console.log('Hello ProfileProvider Provider');
    }
    ProfileProvider.prototype.getProfessions = function () {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        /*    const dataForm = new URLSearchParams();
            dataForm.append('getProfessions', '1');
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            getProfessions: 1,
        });
        return this.http.post(sUrl, body).map(function (res) {
            //console.log(res)
            return res.list;
        });
    };
    ProfileProvider.prototype.getMasterService = function (masterID) {
        if (masterID === void 0) { masterID = '0'; }
        var sUrl = this.config.apiURL + 'fn/profile.php';
        /*    const dataForm = new URLSearchParams();
            dataForm.append('getMasterService', '1');
            dataForm.append('masterID', masterID);
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            masterID: masterID,
            getMasterService: 1,
        });
        return this.http.post(sUrl, body)
            .map(function (res) {
            //console.log(res)
            if (res.status) {
                return res.list.map(function (item) {
                    return new MasterService(item.id, item.title, item.description, parseInt(item.price), parseInt(item.time_minutes), parseInt(item.active));
                });
            }
            else
                return [];
        });
    };
    ProfileProvider.prototype.setProfession = function (profession_id) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        /*    const dataForm = new URLSearchParams();
            dataForm.append('setProfession', '1');
            dataForm.append('profession_id', profession_id);
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            profession_id: profession_id,
            setProfession: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.addService = function (newService) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('addService', '1');
            dataForm.append('options', JSON.stringify(newService));
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            options: newService,
            addService: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.editService = function (newService) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('editService', '1');
            dataForm.append('options', JSON.stringify(newService));
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            options: newService,
            editService: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.deleteService = function (id) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        /*    const dataForm = new URLSearchParams();
            dataForm.append('deleteService', '1');
            dataForm.append('id', id);
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            id: id,
            deleteService: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.getMasterWorkTime = function (id) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        /*    const dataForm = new URLSearchParams();
            dataForm.append('getMasterWorkTime', '1');
            dataForm.append('id', id);
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            id: id,
            getMasterWorkTime: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.setUserRate = function (id, rate) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('setUserRate', '1');
            dataForm.append('id', id);
            dataForm.append('rate', rate);
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            id: id,
            rate: rate,
            setUserRate: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.setMasterRate = function (id, rate, comment, masterid, eventid) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        /*    const dataForm = new URLSearchParams();
            dataForm.append('setMasterRate', '1');
            dataForm.append('options', JSON.stringify({id:id,rate:rate,text:comment, masterid:masterid, eventid:eventid}));
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            options: { id: id, rate: rate, text: comment, masterid: masterid, eventid: eventid },
            setMasterRate: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.getProfileInfo = function (id) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            id: id,
            getProfileInfo: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.blockUsrTo = function (id) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            id: id,
            blockUsrTo: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.unblockUsrTo = function (id) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            id: id,
            unblockUsrTo: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.sendAbortUser = function (id, about) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            id: id,
            about: about,
            sendAbortUser: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.sendOrder = function (siting, dateStart, timeStart, aboutman, Requirements, conditions, userid, name) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            sendorder: true,
            siting: siting,
            dateStart: dateStart,
            timeStart: timeStart,
            aboutman: aboutman,
            conditions: conditions,
            Requirements: Requirements,
            userid: userid,
            name: name
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.getProfileReviews = function (id) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        /*    const dataForm = new URLSearchParams();
            dataForm.append('getProfileReviews', '1');
            dataForm.append('id', id);
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            id: id,
            getProfileReviews: 1,
        });
        return this.http.post(sUrl, body).map(function (res) {
            if (res.status) {
                return res.list;
            }
        });
    };
    ProfileProvider.prototype.getPeople = function (limitFrom, limitTo, options) {
        if (limitFrom === void 0) { limitFrom = 0; }
        if (limitTo === void 0) { limitTo = 10; }
        if (options === void 0) { options = null; }
        var sUrl = this.config.apiURL + 'fn/users.php';
        var body = JSON.stringify({
            options: options,
            limitFrom: limitFrom,
            limitTo: limitTo,
            getPeople: 1,
        });
        return this.http.post(sUrl, body).map(function (res) {
            //console.log(res)
            if (res.status) {
                return res.list;
            }
        });
    };
    ProfileProvider.prototype.addAppReview = function (text) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            text: text,
            addAppReview: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.addNewProfession = function (name) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            name: name,
            addNewProfession: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.sendBuzCoin = function (id, num) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            id: id,
            num: num,
            sendcoin: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.saveKassaOn = function (k, p, kp) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            k: k ? k : null,
            p: p ? p : null,
            kp: kp ? kp : null,
            saveKassaOn: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.getKassaOn = function () {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            getKassaOn: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider.prototype.addToBaseGiveS = function (contact, dateStart, account, givgift, aboutgiv, subnum, price, duration, namegiv, sp, us) {
        var sUrl = this.config.apiURL + 'fn/profile.php';
        var body = JSON.stringify({
            contact: contact,
            dateStart: dateStart,
            account: account,
            givgift: givgift,
            aboutgiv: aboutgiv,
            subnum: subnum,
            price: price,
            duration: duration,
            namegiv: namegiv,
            sp: sp,
            us: us,
            addToBaseGiveS: 1,
        });
        return this.http.post(sUrl, body);
    };
    ProfileProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__config__["a" /* ConfigProvider */]])
    ], ProfileProvider);
    return ProfileProvider;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RelativeTimeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__relative_time__ = __webpack_require__(781);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var RelativeTimeModule = /** @class */ (function () {
    function RelativeTimeModule() {
    }
    RelativeTimeModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__relative_time__["a" /* RelativeTime */],
            ],
            imports: [],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__relative_time__["a" /* RelativeTime */]
            ]
        })
    ], RelativeTimeModule);
    return RelativeTimeModule;
}());

//# sourceMappingURL=relative-time.module.js.map

/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FriendProvider = /** @class */ (function () {
    function FriendProvider(http, config) {
        this.http = http;
        this.config = config;
        //console.log('Hello FriendProvider Provider');
    }
    FriendProvider.prototype.followAdd = function (id) {
        var sUrl = this.config.apiURL + 'fn/friends.php';
        /*    const headers = {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + this.auth.jwt
              }
            };*/
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('followAdd', '1');
            dataForm.append('id', id);
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            id: id,
            followAdd: 1,
        });
        return this.http.post(sUrl, body);
    };
    FriendProvider.prototype.followDel = function (id) {
        var sUrl = this.config.apiURL + 'fn/friends.php';
        /*    const headers = {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + this.auth.jwt
              }
            };*/
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('followDel', '1');
            dataForm.append('id', id);
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            id: id,
            followDel: 1,
        });
        return this.http.post(sUrl, body);
    };
    FriendProvider.prototype.getFollowList = function (id) {
        if (id === void 0) { id = '0'; }
        var sUrl = this.config.apiURL + 'fn/friends.php';
        /*    const dataForm = new URLSearchParams();
            dataForm.append('getFollowList', '1');
            dataForm.append('id', id);
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            id: id,
            getFollowList: 1,
        });
        return this.http.post(sUrl, body).map(function (res) {
            if (res.status) {
                return res.list;
            }
        });
    };
    FriendProvider.prototype.getFollowersList = function (id) {
        if (id === void 0) { id = '0'; }
        var sUrl = this.config.apiURL + 'fn/friends.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('getFollowersList', '1');
            dataForm.append('id', id);
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            id: id,
            getFollowersList: 1,
        });
        return this.http.post(sUrl, body).map(function (res) {
            //console.log('подпочичники',res)
            if (res.status) {
                return res.list;
            }
        });
    };
    FriendProvider.prototype.saveMyContacts = function (data) {
        var sUrl = this.config.apiURL + 'fn/friends.php';
        var body = JSON.stringify({
            data: data,
            saveMyContacts: 1,
        });
        return this.http.post(sUrl, body);
    };
    FriendProvider.prototype.getMyContacts = function () {
        var sUrl = this.config.apiURL + 'fn/friends.php';
        var body = JSON.stringify({
            getMyContacts: 1,
        });
        return this.http.post(sUrl, body).map(function (res) {
            if (res.status) {
                return res.list;
            }
            else
                return [];
        });
    };
    FriendProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__config__["a" /* ConfigProvider */]])
    ], FriendProvider);
    return FriendProvider;
}());

//# sourceMappingURL=friend.js.map

/***/ }),

/***/ 438:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormatTimeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__format_time__ = __webpack_require__(795);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var FormatTimeModule = /** @class */ (function () {
    function FormatTimeModule() {
    }
    FormatTimeModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__format_time__["a" /* FormatTime */],
            ],
            imports: [],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__format_time__["a" /* FormatTime */]
            ]
        })
    ], FormatTimeModule);
    return FormatTimeModule;
}());

//# sourceMappingURL=format-time.module.js.map

/***/ }),

/***/ 448:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__elastic_textarea_elastic_textarea__ = __webpack_require__(784);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__elastic_textarea_elastic_textarea__["a" /* ElasticTextArea */]],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_1__elastic_textarea_elastic_textarea__["a" /* ElasticTextArea */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 449:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export LineItem */
/* unused harmony export LineComment */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LineService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_throw__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_throw___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_throw__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LineItem = /** @class */ (function () {
    function LineItem() {
    }
    return LineItem;
}());

var LineComment = /** @class */ (function () {
    function LineComment() {
    }
    return LineComment;
}());

var LineService = /** @class */ (function () {
    function LineService(http, auth, config) {
        this.http = http;
        this.auth = auth;
        this.config = config;
    }
    LineService.prototype.getLineList = function (limitFrom, limitTo) {
        if (limitTo === void 0) { limitTo = 10; }
        var msgListUrl = this.config.apiURL + 'fn/lenta.php';
        //const dataForm = new URLSearchParams();
        /*
            dataForm.append('killtimeList', '1');
            dataForm.append('limitFrom', limitFrom.toString());
            dataForm.append('limitTo', limitTo.toString());
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            limitFrom: limitFrom,
            limitTo: limitTo,
            killtimeList: 1,
        });
        return this.http.post(msgListUrl, body);
        /*      .map(res=>{
                return res
              }).catch((e)=>{
                return Observable.throw(e);
              });*/
        //.map(response => response.array);
    };
    /* private handleError (error: Response | any) {
       let errMsg: string;
       if (error instanceof Response) {
         const err = error || '';
         errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
       } else {
         errMsg = error.message ? error.message : error.toString();
       }
       console.error(errMsg);
       return Observable.throw(errMsg);
     }
   */
    LineService.prototype.setUpdShow = function (lentaid, text, show) {
        var sUrl = this.config.apiURL + '/fn/lenta.php';
        /*
            const dataForm = new URLSearchParams();
            //dataForm.append('title', title?title:'');
            dataForm.append('text', text?text:'');
            dataForm.append('lentaid', lentaid);
            dataForm.append('show', show);
            dataForm.append('lentaUpdShow', '1');
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            lentaid: lentaid,
            text: text,
            show: show,
            lentaUpdShow: 1,
        });
        return this.http.post(sUrl, body);
    };
    LineService.prototype.addLike = function (lentaid, action) {
        if (action === void 0) { action = null; }
        var sUrl = this.config.apiURL + 'fn/lenta.php';
        /*    const headers = {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer '+this.auth.jwt
              }
            };*/
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('lentaid', lentaid);
            dataForm.append('addLike', '1');
            dataForm.append('action', action);
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            lentaid: lentaid,
            action: action,
            addLike: 1,
        });
        return this.http.post(sUrl, body);
    };
    LineService.prototype.getLineListProfile = function (id) {
        var msgListUrl = this.config.apiURL + 'fn/lenta.php';
        /*
            const dataForm = new URLSearchParams();
        
            dataForm.append('getLineListProfile', '1');
            dataForm.append('id', id);
        
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            id: id,
            getLineListProfile: 1,
        });
        return this.http.post(msgListUrl, body).map(function (res) {
            if (res.status) {
                return res.list;
            }
        });
        /*      .map(res=>{
                return res
              }).catch((e)=>{
                return Observable.throw(e);
              });*/
        //.map(response => response.array);
    };
    LineService.prototype.getLineComments = function (id) {
        var msgListUrl = this.config.apiURL + 'fn/lenta.php';
        /*
        
            const dataForm = new URLSearchParams();
        
            dataForm.append('getLineComments', '1');
            dataForm.append('id', id);
        
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            id: id,
            getLineComments: 1,
        });
        return this.http.post(msgListUrl, body).map(function (res) {
            if (res.status) {
                return res.list;
            }
        });
    };
    LineService.prototype.addComment = function (lentaid, text) {
        var sUrl = this.config.apiURL + 'fn/lenta.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('lentaid', lentaid);
            dataForm.append('text', text);
            dataForm.append('addComment', '1');
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            lentaid: lentaid,
            text: text,
            addComment: 1,
        });
        return this.http.post(sUrl, body);
    };
    LineService.prototype.getRandomLine = function () {
        var msgListUrl = this.config.apiURL + 'fn/lenta.php';
        /*
            const dataForm = new URLSearchParams();
        
            dataForm.append('getRandomLine', '1');
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            getRandomLine: 1,
        });
        return this.http.post(msgListUrl, body).map(function (res) {
            //console.log(res)
            if (res.status) {
                return res.data;
            }
            else {
                return [];
            }
        });
    };
    LineService.prototype.addRandomView = function (lentaid) {
        var sUrl = this.config.apiURL + 'fn/lenta.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('lentaid', lentaid);
            dataForm.append('addRandomView', '1');
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            lentaid: lentaid,
            addRandomView: 1,
        });
        return this.http.post(sUrl, body);
    };
    LineService.prototype.getOneLine = function (id) {
        var msgListUrl = this.config.apiURL + 'fn/lenta.php';
        var body = JSON.stringify({
            getOneLine: 1,
            id: id,
        });
        return this.http.post(msgListUrl, body).map(function (res) {
            //console.log(res)
            if (res.status) {
                return res.data;
            }
            else {
                return [];
            }
        });
    };
    LineService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_6__config__["a" /* ConfigProvider */]])
    ], LineService);
    return LineService;
}());

//# sourceMappingURL=line-service.js.map

/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the HistoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var HistoryProvider = /** @class */ (function () {
    function HistoryProvider(http, config) {
        this.http = http;
        this.config = config;
    }
    HistoryProvider.prototype.getHistoryList = function (limitFrom, limitTo) {
        var sUrl = this.config.apiURL + 'fn/history.php';
        var body = JSON.stringify({
            getHistoryList: 1,
            limitFrom: limitFrom,
            limitTo: limitTo,
        });
        return this.http.post(sUrl, body).map(function (res) {
            console.log(res);
            if (res.status) {
                return res.list;
            }
            else
                return [];
        });
    };
    HistoryProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__config__["a" /* ConfigProvider */]])
    ], HistoryProvider);
    return HistoryProvider;
}());

//# sourceMappingURL=history.js.map

/***/ }),

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Master */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MastersProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Master = /** @class */ (function () {
    function Master() {
        this.id = '';
        this.name = '';
        this.avatar = '';
    }
    return Master;
}());

var MastersProvider = /** @class */ (function () {
    function MastersProvider(http, auth, config) {
        this.http = http;
        this.auth = auth;
        this.config = config;
        //console.log('Hello MastersProvider Provider');
    }
    MastersProvider.prototype.getMastersList = function (limitFrom, limitTo, options) {
        if (limitTo === void 0) { limitTo = 10; }
        if (options === void 0) { options = null; }
        var sUrl = this.config.apiURL + 'fn/users.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('getMastersList', '1');
            dataForm.append('limitFrom', limitFrom.toString());
            dataForm.append('limitTo', limitTo.toString());
            dataForm.append('options', JSON.stringify(options));
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            limitFrom: limitFrom,
            limitTo: limitTo,
            options: options,
            getMastersList: 1,
        });
        return this.http.post(sUrl, body).map(function (res) {
            //console.log(res)
            if (res.status)
                return res.masterList;
        });
    };
    MastersProvider.prototype.getMastersListMap = function (options) {
        if (options === void 0) { options = null; }
        var sUrl = this.config.apiURL + 'fn/users.php';
        /*    const headers = {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + this.auth.jwt
              }
            };*/
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('getMastersListMap', '1');
            dataForm.append('options', JSON.stringify(options));
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            options: options,
            getMastersListMap: 1,
        });
        return this.http.post(sUrl, body).map(function (res) {
            //console.log(res)
            if (res.status)
                return res.masterList;
        });
    };
    MastersProvider.prototype.getMastersCityList = function () {
        var sUrl = this.config.apiURL + 'fn/citylist.php';
        /*
            const dataForm = new URLSearchParams();
            dataForm.append('getCityList', '1');
            const body = dataForm.toString();
        */
        var body = JSON.stringify({
            getCityList: 1,
        });
        return this.http.post(sUrl, body).map(function (res) {
            //console.log(res)
            if (res.status) {
                return res.list;
            }
        });
    };
    MastersProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_3__config__["a" /* ConfigProvider */]])
    ], MastersProvider);
    return MastersProvider;
}());

//# sourceMappingURL=masters.js.map

/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__opt_opt__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, menuCtrl, auth, alertCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.registerCredentials = { tel: '', code: '+7' };
    }
    SignupPage.prototype.ionViewDidEnter = function () {
        //console.log('menu disable')
        this.menuCtrl.enable(false, 'side-menu1');
        this.menuCtrl.swipeEnable(false);
        //if(this.menuCtrl.isEnabled('side-menu1'))
    };
    SignupPage.prototype.ionViewWillLeave = function () {
        //console.log('menu enable')
        this.menuCtrl.enable(true, 'side-menu1');
        this.menuCtrl.swipeEnable(true);
    };
    SignupPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Подождите...',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    SignupPage.prototype.showError = function (text) {
        this.loading.dismiss();
        var alert = this.alertCtrl.create({
            title: 'Ошибка',
            subTitle: text,
            buttons: ['Ок']
        });
        alert.present();
    };
    SignupPage.prototype.signStepOne = function () {
        var _this = this;
        var phone = this.registerCredentials.code + this.registerCredentials.tel;
        //console.log(phone)
        this.showLoading();
        this.auth
            .verifyPhone(phone)
            .subscribe(function (res) {
            //console.log(res)
            if (res.status) {
                var params = {
                    phone: phone
                };
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__opt_opt__["a" /* OptPage */], params);
            }
            else {
                _this.showError(res.text);
            }
        });
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-signup',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/signup/signup.html"*/'<ion-content padding id="page4" text-center>\n  <form id="sign-step-one-form1" (ngSubmit)="signStepOne()" id="ngForm2" #registerForm="ngForm">\n    <div class="spacer" style="height:10px;" id="spacer1"></div>\n    <h2 ion-text>Подтверждение номера</h2>\n    <p>Мы отправим SMS-сообщение, чтобы подтвердить ваш номер телефона.</p>\n\n    <ion-grid no-padding>\n      <ion-row>\n        <ion-col>\n          <ion-item no-padding>\n            <ion-label floating>\n              Код страны\n            </ion-label>\n            <ion-select okText="Выбрать" cancelText="Отмена" name="code"  [(ngModel)]="registerCredentials.code" required>\n              <ion-option value="+7">Россия +7</ion-option>\n            </ion-select>\n\n          </ion-item>\n        </ion-col>\n        <ion-col col-7>\n          <ion-item no-padding>\n            <ion-label floating>\n              Номер телефона\n            </ion-label>\n            <ion-input type="tel" name="tel" placeholder="" [brmasker]="{mask:\'0000000000\', len:10}" minlength="10" maxlength="10" value="" [(ngModel)]="registerCredentials.tel" required></ion-input>\n          </ion-item>\n        </ion-col>\n\n        <button class="button-next" mode="ios" id="sign-step-one-button1" form="ngForm2" type="submit" ion-button color="stable" block [disabled]="!registerForm.form.valid">\n          Далее\n        </button>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/signup/signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* MenuController */], __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(467);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 467:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(814);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_chats_chats__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_signup_signup__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_crop__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_emoji__ = __webpack_require__(429);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_push__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_camera__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_components_emoji_picker_emoji_picker_module__ = __webpack_require__(815);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angular2_virtual_scroll__ = __webpack_require__(817);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angular2_virtual_scroll___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_angular2_virtual_scroll__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_file__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_nointernet_nointernet__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_line_service__ = __webpack_require__(449);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_chat_chat__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pipes_relative_time_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_ng_socket_io__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_24_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_native_audio__ = __webpack_require__(819);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26_ng_lazyload_image__ = __webpack_require__(820);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26_ng_lazyload_image___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_26_ng_lazyload_image__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_network__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_app_minimize__ = __webpack_require__(825);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29_brmasker_ionic_3__ = __webpack_require__(826);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_welcome_welcome__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_opt_opt__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_push_service__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_nameandava_nameandava__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__providers_img_upload_service__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_google_maps__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_geolocation__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_native_geocoder__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__providers_geo_service__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__providers_masters__ = __webpack_require__(460);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__providers_profile__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42_ionic2_calendar__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__angular_common__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__angular_common_locales_ru__ = __webpack_require__(829);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__providers_calendar__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46_ionic2_rating__ = __webpack_require__(450);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pipes_format_time_module__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__providers_friend_friend__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__providers_api_interceptor__ = __webpack_require__(830);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__components_components_module__ = __webpack_require__(448);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__ionic_native_keyboard__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__ionic_native_in_app_browser__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__ionic_native_globalization__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__ionic_native_clipboard__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__providers_history_history__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__ionic_native_contacts__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__ionic_native_sqlite__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__ionic_native_photo_viewer__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__ionic_native_vibration__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__ionic_native_media__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__ionic_native_media_capture__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__ionic_native_toast__ = __webpack_require__(831);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__ionic_native_diagnostic__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__pipes_format_messages_module__ = __webpack_require__(832);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__ionic_native_local_notifications__ = __webpack_require__(428);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


































































Object(__WEBPACK_IMPORTED_MODULE_43__angular_common__["j" /* registerLocaleData */])(__WEBPACK_IMPORTED_MODULE_44__angular_common_locales_ru__["a" /* default */]);
var config = { url: 'http://ezmaven.com:3002', options: {} };
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_31__pages_opt_opt__["a" /* OptPage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_chats_chats__["a" /* ChatsPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_welcome_welcome__["a" /* WelcomePage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_chat_chat__["a" /* Chat */],
                __WEBPACK_IMPORTED_MODULE_5__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_nointernet_nointernet__["a" /* NointernetPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_nameandava_nameandava__["a" /* NameandavaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_42_ionic2_calendar__["a" /* NgCalendarModule */],
                __WEBPACK_IMPORTED_MODULE_46_ionic2_rating__["a" /* Ionic2RatingModule */],
                __WEBPACK_IMPORTED_MODULE_29_brmasker_ionic_3__["a" /* BrMaskerModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_17_angular2_virtual_scroll__["VirtualScrollModule"],
                __WEBPACK_IMPORTED_MODULE_16__pages_components_emoji_picker_emoji_picker_module__["a" /* EmojiPickerComponentModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    platforms: {
                        ios: {
                            backButtonText: 'Назад',
                        }
                    }
                }, {
                    links: [
                        { loadChildren: '../pages/about-app/about-app.module#AboutAppPageModule', name: 'AboutAppPage', segment: 'about-app', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/acceptorder/acceptorder.module#AcceptorderPageModule', name: 'AcceptorderPage', segment: 'acceptorder', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-to-group/add-to-group.module#AddToGroupPageModule', name: 'AddToGroupPage', segment: 'add-to-group', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/app-review/app-review.module#AppReviewPageModule', name: 'AppReviewPage', segment: 'app-review', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/archive/archive.module#ArchivePageModule', name: 'ArchivePage', segment: 'archive', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/calendar/calendar.module#CalendarPageModule', name: 'CalendarPage', segment: 'calendar', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/calendar-list/calendar-list.module#CalendarListPageModule', name: 'CalendarListPage', segment: 'calendar-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/call/call.module#CallPageModule', name: 'CallPage', segment: 'call', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/callstory/callstory.module#CallstoryPageModule', name: 'CallstoryPage', segment: 'callstory', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat-group/chat-group.module#ChatGroupPageModule', name: 'ChatGroupPage', segment: 'chat-group', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chats-group/chats-group.module#ChatsGroupPageModule', name: 'ChatsGroupPage', segment: 'chats-group', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/choice-city/choice-city.module#ChoiceCityPageModule', name: 'ChoiceCityPage', segment: 'choice-city', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/get-media-modal/get-media-modal.module#GetMediaModalPageModule', name: 'GetMediaModalPage', segment: 'get-media-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/giveadd/giveadd.module#GiveaddPageModule', name: 'GiveaddPage', segment: 'giveadd', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/group-view/group-view.module#GroupViewPageModule', name: 'GroupViewPage', segment: 'group-view', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history-action/history-action.module#HistoryActionPageModule', name: 'HistoryActionPage', segment: 'history-action', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invite-users/invite-users.module#InviteUsersPageModule', name: 'InviteUsersPage', segment: 'invite-users', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/masters-map/masters-map.module#MastersMapPageModule', name: 'MastersMapPage', segment: 'masters-map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/maintabs/maintabs.module#MaintabsPageModule', name: 'MaintabsPage', segment: 'maintabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nameandava/nameandava.module#NameandavaPageModule', name: 'NameandavaPage', segment: 'nameandava', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mykassa/mykassa.module#MykassaPageModule', name: 'MykassaPage', segment: 'mykassa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/new-chanel/new-chanel.module#NewChanelPageModule', name: 'NewChanelPage', segment: 'new-chanel', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/new-chat/new-chat.module#NewChatPageModule', name: 'NewChatPage', segment: 'new-chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/new-contact/new-contact.module#NewContactPageModule', name: 'NewContactPage', segment: 'new-contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/order/order.module#OrderPageModule', name: 'OrderPage', segment: 'order', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/opt/opt.module#OptPageModule', name: 'OptPage', segment: 'opt', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/people/people.module#PeoplePageModule', name: 'PeoplePage', segment: 'people', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/peoplecall/peoplecall.module#PeoplecallPageModule', name: 'PeoplecallPage', segment: 'peoplecall', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile-popover/profile-popover.module#ProfilePopoverPageModule', name: 'ProfilePopoverPage', segment: 'profile-popover', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nointernet/nointernet.module#NointernetPageModule', name: 'NointernetPage', segment: 'nointernet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/wait-loading/wait-loading.module#WaitLoadingPageModule', name: 'WaitLoadingPage', segment: 'wait-loading', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/wallet/wallet.module#WalletPageModule', name: 'WalletPage', segment: 'wallet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/welcome/welcome.module#WelcomePageModule', name: 'WelcomePage', segment: 'welcome', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/youcall/youcall.module#YoucallPageModule', name: 'YoucallPage', segment: 'youcall', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/secret/secret.module#SecretPageModule', name: 'SecretPage', segment: 'secret', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-group/create-group.module#CreateGroupPageModule', name: 'CreateGroupPage', segment: 'create-group', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/call-yes/call-yes.module#CallYesPageModule', name: 'CallYesPage', segment: 'call-yes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/callinfo/callinfo.module#CallinfoPageModule', name: 'CallinfoPage', segment: 'callinfo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile-view/profile-view.module#ProfileViewPageModule', name: 'ProfileViewPage', segment: 'profile-view', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile-options/profile-options.module#ProfileOptionsPageModule', name: 'ProfileOptionsPage', segment: 'profile-options', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/statement/statement.module#StatementPageModule', name: 'StatementPage', segment: 'statement', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */]),
                __WEBPACK_IMPORTED_MODULE_24_ng_socket_io__["SocketIoModule"].forRoot(config),
                __WEBPACK_IMPORTED_MODULE_26_ng_lazyload_image__["LazyLoadImageModule"],
                __WEBPACK_IMPORTED_MODULE_47__pipes_format_time_module__["a" /* FormatTimeModule */],
                __WEBPACK_IMPORTED_MODULE_50__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_23__pipes_relative_time_module__["a" /* RelativeTimeModule */],
                __WEBPACK_IMPORTED_MODULE_64__pipes_format_messages_module__["a" /* FormatMessagesModule */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                    name: '__mydbbloggerchu',
                    driverOrder: ['indexeddb', 'sqlite', 'websql']
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_22__pages_chat_chat__["a" /* Chat */],
                __WEBPACK_IMPORTED_MODULE_31__pages_opt_opt__["a" /* OptPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_welcome_welcome__["a" /* WelcomePage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_chats_chats__["a" /* ChatsPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_nointernet_nointernet__["a" /* NointernetPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_nameandava_nameandava__["a" /* NameandavaPage */],
            ],
            providers: [
                {
                    provide: __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["a" /* HTTP_INTERCEPTORS */],
                    useClass: __WEBPACK_IMPORTED_MODULE_49__providers_api_interceptor__["a" /* ApiInterceptor */],
                    multi: true,
                },
                __WEBPACK_IMPORTED_MODULE_51__ionic_native_keyboard__["a" /* Keyboard */],
                __WEBPACK_IMPORTED_MODULE_56__ionic_native_contacts__["c" /* Contacts */],
                __WEBPACK_IMPORTED_MODULE_57__ionic_native_sqlite__["a" /* SQLite */],
                __WEBPACK_IMPORTED_MODULE_62__ionic_native_toast__["a" /* Toast */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_54__ionic_native_clipboard__["a" /* Clipboard */],
                __WEBPACK_IMPORTED_MODULE_53__ionic_native_globalization__["a" /* Globalization */],
                __WEBPACK_IMPORTED_MODULE_52__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_app_minimize__["a" /* AppMinimize */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_crop__["a" /* Crop */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_native_audio__["a" /* NativeAudio */],
                __WEBPACK_IMPORTED_MODULE_13__providers_chat_service__["a" /* ChatService */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_6__providers_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_6__providers_auth_service__["b" /* User */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_11__providers_emoji__["a" /* EmojiProvider */],
                __WEBPACK_IMPORTED_MODULE_58__ionic_native_photo_viewer__["a" /* PhotoViewer */],
                __WEBPACK_IMPORTED_MODULE_59__ionic_native_vibration__["a" /* Vibration */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_60__ionic_native_media__["a" /* Media */],
                __WEBPACK_IMPORTED_MODULE_61__ionic_native_media_capture__["a" /* MediaCapture */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicErrorHandler */] },
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["LOCALE_ID"], useValue: 'ru-RU' },
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_21__providers_line_service__["a" /* LineService */],
                __WEBPACK_IMPORTED_MODULE_32__providers_push_service__["a" /* PushServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_34__providers_img_upload_service__["a" /* ImgUploadServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_35__ionic_native_google_maps__["b" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_35__ionic_native_google_maps__["a" /* Geocoder */],
                __WEBPACK_IMPORTED_MODULE_37__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_38__providers_geo_service__["a" /* GeoServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_39__providers_masters__["a" /* MastersProvider */],
                __WEBPACK_IMPORTED_MODULE_40__providers_profile__["a" /* ProfileProvider */],
                __WEBPACK_IMPORTED_MODULE_41__providers_config__["a" /* ConfigProvider */],
                __WEBPACK_IMPORTED_MODULE_45__providers_calendar__["a" /* CalendarProvider */],
                __WEBPACK_IMPORTED_MODULE_48__providers_friend_friend__["a" /* FriendProvider */],
                __WEBPACK_IMPORTED_MODULE_63__ionic_native_diagnostic__["a" /* Diagnostic */],
                __WEBPACK_IMPORTED_MODULE_55__providers_history_history__["a" /* HistoryProvider */],
                __WEBPACK_IMPORTED_MODULE_65__ionic_native_local_notifications__["a" /* LocalNotifications */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ChatMessage */
/* unused harmony export ChatRoom */
/* unused harmony export UserInfo */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_socket_io__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import {observable} from "rxjs/symbol/observable";

var ChatMessage = /** @class */ (function () {
    function ChatMessage() {
        this.audioPlaying = false;
        this.audioOnPause = false;
        this.typeMsg = 'simple';
    }
    return ChatMessage;
}());

var ChatRoom = /** @class */ (function () {
    function ChatRoom() {
    }
    return ChatRoom;
}());

var UserInfo = /** @class */ (function () {
    function UserInfo() {
    }
    return UserInfo;
}());

var ChatService = /** @class */ (function () {
    function ChatService(http, auth, socket, config) {
        this.http = http;
        this.auth = auth;
        this.socket = socket;
        this.config = config;
    }
    ChatService.prototype.getMsgList = function (roomid, userid, startFrom, limitTo) {
        if (limitTo === void 0) { limitTo = 30; }
        var msgListUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            roomid: roomid,
            userid: userid,
            limitFrom: startFrom,
            limitTo: limitTo,
            msgList: 1,
        });
        return this.http.post(msgListUrl, body);
    };
    ChatService.prototype.getMsgListIfExt = function (roomid, userid, startFrom, limitTo) {
        if (limitTo === void 0) { limitTo = 30; }
        var msgListUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            roomid: roomid,
            userid: userid,
            limitFrom: startFrom,
            limitTo: limitTo,
            msgList: 1,
        });
        return this.http.post(msgListUrl, body);
    };
    ChatService.prototype.getUserList = function () {
        var msgListUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            msgUserList: 1,
        });
        return this.http.post(msgListUrl, body);
    };
    ChatService.prototype.getUserListIfExt = function () {
        var msgListUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            msgUserListIfExt: 1,
        });
        return this.http.post(msgListUrl, body);
    };
    ChatService.prototype.getUserListInRoom = function (id) {
        var msgListUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            getUserListInRoom: id,
        });
        return this.http.post(msgListUrl, body);
    };
    ChatService.prototype.disconectSocket = function () {
        this.socket.disconnect();
    };
    ChatService.prototype.connectSocket = function (roomid) {
        this.socket.connect();
        this.socket.emit('set-nickname', this.auth.currentUser.name);
        this.socket.emit('connect-to-room', roomid);
    };
    ChatService.prototype.getNewMessages = function () {
        var _this = this;
        return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
            _this.socket.on('message', function (data) {
                observer.next(data);
            });
        });
    };
    ChatService.prototype.setReadMessages = function (roomid) {
        if (roomid === void 0) { roomid = null; }
        this.socket.emit('message-read', { userid: this.auth.currentUser.id });
        if (roomid != null) {
            var sUrl = this.config.apiURL + 'fn/messages.php';
            /*
                  const dataForm = new URLSearchParams();
                  dataForm.append('roomid', roomid);
                  dataForm.append('msgUpdView', '1');
                  const body = dataForm.toString();
            */
            var body = JSON.stringify({
                roomid: roomid,
                msgUpdView: 1,
            });
            //console.log('я прочитал сообщения, отправлен запрос')
            return this.http.post(sUrl, body);
        }
    };
    ChatService.prototype.getReadMessages = function () {
        var _this = this;
        return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
            _this.socket.on('message-check', function (data) {
                observer.next(data);
            });
        });
    };
    ChatService.prototype.sendMsg = function (msg, roomid) {
        var msgListUrl = this.config.apiURL + 'fn/messages.php';
        //msg.status='success';
        var socketMsg = new ChatMessage();
        socketMsg.audioDuration = msg.audioDuration;
        socketMsg.audioPlaying = msg.audioPlaying;
        socketMsg.messageId = msg.messageId;
        socketMsg.audio = msg.audio ? this.config.apiURL + 'rfiles/' + roomid + '/' + msg.audio : '';
        socketMsg.video = msg.video ? this.config.apiURL + 'rfiles/' + roomid + '/' + msg.video : '';
        socketMsg.image = msg.image ? this.config.apiURL + 'rfiles/' + roomid + '/' + msg.image : '';
        socketMsg.avatar = msg.avatar;
        socketMsg.date = msg.date;
        socketMsg.name = msg.name;
        socketMsg.audioOnPause = msg.audioOnPause;
        socketMsg.status = msg.status;
        socketMsg.text = msg.text;
        socketMsg.userid = msg.userid;
        socketMsg.typeMsg = msg.typeMsg;
        this.socket.emit('add-message', { message: socketMsg, roomid: roomid });
        /*    const dataForm = new URLSearchParams();
            dataForm.append('roomid', roomid);
            dataForm.append('text', msg.text);
            dataForm.append('msgSend', '1');
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            roomid: roomid,
            text: msg.text,
            image: msg.image,
            video: msg.video,
            audio: msg.audio,
            typeMsg: msg.typeMsg,
            msgSend: 1,
        });
        return this.http.post(msgListUrl, body);
        /*return new Promise(resolve => setTimeout(() => resolve(msg), Math.random() * 1000))
          .then(() => this.mockNewMsg(msg));*/
    };
    ChatService.prototype.getUserInfo = function () {
        var userInfo = this.auth.getUserInfo();
        return new Promise(function (resolve) { return resolve(userInfo); });
    };
    ChatService.prototype.checkCountMsg = function (roomid) {
        if (roomid === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/messages.php';
            /*
                  const dataForm = new URLSearchParams();
                  dataForm.append('roomid', roomid);
                  dataForm.append('msgCount', '1');
                  const body = dataForm.toString();
            */
            var body = JSON.stringify({
                roomid: roomid,
                msgCount: 1,
            });
            return this.http.post(sUrl, body);
        }
    };
    ChatService.prototype.checkUserStatus = function (roomid) {
        if (roomid === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/messages.php';
            var body = JSON.stringify({
                roomid: roomid,
                checkUserStatus: 1,
            });
            return this.http.post(sUrl, body);
        }
    };
    ChatService.prototype.createGroup = function (nameGroup, invateList) {
        if (nameGroup === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/messages.php';
            var body = JSON.stringify({
                nameGroup: nameGroup,
                invateList: JSON.stringify(invateList),
                createGroup: 1,
            });
            return this.http.post(sUrl, body);
        }
    };
    ChatService.prototype.checkUserStatusView = function (userid) {
        if (userid === null) {
            return;
        }
        else {
            var sUrl = this.config.apiURL + 'fn/messages.php';
            var body = JSON.stringify({
                userid: userid,
                checkUserStatusView: 1,
            });
            return this.http.post(sUrl, body);
        }
    };
    ChatService.prototype.unreadCountMsg = function () {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        /*
              const dataForm = new URLSearchParams();
              dataForm.append('msgUnread', '1');
              const body = dataForm.toString();
        */
        var body = JSON.stringify({
            msgUnread: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.updateCurrentRoom = function (roomid) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        /*    const dataForm = new URLSearchParams();
            dataForm.append('msgUpdCurRoom', '1');
            dataForm.append('roomid', roomid);
            const body = dataForm.toString();*/
        var body = JSON.stringify({
            roomid: roomid,
            msgUpdCurRoom: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.msgAddRoom = function (friendid) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            friendid: friendid,
            msgAddRoom: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.msgRemoveRoom = function (roomid) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            roomid: roomid,
            msgRemoveRoom: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.msgRemoveId = function (id) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            id: id,
            msgRemoveId: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.editMsgById = function (id, text) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            id: id,
            text: text,
            msgEditId: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.msgRemoveIdAll = function (id) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            id: id,
            msgRemoveIdAll: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.msgEditId = function (id, text) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            id: id,
            text: text,
            msgEditId: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.addToGroup = function (roomid, userid) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            roomid: roomid,
            userid: userid,
            addToGroup: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.adminChange = function (roomid, userid) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            roomid: roomid,
            userid: userid,
            adminChange: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.delFromRoom = function (roomid, userid) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            roomid: roomid,
            userid: userid,
            delFromRoom: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.leaveFromRoom = function (roomid, userid) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            roomid: roomid,
            userid: userid,
            leaveFromRoom: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.newNameGroup = function (id, newname) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            newNameGroup: newname,
            id: id,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.replaceName = function (name, phone, userid) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            replaceName: name,
            phone: phone,
            userid: userid,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.sendCallPush = function (roomid, userid, type) {
        console.log(type);
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            sendCallPush: 1,
            roomid: roomid,
            userid: userid,
            type: type,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.changeStatusCall = function (roomid) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            changeStatusCall: 1,
            roomid: roomid,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getCall = function (userid) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            getCall: 1,
            userid: userid,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getCallUs = function (userid, id) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            getCallUs: 1,
            userid: userid,
            id: id,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getGif = function () {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            getGif: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getStc = function () {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            getStick: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.dellCall = function (id) {
        var sUrl = this.config.apiURL + 'fn/messages.php';
        var body = JSON.stringify({
            dellCall: 1,
            id: id,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getBloggers = function () {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            getbloggers: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getGiv = function () {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            getGiv: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.reviewAll = function () {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            reviewAll: 1,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getOrders = function () {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            getorders: true,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getAccedptMyOrders = function () {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            getAccedptMyOrders: true,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getAccedptMyOrdersOn = function () {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            getAccedptMyOrdersOn: true,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getMyOrders = function (start) {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            getmyorders: true,
            start: start,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.getMyOrdersS = function () {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            getmyordersS: true,
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.noOrder = function (id, comment, name, userid) {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            noOrder: true,
            comment: comment,
            userid: userid,
            name: name,
            id: id
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.yesOrder = function (id, userid, name) {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            yesOrder: true,
            userid: userid,
            name: name,
            id: id
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.confirmOrder = function (id, comment, userid, name) {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            confirmOrder: true,
            userid: userid,
            comment: comment,
            name: name,
            id: id
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.noConfirmOrder = function (id, comment, userid, name) {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            noConfirmOrder: true,
            userid: userid,
            comment: comment,
            name: name,
            id: id
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.abortOrder = function (id, userid, name, text) {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            abortOrder: true,
            userid: userid,
            name: name,
            text: text,
            id: id
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.abortOrderYes = function (id, userid, name, text) {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            abortOrderYes: true,
            userid: userid,
            name: name,
            text: text,
            id: id
        });
        return this.http.post(sUrl, body);
    };
    ChatService.prototype.acceptOrder = function (id, userid, name, link) {
        var sUrl = this.config.apiURL + 'fn/bloggers.php';
        var body = JSON.stringify({
            acceptOrder: true,
            userid: userid,
            link: link,
            name: name,
            id: id
        });
        return this.http.post(sUrl, body);
    };
    ChatService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5_ng_socket_io__["Socket"],
            __WEBPACK_IMPORTED_MODULE_6__config__["a" /* ConfigProvider */]])
    ], ChatService);
    return ChatService;
}());

//# sourceMappingURL=chat-service.js.map

/***/ }),

/***/ 776:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 781:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RelativeTime; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_date_fns_distance_in_words_to_now__ = __webpack_require__(454);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_date_fns_distance_in_words_to_now___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_date_fns_distance_in_words_to_now__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_date_fns_format__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_date_fns_format___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_date_fns_format__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_date_fns_locale_ru_index_js__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_date_fns_locale_ru_index_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_date_fns_locale_ru_index_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var RelativeTime = /** @class */ (function () {
    function RelativeTime() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    RelativeTime.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        ////console.log(value);
        ////console.log(new Date(format(value, 'YYYY-MM-DDTHH:mm:ss')+'Z'));
        return __WEBPACK_IMPORTED_MODULE_1_date_fns_distance_in_words_to_now___default()(new Date(__WEBPACK_IMPORTED_MODULE_2_date_fns_format___default()(value, 'YYYY-MM-DDTHH:mm:ss') + 'Z'), { addSuffix: true, locale: __WEBPACK_IMPORTED_MODULE_3_date_fns_locale_ru_index_js__ });
    };
    RelativeTime = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'relativeTime',
        })
    ], RelativeTime);
    return RelativeTime;
}());

//# sourceMappingURL=relative-time.js.map

/***/ }),

/***/ 784:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ElasticTextArea; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ElasticTextArea = /** @class */ (function () {
    function ElasticTextArea(textInput, platform) {
        this.textInput = textInput;
        this.platform = platform;
        this.iosTextAreaMinHeight = 40;
        this.mdTextAreaMinHeight = 40;
        this.isIos = this.platform.is('ios');
    }
    ElasticTextArea.prototype.ngOnInit = function () {
        var _this = this;
        // Wait for TextInput _native property to initialize.
        setTimeout(function () {
            _this.textInput.ngControl.valueChanges.subscribe(function () {
                _this.resize();
            });
            _this.resize();
        });
    };
    ElasticTextArea.prototype.resize = function () {
        var height = this.getTextAreaHeight(this.textInput, this.isIos ? this.iosTextAreaMinHeight : this.mdTextAreaMinHeight);
        this.textInput._elementRef.nativeElement.style.height = height + 'px';
    };
    ElasticTextArea.prototype.getTextAreaHeight = function (textArea, minHeight) {
        // Get textarea styles.
        var body = document.querySelector('body'), textAreaElement = textArea._elementRef.nativeElement, style = window.getComputedStyle(textAreaElement, null), paddingHeight = parseInt(style.getPropertyValue('padding-top')) + parseInt(style.getPropertyValue('padding-bottom')), paddingWidth = parseInt(style.getPropertyValue('padding-left')) + parseInt(style.getPropertyValue('padding-right')), borderHeight = parseInt(style.getPropertyValue('border-top-width')) + parseInt(style.getPropertyValue('border-bottom-width')), width = parseInt(style.getPropertyValue('width')) - paddingWidth, lineHeight = style.getPropertyValue('line-height');
        // IE and Firefox do not support 'font' property, so we need to get it ourselves.
        var font = style.getPropertyValue('font-style') + ' ' +
            style.getPropertyValue('font-variant') + ' ' +
            style.getPropertyValue('font-weight') + ' ' +
            style.getPropertyValue('font-size') + ' ' +
            style.getPropertyValue('font-height') + ' ' +
            style.getPropertyValue('font-family');
        // Prepare a temporary textarea to determine the height for a real one.
        var newTextAreaElement = document.createElement('TEXTAREA'), newTextAreaElementId = '__newTextAreaElementId__';
        newTextAreaElement.setAttribute('rows', '1');
        newTextAreaElement.setAttribute('id', newTextAreaElementId);
        newTextAreaElement.style.font = font;
        newTextAreaElement.style.width = width + 'px';
        newTextAreaElement.style.border = '0';
        newTextAreaElement.style.overflow = 'hidden';
        newTextAreaElement.style.padding = '0';
        newTextAreaElement.style.outline = '0';
        newTextAreaElement.style.resize = 'none';
        newTextAreaElement.style.lineHeight = lineHeight;
        // To measure sizes we need to add the textarea to DOM.
        body.insertAdjacentHTML('beforeend', newTextAreaElement.outerHTML);
        newTextAreaElement = document.getElementById(newTextAreaElementId);
        newTextAreaElement.value = textArea.value;
        // Measure the height.
        newTextAreaElement.style.height = 'auto';
        newTextAreaElement.style.height = newTextAreaElement.scrollHeight + 'px';
        var height = parseInt(newTextAreaElement.style.height) + paddingHeight + borderHeight;
        if (height < minHeight) {
            height = minHeight;
        }
        // Remove the remporary textarea.
        body.removeChild(newTextAreaElement);
        return height;
    };
    ElasticTextArea = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[elasticTextArea]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* TextInput */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */]])
    ], ElasticTextArea);
    return ElasticTextArea;
}());

//# sourceMappingURL=elastic-textarea.js.map

/***/ }),

/***/ 788:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 795:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormatTime; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_date_fns_format__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_date_fns_format___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_date_fns_format__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_date_fns_locale_ru_index_js__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_date_fns_locale_ru_index_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_date_fns_locale_ru_index_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FormatTime = /** @class */ (function () {
    function FormatTime() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    FormatTime.prototype.transform = function (d, fmt) {
        ////console.log(new Date(d+'Z'))
        return __WEBPACK_IMPORTED_MODULE_1_date_fns_format___default()(__WEBPACK_IMPORTED_MODULE_1_date_fns_format___default()(d, 'YYYY-MM-DDTHH:mm:ss') + 'Z', fmt, { locale: __WEBPACK_IMPORTED_MODULE_2_date_fns_locale_ru_index_js__ });
    };
    FormatTime = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'formatTime',
        })
    ], FormatTime);
    return FormatTime;
}());

//# sourceMappingURL=format-time.js.map

/***/ }),

/***/ 814:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_globalization__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_chats_chats__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_nointernet_nointernet__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_network__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_welcome_welcome__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_push_service__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_chat_chat__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_push__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_geo_service__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_nameandava_nameandava__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_calendar__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_sqlite__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_contacts__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_local_notifications__ = __webpack_require__(428);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};























//@IonicPage()
var MyApp = /** @class */ (function () {
    // public userInfo: User=new User();
    function MyApp(platform, statusBar, splashScreen, auth, storage, pushService, chatService, calendarService, network, 
        //private appMinimize: AppMinimize,
        events, push, ngZone, sqlite, config, contacts, toastCtrl, globalization, geoService, localNotifications, menuCtrl) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.auth = auth;
        this.storage = storage;
        this.pushService = pushService;
        this.chatService = chatService;
        this.calendarService = calendarService;
        this.network = network;
        this.events = events;
        this.push = push;
        this.ngZone = ngZone;
        this.sqlite = sqlite;
        this.config = config;
        this.contacts = contacts;
        this.toastCtrl = toastCtrl;
        this.globalization = globalization;
        this.geoService = geoService;
        this.localNotifications = localNotifications;
        this.menuCtrl = menuCtrl;
        this.rootPage = 'WaitLoadingPage';
        this.unreadMsg = 0;
        this.clientEventCount = 0;
        this.masterEventCount = 0;
        this.incomingCall = {
            userid: null,
            roomid: null,
            type: null
        };
        this.config.getActiveTheme().subscribe(function (val) { return _this.selectedTheme = val; });
        //this.getNightMode();
        this.config.setActiveTheme('light-theme');
        ////console.log(auth.currentUser)
        //this.navCtrl.setRoot(HomePage);
        platform.ready().then(function () {
            if (_this.platform.is('cordova')) {
                _this.statusBar.styleDefault();
                _this.globalization.getDatePattern({ formatLength: 'full', selector: 'date and time' }).then(function (res) {
                    //console.log(res)
                    setTimeout(function () { _this.auth.saveTimezone(res.timezone, res.iana_timezone).subscribe(function (resp) { }); }, 5000);
                });
            }
            _this.sqlite.create({
                name: 'data.db',
                location: 'default'
            })
                .then(function (db) {
                db.executeSql('SELECT * FROM mycontacts ORDER BY id DESC, name ASC')
                    .then(function (res) {
                    if (res.rows.length > 0) {
                        _this.contacts.find(['*']).then(function (contacts_res) {
                            for (var _i = 0, contacts_res_1 = contacts_res; _i < contacts_res_1.length; _i++) {
                                var contact = contacts_res_1[_i];
                                var name_1 = contact['_objectInstance']['displayName'] ? contact['_objectInstance']['displayName'] : contact['_objectInstance']['name']['formatted'];
                                var _loop_1 = function (myPhone) {
                                    var phone = myPhone.value;
                                    if (phone.indexOf('8') === 0) {
                                        phone = '7' + phone.substr(1);
                                    }
                                    phone = phone.replace(/[^0-9]/g, "");
                                    var index = res.rows.findIndex(function (e) { return e.invatePhone === phone; });
                                    if (res.rows[index].name !== name_1) {
                                        _this.chatService.replaceName(name_1, phone, _this.auth.currentUser.id).subscribe(function (res) { console.log(res); });
                                        db.executeSql('UPDATE mycontacts SET name = ? WHERE id = ?', [contact['_objectInstance']['displayName'], res.rows[index].id])
                                            .then(function (insert_res) {
                                            console.log('insert_res', insert_res);
                                        });
                                    }
                                };
                                for (var _a = 0, _b = contact['_objectInstance']['phoneNumbers']; _a < _b.length; _a++) {
                                    var myPhone = _b[_a];
                                    _loop_1(myPhone);
                                }
                            }
                        });
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                });
            })
                .catch(function (e) {
                console.log(e);
            });
            //console.log('платформа',this.platform.platforms())
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            /*
                  this.storage.get('isLogged').then(logged => {
                    if (logged) {
                      this.rootPage = HomePage;
                    }
                  });*/
            _this.checkNoIntenet();
            _this.updateUnreadMsgCount = setInterval(function () {
                _this.getCountMsg();
            }, 5000);
            platform.resume.subscribe(function (e) {
                //console.log("resume called");
                _this.checkNoIntenet();
                _this.checkHash();
                _this.updateUnreadMsgCount = setInterval(function () {
                    _this.getCountMsg();
                }, 5000);
            });
            platform.pause.subscribe(function (e) {
                //console.log("pause called");
                _this.updStatusOffline();
                _this.disconnectSubscription.unsubscribe();
                clearInterval(_this.updateUnreadMsgCount);
            });
            _this.checkHash().then(function (res) {
                //console.log(res)
                setTimeout(function () {
                    splashScreen.hide();
                }, 100);
                if (_this.platform.is('android')) {
                    statusBar.overlaysWebView(false);
                }
            });
            _this.getCountMsg();
            /*      this.platform.registerBackButtonAction(()=>{
                    //console.log('event back button');
                    ////console.log(this.navCtrl.canGoBack());
                    if (this.navCtrl.canGoBack()){
                      this.navCtrl.pop();
                    } else {
                      this.appMinimize.minimize()
                    }
                  }, 1)*/
            _this.events.subscribe('myPushInit', function () {
                _this.myPushInit();
            });
            _this.events.subscribe('checkHash', function () {
                _this.checkHash();
            });
        });
    }
    MyApp.prototype.getIndexByType = function (array, type) {
        return array.findIndex(function (e) { return e.types.indexOf(type) >= 0; });
    };
    MyApp.prototype.getCurrentAddress = function () {
        var _this = this;
        this.geoService.getCurrentPosition().then(function (location) {
            _this.geoService.geocodeHTTP(location.coords.latitude, location.coords.longitude).subscribe(function (data) {
                //let data=resp.data
                if (data.status === 'OK') {
                    var address = data.results[0].address_components;
                    //console.log('address',address);
                    //console.log(this.getIndexByType(address,'locality'))
                    var city = address[_this.getIndexByType(address, 'locality')].long_name;
                    //console.log('город ', city);
                    _this.auth.currentUser.city = city;
                    _this.auth.setUserCity(city).subscribe(function (res) {
                        //console.log(res)
                    });
                }
            });
        });
    };
    MyApp.prototype.getCountMsg = function () {
        var _this = this;
        if (__WEBPACK_IMPORTED_MODULE_6__providers_auth_service__["a" /* AuthService */].getJTW() && __WEBPACK_IMPORTED_MODULE_6__providers_auth_service__["a" /* AuthService */].getJTW() !== '' && __WEBPACK_IMPORTED_MODULE_6__providers_auth_service__["a" /* AuthService */].getJTW() !== undefined) {
            ////console.log("JTW - ",this.auth.jwt)
            this.calendarService.getClientEventCount().subscribe(function (res) {
                _this.clientEventCount = res.count;
            });
            if (this.auth.currentUser.type == 2) {
                this.calendarService.getMasterEventCount().subscribe(function (res) {
                    _this.masterEventCount = res.count;
                });
            }
            this.chatService
                .unreadCountMsg()
                .subscribe(function (res) {
                ////console.log('Обновление непрочитанных сообщений в меню',res)
                _this.unreadMsg = res.count;
            });
        }
    };
    MyApp.prototype.setNightMode = function (val) {
        var _this = this;
        return this.storage.ready().then(function () {
            _this.storage.set('night', val);
        });
    };
    MyApp.prototype.updStatusOffline = function () {
        this.auth
            .updateStatus()
            .subscribe();
    };
    MyApp.prototype.checkHash = function () {
        var _this = this;
        return new Promise(function (resolve) {
            return _this.storage.ready().then(function () {
                _this.storage.get('hashid').then(function (hashid) {
                    console.log(hashid);
                    if (hashid && hashid != '' && hashid != undefined && hashid != 'undefined') {
                        __WEBPACK_IMPORTED_MODULE_6__providers_auth_service__["a" /* AuthService */].jwt2 = hashid;
                        //this.auth.jwt=hashid;
                        _this.auth
                            .checkHashID(hashid)
                            .subscribe(function (res) {
                            //console.log(res)
                            if (res.status) {
                                _this.auth.setUserInfo(res.user);
                                if (res.user.city == '' || res.user.city == null)
                                    _this.getCurrentAddress();
                                if (!_this.pushService.initPushStatus) {
                                    _this.myPushInit();
                                }
                                if (res.user.name == '' || res.user.name == null)
                                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_16__pages_nameandava_nameandava__["a" /* NameandavaPage */];
                                else
                                    _this.rootPage = 'MaintabsPage';
                                // //console.log(this.navCtrl.getActive().name)
                                //if (this.navCtrl.getActive().name == 'LoginPage') this.navCtrl.setRoot(HomePage);
                                ////console.log(this.navCtrl.getActive())
                            }
                            resolve(res);
                        });
                    }
                    else {
                        //this.navCtrl.setRoot(LoginPage)
                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_11__pages_welcome_welcome__["a" /* WelcomePage */];
                        resolve('hashid is empty');
                    }
                });
            });
        });
    };
    MyApp.prototype.IsJsonString = function (str) {
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    };
    MyApp.prototype.myPushInit = function () {
        var _this = this;
        if (!this.platform.is('cordova')) {
            console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
            return;
        }
        cordova.plugins.CordovaCall.on('answer', function () {
            _this.navCtrl.push('CallYesPage', { info: { userid: _this.incomingCall.userid, roomid: _this.incomingCall.roomid, type: _this.incomingCall.type, status: 'join' } });
        });
        var options = {
            android: {
                senderID: '525559497709',
                clearBadge: true,
                forceShow: false
            },
            ios: {
                alert: true,
                badge: false,
                sound: true,
                clearBadge: true,
            },
            windows: {}
        };
        this.pushObject = this.push.init(options);
        this.pushObject.on('registration').subscribe(function (data) {
            //console.log('device token -> ' + data.registrationId);
            //TODO - send device token to server
            _this.auth.saveToken(data.registrationId, _this.platform.platforms())
                .subscribe(function (res) {
                //console.log('Обновление токена устройства',res)
            });
        });
        this.pushObject.on('notification').subscribe(function (data) {
            console.log(data);
            var mydata = data.additionalData.mydata;
            if (mydata && _this.IsJsonString(mydata)) {
                mydata = JSON.parse(mydata);
            }
            if (_this.platform.is('ios')) {
                if (data.additionalData.foreground) {
                    if (data.additionalData.type == 'chat') {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_chat_chat__["a" /* Chat */], { roomid: data.additionalData.roomid });
                    }
                    else if (data.additionalData.type == 'call') {
                        _this.navCtrl.push('CallYesPage', { info: { userid: mydata.userid, myid: mydata.myid, roomid: data.additionalData.roomid, type: mydata.type, status: 'join' } });
                    }
                }
                else {
                    if (data.additionalData.type == 'chat') {
                        _this.localNotifications.schedule({
                            title: data.title,
                            text: data.message,
                            data: {
                                type: data.additionalData.type,
                                eventid: data.additionalData.eventid,
                                roomid: data.additionalData.roomid,
                                mydata: mydata,
                            }
                        });
                    }
                    else if (data.additionalData.type == 'call') {
                        cordova.plugins.CordovaCall.setIncludeInRecents(true);
                        cordova.plugins.CordovaCall.setAppName('BuzChat');
                        if (mydata.type === 'video') {
                            cordova.plugins.CordovaCall.setVideo(true);
                        }
                        _this.incomingCall.roomid = data.additionalData.roomid;
                        _this.incomingCall.userid = mydata.userid;
                        _this.incomingCall.type = mydata.type;
                        cordova.plugins.CordovaCall.receiveCall(data.title, function (res) { return console.log(res); }, function (err) { return console.error(err); });
                        // this.navCtrl.push('CallYesPage', {info : {userid: mydata.userid, roomid: data.additionalData.roomid, type: mydata.type, status: 'join'}})
                    }
                }
            }
            else {
                if (data.additionalData.foreground) {
                    if (data.additionalData.type == 'chat') {
                        //this.navCtrl.push(Chat, {roomid: data.additionalData.roomid});
                    }
                    else if (data.additionalData.type == 'call') {
                        _this.navCtrl.push('CallYesPage', { info: { userid: mydata.userid, myid: mydata.myid, roomid: data.additionalData.roomid, type: mydata.type, status: 'join' } });
                    }
                }
                else {
                    if (data.additionalData.type == 'chat') {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_chat_chat__["a" /* Chat */], { roomid: data.additionalData.roomid });
                    }
                    else if (data.additionalData.type == 'view_event') {
                        _this.navCtrl.push('EventViewPage', { eventID: data.additionalData.eventid });
                    }
                    else if (data.additionalData.type == 'view_profile') {
                        _this.navCtrl.push('ProfileViewPage', { id: data.additionalData.roomid });
                    }
                    else if (data.additionalData.type == 'view_line') {
                        _this.navCtrl.push('LineViewPage', { lineid: data.additionalData.roomid });
                    }
                    else if (data.additionalData.type == 'call') {
                        _this.navCtrl.push('CallYesPage', { info: { userid: mydata.userid, myid: mydata.myid, roomid: data.additionalData.roomid, type: mydata.type, status: 'join' } });
                    }
                }
            }
        });
        this.pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin' + error); });
        this.localNotifications.on('click').subscribe(function (notification) {
            console.log(notification);
            if (notification.data.type) {
                if (notification.data.type === 'chat') {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_chat_chat__["a" /* Chat */], { roomid: notification.data.roomid });
                }
            }
        });
    };
    MyApp.prototype.goToChats = function (params) {
        if (!params)
            params = {};
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_chats_chats__["a" /* ChatsPage */]);
    };
    MyApp.prototype.goToMyEvents = function () {
        this.navCtrl.setRoot('MyEventsPage');
    };
    MyApp.prototype.goToHistory = function () {
        this.navCtrl.setRoot('HistoryActionPage');
    };
    MyApp.prototype.goToPeople = function () {
        this.navCtrl.setRoot('PeoplePage');
    };
    MyApp.prototype.goToAboutApp = function () {
        this.navCtrl.setRoot('AboutAppPage');
    };
    MyApp.prototype.exitAcc = function () {
        var _this = this;
        this.auth.logout().subscribe(function (allowed) {
            if (allowed) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__pages_welcome_welcome__["a" /* WelcomePage */]);
            }
            else {
            }
        }, function (error) {
        });
    };
    MyApp.prototype.checkNoIntenet = function () {
        var _this = this;
        this.disconnectSubscription = this.network.onDisconnect().subscribe(function () {
            //console.log('network was disconnected :-(');
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_nointernet_nointernet__["a" /* NointernetPage */]);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Nav */])
    ], MyApp.prototype, "navCtrl", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/app/app.html"*/'<!--<ion-menu [content]="mainContent" [class]="selectedTheme">\n  <ion-content id="side-menu1">\n    <ion-list id="menu-list1">\n      <div no-padding class="menu-profile" menuClose="" (click)="goToHome()">\n        <div class="cover">\n          <img src="{{ auth.currentUser.background }}" alt="">\n        </div>\n        <div class="avatar" >\n          <img src="{{ auth.currentUser.avatar }}" alt="">\n        </div>\n        <div class="name" text-right><b>{{ auth.currentUser.name }}</b></div>\n      </div>\n      &lt;!&ndash;<ion-item-divider color="light"></ion-item-divider>&ndash;&gt;\n&lt;!&ndash;      <ion-item color="none" menuClose="" on-click="goToHome()" id="menu-list-item1">\n        <ion-icon name="home" item-left></ion-icon>\n        Профиль\n      </ion-item>&ndash;&gt;\n      <ion-item color="none" menuClose="" on-click="goToHome()" >\n        <ion-icon name="contact" item-left></ion-icon>\n        Мой профиль\n      </ion-item>\n      <ion-item color="none" menuClose="" on-click="goToPeople()" >\n        <ion-icon name="contacts" item-left></ion-icon>\n        Контакты\n      </ion-item>\n      <ion-item color="none" menuClose="" on-click="goToLine()" id="menu-list-item5">\n        <ion-icon name="images" item-left></ion-icon>\n        Публикации\n      </ion-item>\n      <ion-item color="none" menuClose="" on-click="goToChats()" id="menu-list-item2">\n        <ion-icon name="chatbubbles" item-left></ion-icon>\n        Сообщения\n        <ion-badge [color]="config.themeColor.primary" item-end *ngIf="unreadMsg>0">{{unreadMsg}}</ion-badge>\n      </ion-item>\n      <ion-item *ngIf="auth.currentUser.type==2" color="none" menuClose="" on-click="goToCalendar()" id="menu-list-item3">\n        <ion-icon name="calendar" item-left></ion-icon>\n        Мои сеансы\n        <ion-badge [color]="config.themeColor.primary" item-end *ngIf="masterEventCount>0">{{masterEventCount}}</ion-badge>\n      </ion-item>\n      <ion-item color="none" menuClose="" on-click="goToMyEvents()">\n        <ion-icon name="md-list" item-left></ion-icon>\n        Мои визиты\n        <ion-badge [color]="config.themeColor.primary" item-end *ngIf="clientEventCount>0">{{clientEventCount}}</ion-badge>\n      </ion-item>\n      <ion-item color="none" menuClose="" on-click="goToMasters()" id="menu-list-item6">\n        <ion-icon name="podium" item-left></ion-icon>\n        Топ мастеров\n      </ion-item>\n      <ion-item color="none" menuClose="" on-click="goToHistory()" >\n        <ion-icon name="megaphone" item-left></ion-icon>\n        Уведомления\n      </ion-item>\n\n      &lt;!&ndash;<ion-item-divider color="light"></ion-item-divider>&ndash;&gt;\n      <ion-item color="none" >\n        <ion-icon name="moon" item-left></ion-icon>\n        <ion-label>Ночной режим</ion-label>\n        <ion-toggle [color]="config.themeColor.primary" (ionChange)="toggleTheme()" [(ngModel)]="themeSelected" ></ion-toggle>\n      </ion-item>\n\n      <ion-item color="none" menuClose="" on-click="goToAboutApp()" >\n        <ion-icon name="information-circle" item-left></ion-icon>\n        О QRsor\n      </ion-item>\n    </ion-list>\n  </ion-content>\n</ion-menu>-->\n\n<ion-nav #mainContent [root]="rootPage" [class]="selectedTheme"></ion-nav>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_6__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_12__providers_push_service__["a" /* PushServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_17__providers_calendar__["a" /* CalendarProvider */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_19__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_18__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_20__ionic_native_contacts__["c" /* Contacts */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_globalization__["a" /* Globalization */],
            __WEBPACK_IMPORTED_MODULE_15__providers_geo_service__["a" /* GeoServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_21__ionic_native_local_notifications__["a" /* LocalNotifications */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* MenuController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 815:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmojiPickerComponentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__emoji_picker__ = __webpack_require__(816);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EmojiPickerComponentModule = /** @class */ (function () {
    function EmojiPickerComponentModule() {
    }
    EmojiPickerComponentModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__emoji_picker__["a" /* EmojiPickerComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__emoji_picker__["a" /* EmojiPickerComponent */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__emoji_picker__["a" /* EmojiPickerComponent */]
            ]
        })
    ], EmojiPickerComponentModule);
    return EmojiPickerComponentModule;
}());

//# sourceMappingURL=emoji-picker.module.js.map

/***/ }),

/***/ 816:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export EMOJI_PICKER_VALUE_ACCESSOR */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmojiPickerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_emoji__ = __webpack_require__(429);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EMOJI_PICKER_VALUE_ACCESSOR = {
    provide: __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* NG_VALUE_ACCESSOR */],
    useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["forwardRef"])(function () { return EmojiPickerComponent; }),
    multi: true
};
var EmojiPickerComponent = /** @class */ (function () {
    function EmojiPickerComponent(emojiProvider) {
        this.emojiArr = [];
        this.emojiArr = emojiProvider.getEmojis();
    }
    EmojiPickerComponent.prototype.writeValue = function (obj) {
        this._content = obj;
    };
    EmojiPickerComponent.prototype.registerOnChange = function (fn) {
        this._onChanged = fn;
        this.setValue(this._content);
    };
    EmojiPickerComponent.prototype.registerOnTouched = function (fn) {
        this._onTouched = fn;
    };
    EmojiPickerComponent.prototype.setValue = function (val) {
        this._content += val;
        if (this._content) {
            this._onChanged(this._content);
        }
    };
    EmojiPickerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'emoji-picker',
            providers: [EMOJI_PICKER_VALUE_ACCESSOR],template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/components/emoji-picker/emoji-picker.html"*/'<!-- Generated template for the EmojiPickerComponent component -->\n<div class="emoji-picker">\n  <div class="emoji-items">\n    <ion-slides pager>\n\n      <ion-slide *ngFor="let items of emojiArr">\n        <span class="emoji-item"\n              (click)="setValue(item)"\n              *ngFor="let item of items">\n          {{item}}\n        </span>\n      </ion-slide>\n\n    </ion-slides>\n  </div>\n</div>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/components/emoji-picker/emoji-picker.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_emoji__["a" /* EmojiProvider */]])
    ], EmojiPickerComponent);
    return EmojiPickerComponent;
}());

//# sourceMappingURL=emoji-picker.js.map

/***/ }),

/***/ 830:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_service__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ApiInterceptor = /** @class */ (function () {
    function ApiInterceptor() {
    }
    //constructor(private auth:AuthService){}
    ApiInterceptor.prototype.intercept = function (req, next) {
        ////console.log(req)
        req = req.clone({
            //withCredentials: true,
            setHeaders: {
                'Content-Type': 'application/json; charset=utf-8',
                'Authorization': "Bearer " + __WEBPACK_IMPORTED_MODULE_1__auth_service__["a" /* AuthService */].jwt2,
            },
        });
        ////console.log(req)
        return next.handle(req);
    };
    ApiInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], ApiInterceptor);
    return ApiInterceptor;
}());

//# sourceMappingURL=api.interceptor.js.map

/***/ }),

/***/ 832:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormatMessagesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__format_messages__ = __webpack_require__(833);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var FormatMessagesModule = /** @class */ (function () {
    function FormatMessagesModule() {
    }
    FormatMessagesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__format_messages__["a" /* FormatMessagesPipe */],
            ],
            imports: [],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__format_messages__["a" /* FormatMessagesPipe */]
            ]
        })
    ], FormatMessagesModule);
    return FormatMessagesModule;
}());

//# sourceMappingURL=format-messages.module.js.map

/***/ }),

/***/ 833:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormatMessagesPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_linkifyjs_string__ = __webpack_require__(834);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_linkifyjs_string___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_linkifyjs_string__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FormatMessagesPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var FormatMessagesPipe = /** @class */ (function () {
    /**
     * Takes a value and makes it lowercase.
     */
    function FormatMessagesPipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    FormatMessagesPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var newvalue;
        newvalue = value ? __WEBPACK_IMPORTED_MODULE_2_linkifyjs_string___default()(value, { target: '_system' }) : value;
        return newvalue;
    };
    FormatMessagesPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'formatMessages',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], FormatMessagesPipe);
    return FormatMessagesPipe;
}());

//# sourceMappingURL=format-messages.js.map

/***/ })

},[462]);
//# sourceMappingURL=main.js.map