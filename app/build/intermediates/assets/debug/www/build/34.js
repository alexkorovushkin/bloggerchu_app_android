webpackJsonp([34],{

/***/ 844:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppReviewPageModule", function() { return AppReviewPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_review__ = __webpack_require__(924);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(448);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AppReviewPageModule = /** @class */ (function () {
    function AppReviewPageModule() {
    }
    AppReviewPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_review__["a" /* AppReviewPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__app_review__["a" /* AppReviewPage */]),
            ],
        })
    ], AppReviewPageModule);
    return AppReviewPageModule;
}());

//# sourceMappingURL=app-review.module.js.map

/***/ }),

/***/ 924:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppReviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_profile__ = __webpack_require__(434);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppReviewPage = /** @class */ (function () {
    function AppReviewPage(navCtrl, navParams, config, profileService, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.config = config;
        this.profileService = profileService;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.review = {
            text: ''
        };
    }
    AppReviewPage.prototype.addReview = function () {
        var _this = this;
        this.profileService.addAppReview(this.review.text).subscribe(function (res) {
            if (res.status) {
                _this.review.text = '';
                var alert_1 = _this.alertCtrl.create({
                    title: 'Отзыв отправлен',
                    subTitle: 'Благодарим Вас, что нашли время поделиться впечатлением о приложении!',
                    buttons: ['Ок']
                });
                alert_1.present();
                _this.navCtrl.pop();
            }
            else {
                _this.presentToast('Не удалось отправить отзыв');
            }
        });
    };
    AppReviewPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            //console.log('Dismissed toast');
        });
        toast.present();
    };
    AppReviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-app-review',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/app-review/app-review.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Отзыв о приложении</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <form (ngSubmit)="addReview()" id="ngFormReview" #registerForm="ngForm">\n    <ion-item>\n      <ion-label floating>Отзыв</ion-label>\n      <ion-textarea required elasticTextArea name="text"  type="text" [(ngModel)]="review.text"></ion-textarea>\n    </ion-item>\n\n  </form>\n</ion-content>\n<ion-footer class="footer-button" padding no-border>\n  <ion-toolbar >\n    <button mode="ios"  form="ngFormReview" type="submit" ion-button color="stable" block [disabled]="!registerForm.form.valid">\n      Отправить\n    </button>\n  </ion-toolbar>\n\n</ion-footer>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/app-review/app-review.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_config__["a" /* ConfigProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_profile__["a" /* ProfileProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ToastController */]])
    ], AppReviewPage);
    return AppReviewPage;
}());

//# sourceMappingURL=app-review.js.map

/***/ })

});
//# sourceMappingURL=34.js.map