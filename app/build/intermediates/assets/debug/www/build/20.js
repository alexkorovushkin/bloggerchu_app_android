webpackJsonp([20],{

/***/ 1036:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InviteUsersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_profile__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_contacts__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_friend_friend__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_sqlite__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__chat_chat__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the InviteUsersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InviteUsersPage = /** @class */ (function () {
    function InviteUsersPage(navCtrl, navParams, profileService, modalCtrl, auth, chatService, config, contacts, browser, friendService, sqlite, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.profileService = profileService;
        this.modalCtrl = modalCtrl;
        this.auth = auth;
        this.chatService = chatService;
        this.config = config;
        this.contacts = contacts;
        this.browser = browser;
        this.friendService = friendService;
        this.sqlite = sqlite;
        this.platform = platform;
        this.searchText = '';
        this.waitLoading = true;
        this.getLocalContacts();
    }
    InviteUsersPage.prototype.getPeople = function (limitFrom, limitTo, options) {
        var _this = this;
        if (limitFrom === void 0) { limitFrom = 0; }
        if (limitTo === void 0) { limitTo = 10; }
        if (options === void 0) { options = null; }
        return new Promise(function (resolve) {
            _this.profileService.getPeople(limitFrom, limitTo, options).subscribe(function (res) {
                if (!_this.peopleList)
                    _this.peopleList = [];
                //console.log(res)
                for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                    var people = res_1[_i];
                    _this.peopleList.push(people);
                }
                _this.waitLoading = false;
                resolve();
            });
        });
    };
    InviteUsersPage.prototype.onSearch = function (ev) {
        var _this = this;
        this.peopleList = [];
        //this.waitLoading=true;
        this.searchText = ev.target.value;
        if (this.searchText && this.searchText.trim() != '') {
            if (this.myPeopleList && this.myPeopleList.length > 0) {
                this.myPeopleList = this.myPeopleList.filter(function (item) {
                    console.log(item.name.toLowerCase().indexOf(_this.searchText.toLowerCase()));
                    return (item.name.toLowerCase().indexOf(_this.searchText.toLowerCase()) > -1 || item.invatePhone.indexOf(_this.searchText.toLowerCase()) > -1);
                });
            }
            this.getPeople(0, 10, { search: this.searchText.trim() });
        }
        else {
            this.waitLoading = true;
            this.peopleList = [];
            this.getLocalContacts();
        }
    };
    InviteUsersPage.prototype.doInfinite = function () {
        return this.getPeople(this.peopleList.length, 10, { search: this.searchText.trim() });
    };
    InviteUsersPage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    InviteUsersPage.prototype.inviteFriends = function () {
        this.navCtrl.push('InviteUsersPage');
    };
    InviteUsersPage.prototype.addContPage = function () {
        this.navCtrl.push('NewContactPage');
    };
    InviteUsersPage.prototype.goToChat = function (id) {
        var _this = this;
        this.chatService.msgAddRoom(id).subscribe(function (res) {
            if (res.status === true) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__chat_chat__["a" /* Chat */], { roomid: res.roomid });
            }
        });
    };
    InviteUsersPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            //console.log('Begin async operation', refresher);
            _this.sqlite.create({
                name: 'data.db',
                location: 'default'
            })
                .then(function (db) {
                db.executeSql('DELETE FROM mycontacts')
                    .then(function (delete_res) {
                    console.log('delete_res', delete_res);
                    _this.getLocalContacts();
                });
            });
            refresher.complete();
            resolve();
        });
    };
    InviteUsersPage.prototype.newcall = function (userid, type) {
        this.navCtrl.push('CallYesPage', { info: { userid: userid, myid: this.auth.currentUser.id, roomid: userid + '-' + this.auth.currentUser.id + '-' + new Date().getTime(), type: type, status: 'open' } });
    };
    InviteUsersPage.prototype.getLocalContacts = function () {
        var _this = this;
        this.waitLoading = true;
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        })
            .then(function (db) {
            //id INTEGER PRIMARY KEY,
            db.executeSql('CREATE TABLE IF NOT EXISTS mycontacts(id INT, invatePhone TEXT, name TEXT, avatar TEXT, profession TEXT, type INT )')
                .then(function () { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('SELECT * FROM mycontacts ORDER BY id DESC, name ASC')
                .then(function (res) {
                console.log('sqlite_contacts', res);
                _this.myPeopleList = [];
                if (res.rows.length == 0) {
                    _this.contacts.find(['*']).then(function (contacts_res) {
                        _this.myContacts = contacts_res;
                        console.log('contacts_res', _this.myContacts);
                        _this.friendService.saveMyContacts(_this.myContacts).subscribe(function (save_res) {
                            console.log('save_res', save_res);
                            if (save_res.status) {
                                _this.friendService.getMyContacts().subscribe(function (get_contact_res) {
                                    _this.myPeopleList = get_contact_res;
                                    _this.waitLoading = false;
                                    for (var _i = 0, _a = _this.myPeopleList; _i < _a.length; _i++) {
                                        var contact = _a[_i];
                                        db.executeSql('INSERT INTO mycontacts VALUES(?,?,?,?,?,?)', [contact.id, contact.invatePhone, contact.name, contact.avatar, contact.profession, contact.type])
                                            .then(function (insert_res) {
                                            console.log('insert_res', insert_res);
                                        });
                                    }
                                });
                            }
                        });
                    });
                }
                else {
                    _this.waitLoading = false;
                    for (var i = 0; i < res.rows.length; i++) {
                        console.log(res.rows.item(i));
                        _this.myPeopleList.push(res.rows.item(i));
                    }
                }
            })
                .catch(function (e) {
                console.log(e);
                _this.waitLoading = false;
            });
        })
            .catch(function (e) {
            console.log(e);
            _this.waitLoading = false;
        });
    };
    InviteUsersPage.prototype.invateSend = function (phone) {
        var _this = this;
        setTimeout(function () {
            if (_this.platform.is('ios'))
                _this.browser.create("sms:" + phone + ";body=" + encodeURIComponent('Привет, я использую bloggerchu. Присоединяйся!'), '_system');
            else
                _this.browser.create("sms:" + phone + "?body=" + encodeURIComponent('Привет, я использую bloggerchu. Присоединяйся!'), '_system');
        }, 100);
    };
    InviteUsersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-invite-users',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/invite-users/invite-users.html"*/'<!--\n  Generated template for the InviteUsersPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Пригласить</ion-title>\n    <ion-buttons end>\n      Выбрать всех\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-searchbar\n    animated="true"\n    placeholder="Поиск"\n    autocomplete="on"\n    autocorrect="on"\n    (ionInput)="onSearch($event)">\n  </ion-searchbar>\n  <div ion-fixed class="wait-loading" *ngIf="waitLoading">\n    <ion-spinner name="crescent"></ion-spinner>\n  </div>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingIcon="arrow-dropdown"\n      pullingText="Обновить контакты"\n      refreshingSpinner="crescent">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-list no-padding *ngIf="myPeopleList && myPeopleList.length>0">\n    <ion-list-header>\n      Мои контакты\n    </ion-list-header>\n    <ng-container *ngFor="let row of myPeopleList;" >\n      <ion-card *ngIf="row.id==0">\n        <ion-item >\n          <h2 >{{row.name}}</h2>\n          <button [color]="config.themeColor.primary" ion-button (click)="invateSend(row.invatePhone)" item-end mode="ios">\n            Пригласить\n          </button>\n        </ion-item>\n      </ion-card>\n    </ng-container>\n  </ion-list>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/invite-users/invite-users.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_profile__["a" /* ProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_6__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_contacts__["c" /* Contacts */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_8__providers_friend_friend__["a" /* FriendProvider */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */]])
    ], InviteUsersPage);
    return InviteUsersPage;
}());

//# sourceMappingURL=invite-users.js.map

/***/ }),

/***/ 857:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InviteUsersPageModule", function() { return InviteUsersPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__invite_users__ = __webpack_require__(1036);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InviteUsersPageModule = /** @class */ (function () {
    function InviteUsersPageModule() {
    }
    InviteUsersPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__invite_users__["a" /* InviteUsersPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__invite_users__["a" /* InviteUsersPage */]),
            ],
        })
    ], InviteUsersPageModule);
    return InviteUsersPageModule;
}());

//# sourceMappingURL=invite-users.module.js.map

/***/ })

});
//# sourceMappingURL=20.js.map