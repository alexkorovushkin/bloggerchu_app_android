webpackJsonp([5],{

/***/ 1057:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatementPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__ = __webpack_require__(185);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the StatementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var StatementPage = /** @class */ (function () {
    function StatementPage(navCtrl, chatService, alertCtrl, loadingCtrl, auth, browser, navParams) {
        this.navCtrl = navCtrl;
        this.chatService = chatService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.auth = auth;
        this.browser = browser;
        this.navParams = navParams;
        this.orders = [];
        this.myorders = [];
        this.tabs = "orders";
    }
    StatementPage.prototype.updateOrders = function () {
        var _this = this;
        this.chatService.getOrders().subscribe(function (res) {
            if (res.status) {
                _this.orders = res.order;
            }
        });
    };
    StatementPage.prototype.ionViewWillEnter = function () {
        this.updateOrders();
    };
    StatementPage.prototype.goToLink = function (link) {
        this.browser.create(link, '_system');
    };
    StatementPage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    StatementPage.prototype.goAccept = function () {
        this.navCtrl.push('OrderPage');
    };
    StatementPage.prototype.goAcceptOn = function () {
        this.navCtrl.push('AcceptorderPage');
    };
    StatementPage.prototype.findIndexByID = function (id) {
        return this.orders.findIndex(function (e) { return e.id === id; });
    };
    StatementPage.prototype.findIndexByIDMy = function (id) {
        return this.myorders.findIndex(function (e) { return e.id === id; });
    };
    StatementPage.prototype.showLoading = function (text) {
        this.loading = this.loadingCtrl.create({
            content: text,
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    StatementPage.prototype.noOrder = function (id, userid) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Отказ',
            message: 'Опишите почему вы решили отказать в рекламе?',
            inputs: [
                {
                    name: 'noorder',
                    placeholder: 'Причина отказа'
                }
            ],
            buttons: [
                {
                    text: 'Я передумал',
                    role: 'cancel',
                },
                {
                    text: 'Отказать',
                    handler: function (data) {
                        _this.showLoading('Отказываемся');
                        _this.chatService.noOrder(id, data.noorder, _this.auth.currentUser.name, userid).subscribe(function (res) {
                            if (res.status) {
                                var index = _this.findIndexByID(id);
                                if (index >= 0) {
                                    _this.orders.splice(index, 1);
                                }
                                _this.loading.dismiss();
                            }
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    StatementPage.prototype.yesOrder = function (id, userid) {
        var _this = this;
        this.showLoading('Оформляем');
        this.chatService.yesOrder(id, userid, this.auth.currentUser.name).subscribe(function (res) {
            if (res.status) {
                var index = _this.findIndexByID(id);
                if (index >= 0) {
                    _this.orders.splice(index, 1);
                }
                _this.loading.dismiss();
            }
        });
    };
    StatementPage.prototype.confirmOrder = function (id, userid, name) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Отзыв',
            message: 'Опишите насколько приятно было работать с ' + name + '?',
            inputs: [
                {
                    name: 'okey',
                    placeholder: 'Опишите'
                }
            ],
            buttons: [
                {
                    text: 'Нехочу',
                    handler: function (data) {
                        _this.showLoading('Подтверждаем');
                        _this.chatService.confirmOrder(id, '', userid, _this.auth.currentUser.name).subscribe(function (res) {
                            if (res.status) {
                                var index = _this.findIndexByIDMy(id);
                                if (index >= 0) {
                                    _this.myorders.splice(index, 1);
                                    _this.loading.dismiss();
                                }
                            }
                        });
                    }
                },
                {
                    text: 'Подтвердить',
                    handler: function (data) {
                        _this.showLoading('Подтверждаем');
                        _this.chatService.confirmOrder(id, data.okey, userid, _this.auth.currentUser.name).subscribe(function (res) {
                            if (res.status) {
                                var index = _this.findIndexByIDMy(id);
                                if (index >= 0) {
                                    _this.myorders.splice(index, 1);
                                    _this.loading.dismiss();
                                }
                            }
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    StatementPage.prototype.noConfirmOrder = function (id, userid, name) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Отзыв',
            message: 'Опишите чем не соответствует выполненная работа ' + name + '?',
            inputs: [
                {
                    name: 'okey',
                    placeholder: 'Опишите'
                }
            ],
            buttons: [
                {
                    text: 'Нехочу',
                    handler: function (data) {
                        _this.showLoading('Подтверждаем');
                        _this.chatService.noConfirmOrder(id, '', userid, _this.auth.currentUser.name).subscribe(function (res) {
                            if (res.status) {
                                var index = _this.findIndexByIDMy(id);
                                if (index >= 0) {
                                    _this.myorders.splice(index, 1);
                                    _this.loading.dismiss();
                                }
                            }
                        });
                    }
                },
                {
                    text: 'Подтвердить',
                    handler: function (data) {
                        _this.showLoading('Подтверждаем');
                        _this.chatService.noConfirmOrder(id, data.okey, userid, _this.auth.currentUser.name).subscribe(function (res) {
                            if (res.status) {
                                var index = _this.findIndexByIDMy(id);
                                if (index >= 0) {
                                    _this.myorders.splice(index, 1);
                                    _this.loading.dismiss();
                                }
                            }
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    StatementPage.prototype.abortOrder = function (id, userid) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Отмена',
            message: 'Опишите почему вы решили отменить заказ?',
            inputs: [
                {
                    name: 'noorder',
                    placeholder: 'Причина отмены'
                }
            ],
            buttons: [
                {
                    text: 'Я передумал',
                    role: 'cancel',
                },
                {
                    text: 'Отменить',
                    handler: function (data) {
                        _this.showLoading('Отменяем');
                        _this.chatService.abortOrder(id, userid, _this.auth.currentUser.name, data.noorder).subscribe(function (res) {
                            if (res.status) {
                                var index = _this.findIndexByIDMy(id);
                                if (index >= 0) {
                                    _this.myorders.splice(index, 1);
                                }
                                _this.loading.dismiss();
                            }
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    StatementPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-statement',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/statement/statement.html"*/'<!--\n  Generated template for the StatementPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n  <ion-buttons left>\n    <button ion-button icon-only (click)="goAcceptOn()">\n      <ion-icon name="checkmark-circle-outline"></ion-icon>\n    </button>\n  </ion-buttons>\n    <ion-segment class="segmentMy" [(ngModel)]="tabs" [color]="primary">\n      <ion-segment-button value="orders" onselect>\n        Заявки\n      </ion-segment-button>\n      <ion-segment-button value="myorders">\n        Заказы\n      </ion-segment-button>\n    </ion-segment>\n  <ion-buttons end>\n    <button ion-button icon-only (click)="goAccept()">\n      <ion-icon name="ios-clock-outline"></ion-icon>\n    </button>\n  </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <div [ngSwitch]="tabs">\n  <div *ngSwitchCase="\'orders\'">\n    <ion-card  *ngIf="!orders ">\n      У вас нет заявок на рекламу\n    </ion-card>\n    <div *ngIf="orders "  >\n      <ion-card *ngFor="let order of orders">\n        <img src="{{order.avatar}}" (click)="goToProfile(order.userid)"/>\n        <ion-card-content>\n          <ion-card-title>\n            {{order.name}} хочет заказать {{order.siting}}\n          </ion-card-title>\n          <ion-label>\n            О пользователе {{order.name}}\n          </ion-label>\n          <p>\n            {{order.aboutman}}\n          </p>\n          <ion-label>\n            У пользователя {{order.name}} есть требования\n          </ion-label>\n          <p>\n            {{order.requirements}}\n          </p>\n          <ion-label>\n            Хотел бы заказать {{order.siting}}\n          </ion-label>\n          <p class="aboutOrder">\n            На {{order.dateStart}} в {{order.timeStart}}<br>\n            на условиях {{order.conditions}}\n          </p>\n          <button class="noOrder" (click)="noOrder(order.id,order.userid)">Отказать</button>\n          <button *ngIf="order.accept && order.accept===\'0\'" class="yesOrder" (click)="yesOrder(order.id,order.userid)">Принять</button>\n        </ion-card-content>\n      </ion-card>\n    </div>\n  </div>\n  <div *ngSwitchCase="\'myorders\'">\n      <ion-card *ngFor="let myorder of myorders">\n        <img class="imageMyOrder" src="{{myorder.avatar}}"/>\n        <ion-card-content>\n          <ion-card-header>Заказ #{{myorder.id}}</ion-card-header>\n          <p class="aboutMyOrder">\n            Вы заказали {{myorder.siting}} у {{myorder.name}} с требованиями {{myorder.requirements}} На {{myorder.dateStart}} в {{myorder.timeStart}} на условиях {{myorder.conditions}}<br>\n            <span *ngIf="myorder.accept===\'1\'"><b>В работе</b></span>\n            <button *ngIf="myorder.accept===\'3\'" tappable (click)="goToLink(myorder.link)">{{myorder.link}}</button>\n          </p>\n        <button *ngIf="myorder.accept===\'0\'" class="noMyOrder" (click)="abortOrder(myorder.id,myorder.to_userid)">Отменить</button>\n        <button *ngIf="myorder.accept===\'3\'" class="yesOrder" (click)="confirmOrder(myorder.id,myorder.to_userid,myorder.name)">Подтвердить</button>\n        <button *ngIf="myorder.accept===\'3\'" class="noOrder" (click)="noConfirmOrder(myorder.id,myorder.to_userid,myorder.name)">Не соответствует</button>\n        </ion-card-content>\n      </ion-card>\n  </div>\n\n\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/statement/statement.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], StatementPage);
    return StatementPage;
}());

//# sourceMappingURL=statement.js.map

/***/ }),

/***/ 883:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatementPageModule", function() { return StatementPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__statement__ = __webpack_require__(1057);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var StatementPageModule = /** @class */ (function () {
    function StatementPageModule() {
    }
    StatementPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__statement__["a" /* StatementPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__statement__["a" /* StatementPage */]),
            ],
        })
    ], StatementPageModule);
    return StatementPageModule;
}());

//# sourceMappingURL=statement.module.js.map

/***/ })

});
//# sourceMappingURL=5.js.map