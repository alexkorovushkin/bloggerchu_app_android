webpackJsonp([7],{

/***/ 1047:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = /** @class */ (function () {
    function SearchPage(navCtrl, chatService, navParams) {
        this.navCtrl = navCtrl;
        this.chatService = chatService;
        this.navParams = navParams;
        this.tabs = "bloggers";
        this.bloggerslist = [];
        this.givlist = [];
        this.review = [];
    }
    SearchPage.prototype.ionViewWillEnter = function () {
        this.update();
    };
    SearchPage.prototype.update = function () {
        var _this = this;
        this.chatService.getBloggers().subscribe(function (res) {
            if (res.status) {
                _this.bloggerslist = res.list;
            }
        });
        this.chatService.getGiv().subscribe(function (res) {
            if (res.status) {
                console.log(res);
                _this.givlist = res.list;
            }
        });
        this.chatService.reviewAll().subscribe(function (res) {
            if (res.status) {
                _this.review = res.list;
            }
        });
    };
    SearchPage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    SearchPage.prototype.addGive = function () {
        this.navCtrl.push('GiveaddPage');
    };
    SearchPage.prototype.onInput = function (text) {
        console.log(text);
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/search/search.html"*/'<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-toolbar>\n\n    <ion-segment [(ngModel)]="tabs" [color]="primary">\n      <ion-segment-button value="bloggers" onselect>\n        Блоггер\n      </ion-segment-button>\n      <ion-segment-button value="giv">\n        Giveaway\n      </ion-segment-button>\n      <ion-segment-button value="review">\n        Отзывы\n      </ion-segment-button>\n    </ion-segment>\n  </ion-toolbar>\n  <ion-searchbar\n          placeholder="Поиск..."\n          [(ngModel)]="searchinput"\n          (ionInput)="onInput($event)">\n  </ion-searchbar>\n</ion-header>\n\n\n<ion-content>\n\n\n\n  <div [ngSwitch]="tabs">\n    <div *ngSwitchCase="\'bloggers\'">\n      <ion-list >\n        <ion-item *ngFor="let blog of bloggerslist">\n          <ion-avatar item-start  (click)="goToProfile(blog.id)">\n            <img src="{{blog.avatar}}">\n          </ion-avatar>\n          <h2  (click)="goToProfile(blog.id)">{{blog.name}}</h2>\n          <h3 *ngIf="blog.city !== \'\'"  (click)="goToProfile(blog.id)">{{blog.city}}</h3>\n          <h3 *ngIf="blog.city === null"  (click)="goToProfile(blog.id)">Не указал город</h3>\n          <p *ngIf="blog.about !== \'\'"  (click)="goToProfile(blog.id)">{{blog.about}}</p>\n          <p *ngIf="blog.about === null"  (click)="goToProfile(blog.id)">Ничего о себе не написал</p>\n        </ion-item>\n      </ion-list>\n    </div>\n    <div *ngSwitchCase="\'giv\'">\n      <ion-buttons>\n        <button class="addGiv" ion-button (click)="addGive()">Добавить Giv</button>\n      </ion-buttons>\n\n      <ion-card *ngFor="let giv of givlist">\n        <ion-item>\n          <ion-avatar item-start (click)="goToProfile(giv.id)">\n            <img src="{{giv.avatar}}">\n          </ion-avatar>\n          <h2>{{giv.nameuser}}</h2>\n          <p>{{giv.date}}</p>\n        </ion-item>\n        <ion-card-content>\n          <p><b>{{giv.name}}</b></p>\n          <p>{{giv.about}}</p>\n          <p><b>Призы: </b>{{giv.gifts}}</p>\n          <p><b>Гарант подписчиков: </b>{{giv.garant_sub}}</p>\n          <p><b>Дата начала: </b>{{giv.start}}</p>\n          <p><b>Длительность: </b>{{giv.duration}} дней</p>\n          <p><b>Контакты: </b>{{giv.contacts}}</p>\n\n          <b>Участники</b>\n          <div class="listGroupUsers" *ngIf="giv.users">\n            <ng-container *ngFor="let users of giv.users" >\n            <span class="manPlus">\n               <span class="nameGroup">@{{users}} </span>\n            </span>\n            </ng-container>\n          </div>\n\n          <b>Спонсоры</b>\n          <div class="listGroupUsers" *ngIf="giv.sponsors">\n            <ng-container *ngFor="let sponsors of giv.sponsors" >\n            <span class="manPlus">\n               <span class="nameGroup">@{{sponsors}} </span>\n            </span>\n            </ng-container>\n          </div>\n\n        </ion-card-content>\n      </ion-card>\n\n\n    </div>\n    <div *ngSwitchCase="\'review\'">\n      <ion-card *ngFor="let rev of review">\n        <ion-item>\n          <ion-avatar item-start (click)="goToProfile(rev.userid)">\n            <img src="{{rev.avatar}}">\n          </ion-avatar>\n          <h2>{{rev.nameuser}}</h2>\n          <p>{{rev.name}}</p>\n        </ion-item>\n        <ion-card-content>\n          <p>{{rev.date}}</p>\n          <p><b>Отзыв: </b>{{rev.comment}}</p>\n          <p *ngIf="rev.accept === \'4\'">Выполнено</p>\n          <p *ngIf="rev.accept === \'3\'">Не выполнено</p>\n          <p *ngIf="rev.accept === \'2\'">Не принято</p>\n        </ion-card-content>\n      </ion-card>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/search/search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 870:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search__ = __webpack_require__(1047);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SearchPageModule = /** @class */ (function () {
    function SearchPageModule() {
    }
    SearchPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */]),
            ],
        })
    ], SearchPageModule);
    return SearchPageModule;
}());

//# sourceMappingURL=search.module.js.map

/***/ })

});
//# sourceMappingURL=7.js.map