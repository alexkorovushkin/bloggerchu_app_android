webpackJsonp([31],{

/***/ 1053:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CallYesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_profile__ = __webpack_require__(434);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the CallYesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CallYesPage = /** @class */ (function () {
    function CallYesPage(navCtrl, navParams, platform, ref, profile, auth, chatService, diagnostic) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.ref = ref;
        this.profile = profile;
        this.auth = auth;
        this.chatService = chatService;
        this.diagnostic = diagnostic;
        this.streemAudio = [];
        this.callInfo = {
            userid: null,
            roomid: null,
            type: 'video',
            status: 'open'
        };
        this.camerafront = true;
        if (this.platform.is('cordova')) {
            this.diagnostic.getCameraAuthorizationStatus().then(function (status) {
                if (status != _this.diagnostic.permissionStatus.GRANTED) {
                    _this.diagnostic.requestCameraAuthorization();
                }
            });
            this.diagnostic.getMicrophoneAuthorizationStatus().then(function (status) {
                if (status != _this.diagnostic.permissionStatus.GRANTED) {
                    _this.diagnostic.requestMicrophoneAuthorization();
                }
            });
        }
        this.callInfo = this.navParams.get('info');
        console.log(this.callInfo);
        if (this.callInfo.status === 'open') {
            this.profile.getProfileInfo(this.callInfo.userid).subscribe(function (res) {
                if (res.status) {
                    console.log('infoUserCall', res);
                    _this.imageusercall = res.data.avatar;
                    _this.nameusercall = res.data.profilename;
                    _this.phoneusercall = res.data.phone;
                }
            });
            if (this.callInfo.type === 'video') {
                this.openroom();
            }
            else if (this.callInfo.type === 'audio') {
                this.openaudioroom();
            }
        }
        else if (this.callInfo.status === 'join') {
            this.profile.getProfileInfo(this.callInfo.userid).subscribe(function (res) {
                if (res.status) {
                    console.log('infoUserCall', res);
                    _this.imageusercall = res.data.avatar;
                    _this.nameusercall = res.data.profilename;
                    _this.phoneusercall = res.data.phone;
                }
            });
            if (this.callInfo.type === 'video') {
                this.joinroom();
            }
            else if (this.callInfo.type === 'audio') {
                this.joinaudioroom();
            }
        }
    }
    CallYesPage.prototype.ionViewWillLeave = function () {
        this.leaveroom();
    };
    CallYesPage.prototype.initMyRTC = function () {
        var _this = this;
        this.connection = new RTCMultiConnection();
        this.connection.socketURL = 'https://ezmaven.com:8888/';
        this.connection.socketMessageEvent = 'video-conference-demo';
        this.connection.session = {
            audio: true,
            video: true
        };
        this.connection.mediaConstraints = {
            audio: true,
            video: {
                mandatory: {},
                optional: [{
                        facingMode: 'user'
                    }]
            }
        };
        this.connection.sdpConstraints.mandatory = {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
        };
        this.connection.onstream = function (event) {
            if (event.type == 'local') {
                _this.myvideo = { id: event.streamid, url: event.stream, type: event.type };
            }
            else {
                _this.othervideo = { id: event.streamid, url: event.stream, type: event.type };
            }
            if (_this.platform.is('ios')) {
                cordova.plugins.audioroute.overrideOutput('speaker', function (res) { return console.log('res', res); }, function (err) { return console.log('err', err); });
            }
            _this.refreshVideos();
        };
        this.connection.onstreamended = function (event) {
            _this.streemAudio.splice(_this.getMsgIndexById(event.streamid), 1);
        };
    };
    CallYesPage.prototype.initAudioMyRTC = function () {
        var _this = this;
        this.connection = new RTCMultiConnection();
        this.connection.socketURL = 'https://ezmaven.com:8888/';
        this.connection.socketMessageEvent = 'audio-conference-demo';
        this.connection.session = {
            audio: true,
            video: false
        };
        this.connection.mediaConstraints = {
            audio: true,
            video: false
        };
        this.connection.sdpConstraints.mandatory = {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: false
        };
        this.connection.onstream = function (event) {
            if (event.type == 'local') {
                _this.myvideo = { id: event.streamid, url: event.stream, type: event.type };
            }
            else {
                _this.othervideo = { id: event.streamid, url: event.stream, type: event.type };
            }
            if (_this.platform.is('ios')) {
                cordova.plugins.audioroute.overrideOutput('speaker', function (res) { return console.log('res', res); }, function (err) { return console.log('err', err); });
            }
            _this.refreshVideos();
        };
        this.connection.onstreamended = function (event) {
            _this.streemAudio.splice(_this.getMsgIndexById(event.streamid), 1);
        };
    };
    CallYesPage.prototype.openroom = function () {
        var _this = this;
        this.initMyRTC();
        this.connection.open(this.callInfo.roomid, function () {
            console.log(_this.connection);
        });
        this.chatService.sendCallPush(this.callInfo.roomid, this.callInfo.userid, this.callInfo.type).subscribe();
        console.log(this.callInfo.roomid);
    };
    CallYesPage.prototype.openaudioroom = function () {
        var _this = this;
        this.initAudioMyRTC();
        this.connection.open(this.callInfo.roomid, function () {
            console.log(_this.connection);
        });
        this.chatService.sendCallPush(this.callInfo.roomid, this.callInfo.userid, this.callInfo.type).subscribe();
        console.log(this.callInfo.roomid);
    };
    CallYesPage.prototype.joinroom = function () {
        this.initMyRTC();
        this.connection.join(this.callInfo.roomid);
        this.chatService.changeStatusCall(this.callInfo.roomid).subscribe();
        console.log(this.callInfo.roomid);
    };
    CallYesPage.prototype.joinaudioroom = function () {
        this.initAudioMyRTC();
        this.connection.join(this.callInfo.roomid);
        this.chatService.changeStatusCall(this.callInfo.roomid).subscribe();
        console.log(this.callInfo.roomid);
    };
    CallYesPage.prototype.getMsgIndexById = function (id) {
        return this.streemAudio.findIndex(function (e) { return e.id === id; });
    };
    CallYesPage.prototype.refreshVideos = function () {
        // tell the modal that we need to revresh the video
        this.ref.tick();
        if (!this.platform.is('cordova')) {
            return;
        }
        try {
            for (var x = 0; x <= 3000; x += 300) {
                console.log(x);
                setTimeout(cordova.plugins.iosrtc.refreshVideos, x);
            }
        }
        catch (e) {
            console.log(e);
        }
    };
    ;
    CallYesPage.prototype.closecall = function () {
        this.navCtrl.pop();
    };
    CallYesPage.prototype.changecam = function () {
        if (this.platform.is('ios')) {
            if (this.camerafront) {
                this.connection.mediaConstraints = {
                    audio: true,
                    video: {
                        facingMode: 'application'
                    }
                };
                this.camerafront = false;
            }
            else {
                this.connection.mediaConstraints = {
                    audio: true,
                    video: {
                        facingMode: 'user'
                    }
                };
                this.camerafront = true;
            }
        }
        else {
            console.log(DetectRTC.videoInputDevices);
            if (this.camerafront) {
                this.connection.mediaConstraints = {
                    audio: true,
                    video: {
                        mandatory: {
                            sourceId: DetectRTC.videoInputDevices[1].deviceId // back-camera
                        },
                        optional: []
                    }
                };
                this.camerafront = false;
            }
            else {
                this.connection.mediaConstraints = {
                    audio: true,
                    video: {
                        deviceId: DetectRTC.videoInputDevices[0].deviceId // front-camera
                    }
                };
                this.camerafront = true;
            }
        }
        console.log('aga');
    };
    CallYesPage.prototype.leaveroom = function () {
        this.connection.attachStreams.forEach(function (localStream) {
            localStream.stop();
        });
        this.connection.close();
        this.connection.disconnect();
        this.connection = false;
        if (this.platform.is('cordova')) {
            cordova.plugins.CordovaCall.endCall();
        }
    };
    CallYesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-call-yes',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/call-yes/call-yes.html"*/'<!--\n  Generated template for the CallYesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<div class="headerAll">\n  <span class="titleHeader" *ngIf="this.callInfo.type===\'video\'"><img src="assets/img/logo_inverse.png" >Видеозвонок</span>\n  <span class="titleHeader" *ngIf="this.callInfo.type===\'audio\'"><img src="assets/img/logo_inverse.png" >Аудиозвонок</span>\n</div>\n\n<ion-content>\n  <div class="classStartImg">\n    <img src="assets/img/call.jpg">\n  </div>\n  <div [ngStyle]="{\'display\':this.callInfo.type==\'video\'?\'block\':\'none\'}">\n    <div class="frameVideo">\n      <video  class="videoManMy" [id]="myvideo.id" *ngIf="myvideo" autoplay [muted]="true" [srcObject]="myvideo.url" playsinline></video>\n    </div>\n    <div class="otherFrameVideo">\n      <video  class="videoMan" [id]="othervideo.id" *ngIf="othervideo" autoplay [srcObject]="othervideo.url" playsinline></video>\n    </div>\n  </div>\n  <span class="nameManCall">{{this.nameusercall}}</span>\n  <span class="phoneManCall">вызов <span>{{this.phoneusercall}}</span>...</span>\n  <div class="divImageCall">\n    <img class="imageAvatarCall" [src]="this.imageusercall">\n  </div>\n  <div *ngIf="this.callInfo.type==\'audio\'">\n    <ion-fab center bottom class="botButSl">\n      <button ion-fab icon-only (click)="closecall()"><ion-icon name="ios-volume-mute"></ion-icon> <!--<ion-icon name="volume-up"></ion-icon>--></button>\n      <span>Динамик</span>\n    </ion-fab>\n    <ion-fab center bottom class="botButS">\n      <button disabled ion-fab icon-only (click)="closecall()"><ion-icon name="videocam"></ion-icon></button>\n      <span>Видео</span>\n    </ion-fab>\n    <ion-fab center bottom class="botButSr">\n      <button ion-fab icon-only (click)="closecall()"><ion-icon name="mic"></ion-icon> <!--<ion-icon name="mic-off"></ion-icon>--></button>\n      <span>Микрофон</span>\n    </ion-fab>\n\n\n\n    <ion-fab center bottom class="botBut">\n      <button ion-fab icon-only (click)="closecall()"><ion-icon name="close"></ion-icon></button>\n    </ion-fab>\n  </div>\n</ion-content>\n\n<ion-footer *ngIf="this.callInfo.type==\'video\'">\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12>\n        <button (click)="closecall()" class="closeroom" ion-button icon-only color="secondary" full>\n          <ion-icon name="call"></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/call-yes/call-yes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ApplicationRef"],
            __WEBPACK_IMPORTED_MODULE_5__providers_profile__["a" /* ProfileProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__["a" /* Diagnostic */]])
    ], CallYesPage);
    return CallYesPage;
}());

//# sourceMappingURL=call-yes.js.map

/***/ }),

/***/ 879:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallYesPageModule", function() { return CallYesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__call_yes__ = __webpack_require__(1053);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CallYesPageModule = /** @class */ (function () {
    function CallYesPageModule() {
    }
    CallYesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__call_yes__["a" /* CallYesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__call_yes__["a" /* CallYesPage */]),
            ],
        })
    ], CallYesPageModule);
    return CallYesPageModule;
}());

//# sourceMappingURL=call-yes.module.js.map

/***/ })

});
//# sourceMappingURL=31.js.map