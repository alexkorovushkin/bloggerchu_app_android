webpackJsonp([18],{

/***/ 1037:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MastersMapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_geocoder__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_masters__ = __webpack_require__(460);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import {MastersMapProfilePage} from "../masters-map-profile/masters-map-profile";
//import {ProfileViewPage} from "../profile-view/profile-view";
var MastersMapPage = /** @class */ (function () {
    function MastersMapPage(navCtrl, navParams, nativeGeocoder, masterService, modalCtrl, 
        //private ngZone:NgZone,
        viewCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.nativeGeocoder = nativeGeocoder;
        this.masterService = masterService;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.events = events;
        this.mastersList = {};
        this.markers = [];
        this.myLocation = {
            latitude: 55.75370903771494,
            longitude: 37.61981338262558
        };
        this.loadMap();
        //let mastersList=this.navParams.get('mastersList');
        /*
            for (let master of mastersList){
              if(master.lat!=null && master.lng!=null){
                console.log('proconal', master)
        
                let marker: MarkerOptions ={
                  title: master.name,
                  icon: 'blue',
                  animation: 'DROP',
                  position: {
                    lat: parseFloat(master.lat),
                    lng: parseFloat(master.lng)
                  }
                };
        
                this.markers.push(marker )
        
                /!*
                            cluster.addMarker({
                              title: master.name,
                              icon: 'blue',
                              animation: 'DROP',
                              position: {
                                lat: master.lat,
                                lng: master.lng
                              }
                            })
                            .then(marker => {
                              marker.on(GoogleMapsEvent.MARKER_CLICK)
                                .subscribe(() => {
                                  alert('clicked');
                                });
                            });*!/
              }
            }*/
    }
    MastersMapPage.prototype.ionViewWillEnter = function () {
    };
    MastersMapPage.prototype.ionViewWillLeave = function () {
        this.map.remove();
    };
    MastersMapPage.prototype.addMarker = function (markers) {
        var _this = this;
        var clusterOptions = {
            boundsDraw: false,
            maxZoomLevel: 15,
            markers: markers,
            icons: [
                { url: "assets/img/m1.png", min: 2, max: 10, anchor: { x: 25, y: 25 }, label: { color: "white", bold: true, fontSize: 14 }, size: { width: 50, height: 50 } },
                { url: "assets/img/m2.png", min: 10, max: 50, anchor: { x: 40, y: 40 }, label: { color: "white", bold: true, fontSize: 14 }, size: { width: 80, height: 80 } },
                { url: "assets/img/m3.png", min: 50, max: 200, anchor: { x: 60, y: 60 }, label: { color: "white", bold: true, fontSize: 14 }, size: { width: 120, height: 120 } },
                { url: "assets/img/m4.png", min: 200, anchor: { x: 75, y: 75 }, label: { color: "white", bold: true, fontSize: 14 }, size: { width: 150, height: 150 } },
            ]
        };
        this.map.addMarkerCluster(clusterOptions).then(function (markerCluster) {
            _this.markerCluster = markerCluster;
            markerCluster.on(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["c" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function (params) {
                console.log(params);
                var marker = params[1];
                console.log(marker);
            });
        });
    };
    MastersMapPage.prototype.getMyLocation = function () {
        var _this = this;
        this.map.getMyLocation({ enableHighAccuracy: true }).then(function (location) {
            console.log(location);
            _this.map.setCameraTarget(location.latLng);
        });
    };
    MastersMapPage.prototype.sendGeo = function () {
        var _this = this;
        this.map.getMyLocation({ enableHighAccuracy: true }).then(function (location) {
            _this.map.remove();
            _this.viewCtrl.dismiss(location);
            /*this.callback = this.navParams.get("callback")
            this.callback(location).then(()=>{
              this.navCtrl.pop();
            });*/
        });
    };
    MastersMapPage.prototype.loadMap = function () {
        var _this = this;
        var mapOptions = {
            controls: {
                myLocationButton: false,
                compass: true,
                indoorPicker: false,
                mapToolbar: false
            },
            camera: {
                target: {
                    lat: 55.75370903771494,
                    lng: 37.61981338262558
                },
                zoom: 16,
            }
        };
        this.map = __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
        this.map.one(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["c" /* GoogleMapsEvent */].MAP_READY)
            .then(function () {
            console.log('Map is ready!');
            _this.map.setMyLocationEnabled(true);
            _this.getMyLocation();
        });
    };
    MastersMapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-masters-map',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/masters-map/masters-map.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Карта</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div style="height: 100%;" id="map_canvas">\n    <ion-fab top right >\n      <button style="z-index: 9999" (click)="getMyLocation()" ion-fab mini><ion-icon name="locate"></ion-icon></button>\n    </ion-fab>\n  </div>\n  <div class="sendGeoposition" padding>\n    <button  ion-button block (click)="sendGeo()">Отправить геопозицию</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/masters-map/masters-map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
            __WEBPACK_IMPORTED_MODULE_4__providers_masters__["a" /* MastersProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */]])
    ], MastersMapPage);
    return MastersMapPage;
}());

//# sourceMappingURL=masters-map.js.map

/***/ }),

/***/ 858:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MastersMapPageModule", function() { return MastersMapPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__masters_map__ = __webpack_require__(1037);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MastersMapPageModule = /** @class */ (function () {
    function MastersMapPageModule() {
    }
    MastersMapPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__masters_map__["a" /* MastersMapPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__masters_map__["a" /* MastersMapPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__masters_map__["a" /* MastersMapPage */]
            ]
        })
    ], MastersMapPageModule);
    return MastersMapPageModule;
}());

//# sourceMappingURL=masters-map.module.js.map

/***/ })

});
//# sourceMappingURL=18.js.map