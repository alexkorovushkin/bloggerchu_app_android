webpackJsonp([28],{

/***/ 1026:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CallstoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__ = __webpack_require__(75);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CallstoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CallstoryPage = /** @class */ (function () {
    function CallstoryPage(navCtrl, chatService, auth, navParams, sqlite, platform) {
        this.navCtrl = navCtrl;
        this.chatService = chatService;
        this.auth = auth;
        this.navParams = navParams;
        this.sqlite = sqlite;
        this.platform = platform;
        this.callstory = [];
        this.myPeopleList = [];
        this.lastOtherUserID = 0;
        if (this.platform.is('cordova')) {
            this.getLocalContacts();
        }
    }
    CallstoryPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.chatService.getCall(this.auth.currentUser.id).subscribe(function (res) {
            if (res.status) {
                console.log(res);
                _this.callstory = res.list;
            }
        });
    };
    CallstoryPage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    CallstoryPage.prototype.gotoCallInfo = function (id) {
        this.navCtrl.push('CallinfoPage', { id: id });
    };
    CallstoryPage.prototype.goToPeoplePhone = function () {
        this.navCtrl.push('PeoplecallPage');
    };
    CallstoryPage.prototype.newcall = function (userid, type) {
        this.navCtrl.push('CallYesPage', { info: { userid: userid, myid: this.auth.currentUser.id, roomid: userid + '-' + this.auth.currentUser.id + '-' + new Date().getTime(), type: type, status: 'open' } });
    };
    CallstoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CallstoryPage');
    };
    CallstoryPage.prototype.getLocalContacts = function () {
        var _this = this;
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        })
            .then(function (db) {
            db.executeSql('SELECT * FROM mycontacts ORDER BY id DESC, name ASC')
                .then(function (res) {
                console.log('sqlite_contacts', res);
                _this.myPeopleList = [];
                for (var i = 0; i < res.rows.length; i++) {
                    console.log(res.rows.item(i));
                    _this.myPeopleList.push(res.rows.item(i));
                    console.log(_this.myPeopleList);
                }
            })
                .catch(function (e) {
                console.log(e);
            });
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    CallstoryPage.prototype.findMyContactIndexByID = function (id) {
        return this.myPeopleList.findIndex(function (e) { return e.id === +id; });
    };
    CallstoryPage.prototype.delCallStory = function (id) {
        var _this = this;
        this.chatService.dellCall(id).subscribe(function (res) {
            if (res.status) {
                _this.chatService.getCall(_this.auth.currentUser.id).subscribe(function (res) {
                    if (res.status) {
                        _this.callstory = res.list;
                    }
                });
            }
        });
    };
    CallstoryPage.prototype.countCallsHide = function (index) {
        var count = 0, userid = true, useridFrom = this.callstory[index].userid;
        for (var i = index; userid && i < this.callstory.length; i++) {
            if (this.callstory[i].userid === useridFrom) {
                count++;
            }
            else {
                userid = false;
            }
        }
        return count;
    };
    CallstoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-callstory',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/callstory/callstory.html"*/'<!--\n  Generated template for the CallstoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-buttons end color="primary2">\n      <button ion-button icon-only (click)="goToPeoplePhone()" >\n        <ion-icon name="ios-call-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>\n      Звонки\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content >\n  <ion-searchbar\n    animated="true"\n    placeholder="Поиск"\n    autocomplete="on"\n    autocorrect="on"\n    (ionInput)="onSearch($event)">\n  </ion-searchbar>\n  <ion-list id="chats-list4" >\n    <ng-container *ngFor="let row of callstory; let i = index">\n      <ion-item-sliding *ngIf="i === 0 || callstory[i ===0 ? 0 : i - 1].userid !== callstory[i].userid">\n        <ion-item color="none"  id="chats-list-item4" >\n          <ion-avatar item-left  (click)="goToProfile(row.userid)">\n            <img [src]="row.avatar"/>\n          </ion-avatar>\n          <h2 (click)="newcall(row.userid,row.type)">\n            {{findMyContactIndexByID(row.userid)==-1?row.name:myPeopleList[findMyContactIndexByID(row.userid)].name}}\n            <small class="countCall" *ngIf="countCallsHide(i) > 1">({{countCallsHide(i)}})</small>\n          </h2>\n          <p>\n            <ion-icon *ngIf="row.status===\'out\' || row.status===\'in\'" [name]="row.type==\'audio\'?\'call\':\'videocam\'"></ion-icon>\n            <ion-icon style="color: #ff6666!important;" *ngIf="row.status===\'missed\'" [name]="row.type==\'audio\'?\'call\':\'videocam\'"></ion-icon>\n            <span *ngIf="row.status===\'out\'">Исходящий</span>\n            <span *ngIf="row.status===\'in\'">Входящий</span>\n            <span style="color: #ff6666" *ngIf="row.status===\'missed\'">Пропущенный</span>\n          </p>\n          <span *ngIf="row.moreoneday==0" item-end class="timeMes">сегодня в {{row.date | formatTime:\'HH:mm\'}}</span>\n          <span *ngIf="row.moreoneday==1" item-end class="timeMes">{{row.date | formatTime:\'DD.MM.YY\'}}</span>\n          <button class="buttonInfo" ion-button clear icon-only item-end (click)="gotoCallInfo(row.userid)">\n            <ion-icon [color]="primary" item-end name="ios-information-circle-outline"></ion-icon>\n          </button>\n        </ion-item>\n        <ion-item-options icon-start (ionSwipe)="delCallStory(row.id)">\n          <button class="iconTrash" icon-only expandable (click)="delCallStory(row.id)">\n            <ion-icon name="ios-trash-outline"></ion-icon>\n          </button>\n        </ion-item-options>\n      </ion-item-sliding>\n    </ng-container>\n  </ion-list>\n\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/callstory/callstory.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */]])
    ], CallstoryPage);
    return CallstoryPage;
}());

//# sourceMappingURL=callstory.js.map

/***/ }),

/***/ 849:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallstoryPageModule", function() { return CallstoryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__callstory__ = __webpack_require__(1026);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_format_time_module__ = __webpack_require__(438);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CallstoryPageModule = /** @class */ (function () {
    function CallstoryPageModule() {
    }
    CallstoryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__callstory__["a" /* CallstoryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__pipes_format_time_module__["a" /* FormatTimeModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__callstory__["a" /* CallstoryPage */]),
            ],
        })
    ], CallstoryPageModule);
    return CallstoryPageModule;
}());

//# sourceMappingURL=callstory.module.js.map

/***/ })

});
//# sourceMappingURL=28.js.map