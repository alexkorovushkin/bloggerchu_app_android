webpackJsonp([26],{

/***/ 1028:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatsGroupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_config__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chat_chat__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ChatsGroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatsGroupPage = /** @class */ (function () {
    function ChatsGroupPage(platform, navCtrl, auth, chatService, config, alertCtrl) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.chatService = chatService;
        this.config = config;
        this.alertCtrl = alertCtrl;
        this.messageList = [];
        this.waitLoading = true;
        this.resumeEvent = platform.resume.subscribe(function (e) {
            if (_this.navCtrl.getActive().name == 'ChatsPage') {
                //console.log("resume called");
                _this.updateUnreadMsgCount = setInterval(function () {
                    _this.getCountMsg();
                }, 2000);
            }
        });
        this.pauseEvent = platform.pause.subscribe(function (e) {
            if (_this.navCtrl.getActive().name == 'ChatsPage') {
                //console.log("pause called");
                clearInterval(_this.updateUnreadMsgCount);
            }
        });
    }
    ChatsGroupPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.getRooms();
        this.updateUnreadMsgCount = setInterval(function () {
            _this.getCountMsg();
        }, 2000);
    };
    ChatsGroupPage.prototype.ionViewWillLeave = function () {
        // unsubscribe
        this.resumeEvent.unsubscribe();
        this.pauseEvent.unsubscribe();
        clearInterval(this.updateUnreadMsgCount);
    };
    ChatsGroupPage.prototype.getRooms = function () {
        var _this = this;
        var tmp;
        return this.chatService
            .getUserList()
            .subscribe(function (res) {
            //console.log('Получение списка чатов', res)
            tmp = res;
            _this.messageList = tmp.msgList;
            _this.waitLoading = false;
        });
    };
    ChatsGroupPage.prototype.getCountMsg = function () {
        var _this = this;
        return this.chatService
            .unreadCountMsg()
            .subscribe(function (res) {
            //console.log('Обновление списка чатов',res)
            if (_this.unreadMsg != res.count)
                _this.getRooms();
            _this.unreadMsg = res.count;
        });
    };
    ChatsGroupPage.prototype.delChatConfirm = function (roomid) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Удаление чата",
            message: 'Вы действительно хотите удалить чат? Данный чат невозможно будет восстановить...',
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                },
                {
                    text: 'Удалить',
                    handler: function () {
                        _this.chatService.msgRemoveRoom(roomid).subscribe(function (res) {
                            if (res.status) {
                                _this.getRooms();
                            }
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    ChatsGroupPage.prototype.goToProfile = function (id) {
        this.navCtrl.push('ProfileViewPage', { id: id });
    };
    ChatsGroupPage.prototype.goToChat = function (params) {
        this.navCtrl.popToRoot();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__chat_chat__["a" /* Chat */], params);
    };
    ChatsGroupPage.prototype.peopleSearch = function () {
        this.navCtrl.push('PeoplePage');
    };
    ChatsGroupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chats-group',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/chats-group/chats-group.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Группы</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/chats-group/chats-group.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_config__["a" /* ConfigProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ChatsGroupPage);
    return ChatsGroupPage;
}());

//# sourceMappingURL=chats-group.js.map

/***/ }),

/***/ 851:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatsGroupPageModule", function() { return ChatsGroupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chats_group__ = __webpack_require__(1028);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ChatsGroupPageModule = /** @class */ (function () {
    function ChatsGroupPageModule() {
    }
    ChatsGroupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__chats_group__["a" /* ChatsGroupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__chats_group__["a" /* ChatsGroupPage */]),
            ],
        })
    ], ChatsGroupPageModule);
    return ChatsGroupPageModule;
}());

//# sourceMappingURL=chats-group.module.js.map

/***/ })

});
//# sourceMappingURL=26.js.map