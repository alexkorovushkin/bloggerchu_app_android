webpackJsonp([13],{

/***/ 1043:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the OrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OrderPage = /** @class */ (function () {
    function OrderPage(navCtrl, chatService, auth, loadingCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.chatService = chatService;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.Accedptorders = [];
    }
    OrderPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.chatService.getAccedptMyOrders().subscribe(function (res) {
            if (res.status) {
                _this.Accedptorders = res.order;
            }
        });
    };
    OrderPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.chatService.getAccedptMyOrders().subscribe(function (res) {
            if (res.status) {
                _this.Accedptorders = res.order;
            }
        });
    };
    OrderPage.prototype.findIndexByID = function (id) {
        return this.Accedptorders.findIndex(function (e) { return e.id === id; });
    };
    OrderPage.prototype.showLoading = function (text) {
        this.loading = this.loadingCtrl.create({
            content: text,
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    OrderPage.prototype.acceptOrder = function (id, userid) {
        var _this = this;
        this.showLoading('Оформляем');
        this.chatService.acceptOrder(id, userid, this.auth.currentUser.name, this.lonkOrder).subscribe(function (res) {
            if (res.status) {
                console.log(_this.lonkOrder);
                var index = _this.findIndexByID(id);
                if (index >= 0) {
                    _this.Accedptorders.splice(index, 1);
                }
                _this.loading.dismiss();
            }
        });
    };
    OrderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-order',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/order/order.html"*/'<!--\n  Generated template for the OrderPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Заказы в работе</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n  <ion-card  *ngIf="!Accedptorders">\n    У вас нет заявок на рекламу\n  </ion-card>\n  <ion-card *ngFor="let order of Accedptorders">\n    <img src="{{order.avatar}}"/>\n    <ion-card-content>\n    <p>\n      {{order.name}} заказал у вас {{order.siting}} с требованиями {{order.requirements}} На {{order.dateStart}} в {{order.timeStart}} на условиях {{order.conditions}}\n    </p>\n    <ion-input [(ngModel)]="lonkOrder" type="text" placeholder="Ссылка на пост"></ion-input>\n    <button *ngIf="lonkOrder && lonkOrder.length>5" class="yesOrder" (click)="acceptOrder(order.id,order.userid)">Выполнено</button>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/order/order.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], OrderPage);
    return OrderPage;
}());

//# sourceMappingURL=order.js.map

/***/ }),

/***/ 865:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderPageModule", function() { return OrderPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order__ = __webpack_require__(1043);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var OrderPageModule = /** @class */ (function () {
    function OrderPageModule() {
    }
    OrderPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__order__["a" /* OrderPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__order__["a" /* OrderPage */]),
            ],
        })
    ], OrderPageModule);
    return OrderPageModule;
}());

//# sourceMappingURL=order.module.js.map

/***/ })

});
//# sourceMappingURL=13.js.map