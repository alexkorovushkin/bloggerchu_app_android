webpackJsonp([30],{

/***/ 1025:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CallPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__ = __webpack_require__(191);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CallPage = /** @class */ (function () {
    function CallPage(navCtrl, navParams, platform, ref, diagnostic) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.ref = ref;
        this.diagnostic = diagnostic;
        this.roomid = '7822';
        this.disablebutton = false;
        this.streemAudio = [];
        if (this.platform.is('cordova')) {
            this.diagnostic.getCameraAuthorizationStatus().then(function (status) {
                if (status != _this.diagnostic.permissionStatus.GRANTED) {
                    _this.diagnostic.requestCameraAuthorization();
                }
            });
            this.diagnostic.getMicrophoneAuthorizationStatus().then(function (status) {
                if (status != _this.diagnostic.permissionStatus.GRANTED) {
                    _this.diagnostic.requestMicrophoneAuthorization();
                }
            });
        }
    }
    CallPage.prototype.initMyRTC = function () {
        var _this = this;
        this.connection = new RTCMultiConnection();
        this.connection.socketURL = 'https://ezmaven.com:8888/';
        this.connection.socketMessageEvent = 'video-conference-demo';
        this.connection.session = {
            audio: true,
            video: true
        };
        this.connection.mediaConstraints = {
            audio: true,
            video: {
                mandatory: {},
                optional: [{
                        facingMode: 'user'
                    }]
            }
        };
        this.connection.sdpConstraints.mandatory = {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
        };
        this.connection.onstream = function (event) {
            _this.streemAudio.push({ id: event.streamid, url: event.stream, type: event.type });
            if (_this.platform.is('ios')) {
                cordova.plugins.audioroute.overrideOutput('speaker', function (res) { return console.log('res', res); }, function (err) { return console.log('err', err); });
            }
            _this.refreshVideos();
        };
        this.connection.onstreamended = function (event) {
            _this.streemAudio.splice(_this.getMsgIndexById(event.streamid), 1);
        };
    };
    CallPage.prototype.initAudioMyRTC = function () {
        var _this = this;
        this.connection = new RTCMultiConnection();
        this.connection.socketURL = 'https://ezmaven.com:8888/';
        this.connection.socketMessageEvent = 'audio-conference-demo';
        this.connection.session = {
            audio: true,
            video: false
        };
        this.connection.mediaConstraints = {
            audio: true,
            video: false
        };
        this.connection.sdpConstraints.mandatory = {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: false
        };
        this.connection.onstream = function (event) {
            _this.streemAudio.push({ id: event.streamid, url: event.stream, type: event.type });
            if (_this.platform.is('ios')) {
                cordova.plugins.audioroute.overrideOutput('speaker', function (res) { return console.log('res', res); }, function (err) { return console.log('err', err); });
            }
            _this.refreshVideos();
        };
        this.connection.onstreamended = function (event) {
            _this.streemAudio.splice(_this.getMsgIndexById(event.streamid), 1);
        };
    };
    CallPage.prototype.disableInputButtons = function () {
        this.disablebutton = true;
    };
    CallPage.prototype.openroom = function () {
        var _this = this;
        this.initMyRTC();
        this.disableInputButtons();
        this.connection.open(this.roomid, function () {
            console.log(_this.connection);
        });
        console.log(this.roomid);
    };
    CallPage.prototype.openaudioroom = function () {
        var _this = this;
        this.initAudioMyRTC();
        this.disableInputButtons();
        this.connection.open(this.roomid, function () {
            console.log(_this.connection);
        });
        console.log(this.roomid);
    };
    CallPage.prototype.joinroom = function () {
        this.initMyRTC();
        this.disableInputButtons();
        this.connection.join(this.roomid);
        console.log(this.roomid);
    };
    CallPage.prototype.joinaudioroom = function () {
        this.initAudioMyRTC();
        this.disableInputButtons();
        this.connection.join(this.roomid);
        console.log(this.roomid);
    };
    CallPage.prototype.getMsgIndexById = function (id) {
        return this.streemAudio.findIndex(function (e) { return e.id === id; });
    };
    CallPage.prototype.refreshVideos = function () {
        // tell the modal that we need to revresh the video
        this.ref.tick();
        if (!this.platform.is('cordova')) {
            return;
        }
        try {
            for (var x = 0; x <= 3000; x += 300) {
                console.log(x);
                setTimeout(cordova.plugins.iosrtc.refreshVideos, x);
            }
        }
        catch (e) {
            console.log(e);
        }
    };
    ;
    CallPage.prototype.leaveroom = function () {
        this.connection.attachStreams.forEach(function (localStream) {
            localStream.stop();
        });
        this.connection.close();
        this.connection = false;
        this.disablebutton = false;
    };
    CallPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-call',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/call/call.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-title>История вызовов</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n\n\n\n  <ion-item>\n    <ion-label floating>id комнаты</ion-label>\n    <ion-input required minlength="1"  maxlength="60" name="roomid" type="roomid" [(ngModel)]="roomid"></ion-input>\n  </ion-item>\n  <div style="height: 20px"></div>\n  <button *ngIf="!connection" [disabled]="disablebutton || roomid.length==0" ion-button block mode="ios" (click)="openroom()">Создать Видео конференцию</button>\n  <button *ngIf="!connection" [disabled]="disablebutton || roomid.length==0" ion-button block mode="ios" (click)="openaudioroom()">Создать Аудио конференцию</button>\n  <button *ngIf="!connection" [disabled]="disablebutton || roomid.length==0" ion-button block mode="ios" (click)="joinroom()">Подключиться к видео</button>\n  <button *ngIf="!connection" [disabled]="disablebutton || roomid.length==0" ion-button block mode="ios" (click)="joinaudioroom()">Подключиться к аудио</button>\n  <button *ngIf="connection" ion-button block mode="ios" (click)="leaveroom()">Отключиться</button>\n  <div style="height: 20px"></div>\n  <ion-grid>\n    <ion-row>\n      <ion-col class="videoMan" col-6 *ngFor="let row of streemAudio">\n        <video style="width: 100%; display: inline-block; visibility: visible; float: left;" [id]="row.id" *ngIf="row" autoplay [muted]="row.type==\'local\'" [srcObject]="row.url" playsinline></video>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/call/call.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ApplicationRef"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__["a" /* Diagnostic */]])
    ], CallPage);
    return CallPage;
}());

//# sourceMappingURL=call.js.map

/***/ }),

/***/ 848:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallPageModule", function() { return CallPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__call__ = __webpack_require__(1025);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CallPageModule = /** @class */ (function () {
    function CallPageModule() {
    }
    CallPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__call__["a" /* CallPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__call__["a" /* CallPage */]),
            ],
        })
    ], CallPageModule);
    return CallPageModule;
}());

//# sourceMappingURL=call.module.js.map

/***/ })

});
//# sourceMappingURL=30.js.map