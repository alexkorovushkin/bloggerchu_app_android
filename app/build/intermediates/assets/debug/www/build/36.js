webpackJsonp([36],{

/***/ 842:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcceptorderPageModule", function() { return AcceptorderPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__acceptorder__ = __webpack_require__(922);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AcceptorderPageModule = /** @class */ (function () {
    function AcceptorderPageModule() {
    }
    AcceptorderPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__acceptorder__["a" /* AcceptorderPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__acceptorder__["a" /* AcceptorderPage */]),
            ],
        })
    ], AcceptorderPageModule);
    return AcceptorderPageModule;
}());

//# sourceMappingURL=acceptorder.module.js.map

/***/ }),

/***/ 922:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcceptorderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__ = __webpack_require__(185);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the AcceptorderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AcceptorderPage = /** @class */ (function () {
    function AcceptorderPage(navCtrl, chatService, auth, browser, loadingCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.chatService = chatService;
        this.auth = auth;
        this.browser = browser;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.Accedptorderson = [];
    }
    AcceptorderPage.prototype.goToLink = function (link) {
        this.browser.create(link, '_system');
    };
    AcceptorderPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.chatService.getAccedptMyOrdersOn().subscribe(function (res) {
            if (res.status) {
                _this.Accedptorderson = res.order;
            }
        });
    };
    AcceptorderPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.chatService.getAccedptMyOrdersOn().subscribe(function (res) {
            if (res.status) {
                _this.Accedptorderson = res.order;
            }
        });
    };
    AcceptorderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-acceptorder',template:/*ion-inline-start:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/acceptorder/acceptorder.html"*/'<!--\n  Generated template for the AcceptorderPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Выполненные заказы</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-card  *ngIf="!Accedptorderson ">\n    У вас выполненных заказов\n  </ion-card>\n  <ion-card *ngFor="let order of Accedptorderson">\n    <img src="{{order.avatar}}"/>\n    <ion-card-content>\n      <p>\n        {{order.name}} заказал у вас {{order.siting}} с требованиями {{order.requirements}} На {{order.dateStart}} в {{order.timeStart}} на условиях {{order.conditions}}<br>\n        <span *ngIf="order.accept===\'3\'"><b>Идет проверка вашего поста</b></span>\n        <span *ngIf="order.accept===\'4\'"><b>Отлично заказ выполнен</b></span>\n      </p>\n      <button tappable (click)=" goToLink(order.link)">{{order.link}}</button>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/hardcorelite/Desktop/projects/allproject/bloggerchu/src/pages/acceptorder/acceptorder.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavParams */]])
    ], AcceptorderPage);
    return AcceptorderPage;
}());

//# sourceMappingURL=acceptorder.js.map

/***/ })

});
//# sourceMappingURL=36.js.map